﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
   public class HolidayManager : IHolidayManager
    {
        private readonly IHolidayRepository holidayRepository;
        public HolidayManager(IHolidayRepository holidayRepository)
        {
            this.holidayRepository = holidayRepository;
        }

        public HolidayResponse GetHolidays(string employeeId,string LanguageCode, CancellationToken cancellationToken)
        {
            return holidayRepository.GetHolidays(employeeId, LanguageCode);
        }
    }
}
