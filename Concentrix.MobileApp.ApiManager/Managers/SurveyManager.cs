﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Survey;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class SurveyManager : ISurveyManager
    {
        private readonly ISurveyRepository surveyRepository;
        public SurveyManager(ISurveyRepository surveyRepository)
        {
            this.surveyRepository = surveyRepository;
        }

        public PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId, CancellationToken cancellationToken)
        {
            return surveyRepository.GetTrainingSurveyList(surveylist,employeeId);

        }

        public PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId, CancellationToken cancellationToken)
        {
            return surveyRepository.GetmiHRSurveyList(mihrsurveylist,employeeId);

        }
        public GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId, CancellationToken cancellationToken)
        {
            return surveyRepository.RequestSurveyQuestions(surveyquestions,employeeId);

        }

        public SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId, CancellationToken cancellationToken)
        {
            SubmitSurveyResponse updateResponse = validateUpdateSurvey(submitsurvey);
            if (updateResponse.Acknowledge != AcknowledgeType.Success)
            {
                return updateResponse;
            }
            else
            {
                return surveyRepository.UpdateSurveyAnswer(submitsurvey,employeeId);
            }

        }
        #region Validation
        private SubmitSurveyResponse validateUpdateSurvey(SubmitSurveyRequest surveyRequest)
        {
            SubmitSurveyResponse surveyResponse = new SubmitSurveyResponse();

            if (surveyRequest.Surveys.Count == 0)
            {
                surveyResponse.Acknowledge = AcknowledgeType.Failure;
                surveyResponse.Message = "Invalid Inputs";

                foreach (var it in surveyRequest.Surveys)
                {
                    if (it.Rating == 0)
                    {
                        surveyResponse.ErrorCode.Add(new ErrorDetails { Code = "10851", Description = "Questions to be answered before you submit!!" });

                    }
                }

            }
            else
            {
                surveyResponse.Acknowledge = AcknowledgeType.Success;
            }
            return surveyResponse;

        }
        #endregion
    }
}
