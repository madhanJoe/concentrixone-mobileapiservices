﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class FeatureDepartmentManager : IFeatureDepartmentManager
    {
        private readonly IFeatureDepartmentRepository featureDepartmentRepository;
        public FeatureDepartmentManager(IFeatureDepartmentRepository featureDepartmentRepository)
        {
            this.featureDepartmentRepository = featureDepartmentRepository;
        }

        public GetFeatureDepartmentResponse GetSurveyDeparments(string EmployeeId, string LanguageCode, CancellationToken cacellationToken)
        {
            return featureDepartmentRepository.GetSurveyDeparments(EmployeeId,LanguageCode);
        }
    }
}
