﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class QuizManager : IQuizManager
    {

        private readonly IQuizRepository quizRepository;
        public QuizManager(IQuizRepository quizRepository)
        {
            this.quizRepository = quizRepository;
        }

        public PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string employeeId, CancellationToken cancellationToken)
        {
            return quizRepository.GetQuizList(quizlist,employeeId);

        }
        public GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string employeeId, CancellationToken cacellationToken)
        {

            return quizRepository.RequestQuizQuestions(requestquizquestions,employeeId);
        }

        public SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string employeeId, CancellationToken cacellationToken)
        {
            SubmitQuizResponse updateResponse = validateUpdateQuiz(updatequizanswer);
            if (updateResponse.Acknowledge != AcknowledgeType.Success)
            {
                return updateResponse;
            }
            else
            {

                return quizRepository.UpdateQuizAnswer(updatequizanswer,employeeId);
            }
        }

        public QuizSummaryResponse QuizSummary(QuizSummaryRequest quizsummary, string employeeId, CancellationToken cacellationToken)
        {

            return quizRepository.QuizSummary(quizsummary,employeeId);
        }

        #region Validation
        private SubmitQuizResponse validateUpdateQuiz(SubmitQuizRequest quizRequest)
        {
            SubmitQuizResponse quizResponse = new SubmitQuizResponse();

            if (quizRequest.Questions.ChoicesSelected.Count==0)
            {
                quizResponse.Acknowledge = AcknowledgeType.Failure;
                quizResponse.Message = "Invalid Inputs";

                quizResponse.ErrorCode.Add(new ErrorDetails { Code = "10751", Description = "Questions to be answered before you submit!!" });


            }
            else
            {
                quizResponse.Acknowledge = AcknowledgeType.Success;
            }
            return quizResponse;

        }
        #endregion
    }
}
