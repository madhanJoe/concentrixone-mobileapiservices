﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
  public  class LocationManager : ILocationManager
    {
        private readonly ILocationRepository locationRepository;
        public LocationManager(ILocationRepository locationRepository)
        {
            this.locationRepository = locationRepository;
        }

        public LocationResponse GetLocations(string employeeId, string locationCode, string languageCode, CancellationToken cancellationToken)
        {
            return locationRepository.GetLocations(employeeId,locationCode,languageCode);
        }
    }
}
