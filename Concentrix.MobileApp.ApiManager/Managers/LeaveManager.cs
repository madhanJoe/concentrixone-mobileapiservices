﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class LeaveManager : ILeaveManager
    {
        private readonly ILeaveRepository leaveRepository;
        public LeaveManager(ILeaveRepository leaveRepository)
        {
            this.leaveRepository = leaveRepository;
        }

        public LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leavebalance, string employeeId, CancellationToken cancellationToken)
        {
            LeaveBalanceResponse response = ValidateRequest(leavebalance);

            if (response.Acknowledge != AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return leaveRepository.GetLeaveBalance(leavebalance, employeeId);
            }
        }

        #region Validation

        private LeaveBalanceResponse ValidateRequest(LeaveBalanceRequest leaveBalance)
        {
            LeaveBalanceResponse leaveResponse = new LeaveBalanceResponse();

            if (string.IsNullOrEmpty(leaveBalance.CODE))
            {
                leaveResponse.Acknowledge = AcknowledgeType.Failure;
                leaveResponse.Message = "Invalid Inputs";

                leaveResponse.ErrorCode.Add(new ErrorDetails { Code = "10201", Description = "Please provide the Code." });
            }
            else
            {
                leaveResponse.Acknowledge = AcknowledgeType.Success;
            }

            return leaveResponse;
        }

        #endregion
    }
}
