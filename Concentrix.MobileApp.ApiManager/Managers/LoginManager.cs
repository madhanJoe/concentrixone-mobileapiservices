﻿using Concentrix.MobileApp.ApiManager.ADAuthenticationService;
using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Login;
using Concentrix.MobileApp.Helper.Validation;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class LoginManager : ILoginManager
    {
        private readonly ILoginRepository loginRepository;
        public LoginManager(ILoginRepository loginRepository)
        {
            this.loginRepository = loginRepository;
        }

        public LoginADResponse ADAuthentication(LoginADRequest login, CancellationToken cancellationToken)
        {
            var adResponse = new LoginADResponse();

            var emailUserName = string.Empty;

            if (login.UserName.ToLower().EndsWith("@concentrix.com"))
            {
                var onlyUserName = login.UserName.Split('@');

                if (onlyUserName.Length > 1)
                {
                    emailUserName = "concentrix\\" + onlyUserName[0];
                }
                else
                {
                    adResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                    adResponse.Message = "Not a valid username";
                    return adResponse;
                }
            }
            else if (login.UserName.ToLower().EndsWith("@minacs.com"))
            {
                var onlyUserName = login.UserName.Split('@');

                if (onlyUserName.Length > 1)
                {
                    emailUserName = "mx\\" + onlyUserName[0];
                }
                else
                {
                    adResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                    adResponse.Message = "Not a valid username";
                    return adResponse;
                }
            }
            else
            {
                adResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                adResponse.Message = "Not a valid email id";
                adResponse.ErrorCode.Add(new ErrorDetails { Code = "10309", Description = "Not a valid email id" });

                return adResponse;
            }

            var validationResponse = ValidateEmployee(login);

            if (validationResponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                return validationResponse;
            }

            login.UserName = emailUserName;

            var ADUser = new ADAuthenticationService.LoginRequest()
            {
                UserName = login.UserName,
                Password = login.Password
            };

            var proxy = new ActionsClient();

            var response = proxy.ValidateUser(ADUser);

            if (response.Acknowledge == ADAuthenticationService.AcknowledgeType.Success)
            {
                return loginRepository.GenerateToken(login);
            }
            else
            {
                adResponse.Acknowledge = (Domain.DomainModels.Base.AcknowledgeType)response.Acknowledge;
                adResponse.Message = response.Message;
                adResponse.ErrorCode.Add(new ErrorDetails { Code = "10001", Description = response.Message });
            }

            return adResponse;
        }

        public LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial, CancellationToken cancellationToken)
        {
            var response = ValidateSocialGuest(guestsocial);

            if (response.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return loginRepository.ValidateGuestSocial(guestsocial);
            }
        }
        
        public LoginADResponse ValidateGuest(LoginADRequest guestlogin, CancellationToken cancellationToken)
        {
            LoginADResponse Userresponse = ValidateGuestUser(guestlogin);

            if (Userresponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                return Userresponse;
            }
            else
            {
                return loginRepository.ValidateGuest(guestlogin);
            }
        }

        public ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotpassword, CancellationToken cancellationToken)
        {
            ForgotPasswordResponse guestresponse = ValidateGuestPassword(forgotpassword);

            if (guestresponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                return guestresponse;
            }
            else
            {
                return loginRepository.ForgotPassword(forgotpassword);
            }
        }

        #region Validation

        private LoginADResponse ValidateEmployee(LoginADRequest employeeRequest)
        {
            LoginADResponse validationResponse = new LoginADResponse();

            if (employeeRequest.UserType != 1)
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10312", Description = "Invalid User Type" });

            if (string.IsNullOrEmpty(employeeRequest.UserName))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10302", Description = "Please provide a valid UserName" });

            if (string.IsNullOrEmpty(employeeRequest.Password))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10314", Description = "Please provide a valid Password" });

            if (string.IsNullOrEmpty(employeeRequest.Email))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10303", Description = "Please provide a valid Email" });

            if (employeeRequest.Email.Length > 0)
            {
                if (!Validate.IsValidEmail(employeeRequest.Email))
                    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10308", Description = "Please provide a valid Email" });
            }

            if (string.IsNullOrEmpty(employeeRequest.DeviceId))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10315", Description = "Please provide a valid DeviceId" });

            //if (string.IsNullOrEmpty(employeeRequest.Longitude))
            //    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10316", Description = "Please provide a valid Longitude" });

            //if (string.IsNullOrEmpty(employeeRequest.Latitude))
            //    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10317", Description = "Please provide a valid Latitude" });

            if (validationResponse.ErrorCode.Count > 0)
            {
                validationResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                validationResponse.Message = "Missing values for Login";
            }
            else
                validationResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Success;

            return validationResponse;
        }

        private LoginADResponse ValidateSocialGuest(LoginSocialRequest socialRequest)
        {
            var socialResponse = new LoginADResponse();

            if (string.IsNullOrEmpty(socialRequest.UserName))
                socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10305", Description = "Please provide the UserName." });

            if (socialRequest.UserType != 2)
                socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10306", Description = "Please provide a valid UserType." });

            if (!(socialRequest.LoginType >= 45 && socialRequest.LoginType <= 49))
                socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10307", Description = "Please provide a valid LoginType." });

            if (string.IsNullOrEmpty(socialRequest.Email))
                socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10301", Description = "Please provide the Email." });
            else
            {
                if (!Validate.IsValidEmail(socialRequest.Email))
                    socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10308", Description = "Please provide a valid Email" });
            }

            if (string.IsNullOrEmpty(socialRequest.DeviceId))
                socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10315", Description = "Please provide a valid DeviceId" });

            //if (string.IsNullOrEmpty(socialRequest.Longitude))
            //    socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10316", Description = "Please provide a valid Longitude" });

            //if (string.IsNullOrEmpty(socialRequest.Latitude))
            //    socialResponse.ErrorCode.Add(new ErrorDetails { Code = "10317", Description = "Please provide a valid Latitude" });

            if (socialResponse.ErrorCode.Count > 0)
            {
                socialResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                socialResponse.Message = "Missing values for the Social Login";
            }
            else
                socialResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Success;

            return socialResponse;
        }

        private LoginADResponse ValidateGuestUser(LoginADRequest guestRequest)
        {
            LoginADResponse validationResponse = new LoginADResponse();

            if (guestRequest.UserType != 2)
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10312", Description = "Invalid User Type" });

            if (string.IsNullOrEmpty(guestRequest.UserName))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10302", Description = "Please provide a valid UserName" });

            if (string.IsNullOrEmpty(guestRequest.Password))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10314", Description = "Please provide a valid Password" });

            if (string.IsNullOrEmpty(guestRequest.Email))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10303", Description = "Please provide a valid Email" });

            if (guestRequest.Email.Length > 0)
            {
                if (!Validate.IsValidEmail(guestRequest.Email))
                    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10308", Description = "Please provide a valid Email" });
            }

            if (string.IsNullOrEmpty(guestRequest.DeviceId))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10315", Description = "Please provide a valid DeviceId" });

            //if (string.IsNullOrEmpty(guestRequest.Longitude))
            //    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10316", Description = "Please provide a valid Longitude" });

            //if (string.IsNullOrEmpty(guestRequest.Latitude))
            //    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10317", Description = "Please provide a valid Latitude" });

            if (validationResponse.ErrorCode.Count > 0)
            {
                validationResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                validationResponse.Message = "Missing values for Guest Login";
            }
            else
                validationResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Success;

            return validationResponse;
        }

        private ForgotPasswordResponse ValidateGuestPassword(ForgotPasswordRequest guestRequest)
        {
            ForgotPasswordResponse guestResponse = new ForgotPasswordResponse();

            if (string.IsNullOrEmpty(guestRequest.GuestEmail))
            {
                guestResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                guestResponse.Message = "Invalid Guest Email";

                if (string.IsNullOrEmpty(guestRequest.GuestEmail))
                {
                    guestResponse.ErrorCode.Add(new ErrorDetails { Code = "10304", Description = "Please provide the Guest Email." });
                }

                if (guestRequest.GuestEmail.Length > 0)
                {
                    if (!Validate.IsValidEmail(guestRequest.GuestEmail))
                        guestResponse.ErrorCode.Add(new ErrorDetails { Code = "10313", Description = "Please provide a valid Guest Email" });
                }
            }
            else
            {
                guestResponse.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Success;
            }

            return guestResponse;
        }
        #endregion
    }
}
