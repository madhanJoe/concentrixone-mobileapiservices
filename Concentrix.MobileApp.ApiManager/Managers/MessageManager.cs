﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Message;
using Concentrix.MobileApp.Helper.Validation;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
   public class MessageManager : IMessageManager
    {
        private readonly IMessageRepository messageRepository;
        public MessageManager(IMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        public DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple, string employeeId, CancellationToken cancellationToken)
        {
            DeleteMultipleMessagesResponse deletemutipleResponse = ValidateMesssageIDs(deletemultiple);

            if (deletemutipleResponse.Acknowledge != AcknowledgeType.Success)
            {
                return deletemutipleResponse;
            }
            else
            {
                return messageRepository.DeleteMultipleMessage(deletemultiple,employeeId);
            }
        }

        public DeleteAllMessagesResponse DeleteAllMessage(string employeeId, CancellationToken cancellationToken)
        {
            return messageRepository.DeleteAllMessage(employeeId);
        }

        public DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage, CancellationToken cancellationToken)
        {
            DeleteMessageResponse deleteResponse = ValidateMessage(deletemessage);

            if (deleteResponse.Acknowledge != AcknowledgeType.Success)
            {
                return deleteResponse;
            }
            else
            {
                return messageRepository.DeleteMessage(deletemessage);
            }
        }

        public UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId, CancellationToken cancellationToken)
        {
            return messageRepository.UpdateMessageRead(updatemessage,employeeId);
        }

        public MessageResponse GetMessage(MessageRequest messageRequest, string employeeId, CancellationToken cancellationToken)
        {
            var response = ValidateGetMessage(messageRequest, employeeId);

            if (response.Acknowledge != AcknowledgeType.Success)
                return response;
            else
                return messageRepository.GetMessage(messageRequest, employeeId);
        }

        #region Validation

        private MessageResponse ValidateGetMessage(MessageRequest messageRequest, string employeeId)
        {
            var response = new MessageResponse();

            if (messageRequest.PageNumber <= 0)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = "Validation Error";
                response.ErrorCode.Add(new ErrorDetails { Code = "10403", Description = "Please provide a valid PageNumber." });
            }

            if (string.IsNullOrEmpty(employeeId))
            {
                if (string.IsNullOrEmpty(messageRequest.EmailId))
                    response.ErrorCode.Add(new ErrorDetails { Code = "10404", Description = "Please provide the Email." });
                else
                {
                    if (!Validate.IsValidEmail(messageRequest.EmailId))
                    {
                        response.ErrorCode.Add(new ErrorDetails { Code = "10405", Description = "Please provide a valid Email" });
                    }
                }
            }

            if (response.ErrorCode.Count > 0)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = "Invalid Inputs";
            }
            else
            {
                response.Acknowledge = AcknowledgeType.Success;
            }

            return response;
        }

        private DeleteMultipleMessagesResponse ValidateMesssageIDs(DeleteMultipleMessagesRequest multiplerequest)
        {
            DeleteMultipleMessagesResponse multipleResponse = new DeleteMultipleMessagesResponse();

            if (multiplerequest.MessageIds == null)
            {
                multipleResponse.Acknowledge = AcknowledgeType.Failure;
                multipleResponse.Message = "Invalid Inputs";
                
                if (multiplerequest.MessageIds == null || multiplerequest.MessageIds.Count == 0)
                {
                    multipleResponse.ErrorCode.Add(new ErrorDetails { Code = "10401", Description = "You need to provide Message ids!." });
                }
            }
            else
            {
                multipleResponse.Acknowledge = AcknowledgeType.Success;
            }

            return multipleResponse;
        }

        private DeleteMessageResponse ValidateMessage(DeleteMessageRequest deleterequest)
        {
            DeleteMessageResponse deleteResponse = new DeleteMessageResponse();

            if (deleterequest.MessageId == 0)
            {
                deleteResponse.Acknowledge = AcknowledgeType.Failure;
                deleteResponse.Message = "Invalid Inputs";
                deleteResponse.ErrorCode.Add(new ErrorDetails { Code = "10402", Description = "Please provide the MessageId." });
            }
            else
            {
                deleteResponse.Acknowledge = AcknowledgeType.Success;
            }

            return deleteResponse;
        }
        #endregion


    }
}
