﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
  public  class TrainingVisibleManager : ITrainingVisibleManager
    {
        private readonly ITrainingVisibleRepository trainingRepository;
        public TrainingVisibleManager(ITrainingVisibleRepository trainingRepository)
        {
            this.trainingRepository = trainingRepository;
        }

        public TrainingVisibleResponse TrainingVisible(string employeeId, CancellationToken cancellationToken)
        {
            return trainingRepository.TrainingVisible(employeeId);
        }
    }
}
