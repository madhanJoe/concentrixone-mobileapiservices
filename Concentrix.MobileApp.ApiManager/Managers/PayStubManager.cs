﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class PayStubManager : IPayStubManager
    {
        private readonly IPayStubRepository paystubRepository;
        public PayStubManager(IPayStubRepository paystubRepository)
        {
            this.paystubRepository = paystubRepository;
        }

        public PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId, CancellationToken cancellationToken)
        {
            PayStubResponse payslipResponse = validatePayStub(payslip);
            if (payslipResponse.Acknowledge != AcknowledgeType.Success)
            {
                return payslipResponse;
            }
            else
            {
                return paystubRepository.GetPaySlip(payslip,employeeId);
            }
        }

        public PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId, CancellationToken cancellationToken)
        {
            PayStubResponse demandletterResponse = validatePayStub(demandletter);
            if (demandletterResponse.Acknowledge != AcknowledgeType.Success)
            {
                return demandletterResponse;
            }
            else
            {
                return paystubRepository.GetPaySlip(demandletter,employeeId);
            }
        }

        public PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId, CancellationToken cancellationToken)
        {
            PayStubResponse paystubResponse = validatePayStub(paystub);
            if (paystubResponse.Acknowledge != AcknowledgeType.Success)
            {
                return paystubResponse;
            }
            else
            {
                return paystubRepository.GetPaySlip(paystub,employeeId);
            }
        }

        #region Validation
        private PayStubResponse validatePayStub(PayStubRequest payRequest)
        {
            PayStubResponse payResponse = new PayStubResponse();

            if (string.IsNullOrEmpty(payRequest.Code))
            {
                payResponse.Acknowledge = AcknowledgeType.Failure;
                payResponse.Message = "Invalid Inputs";
                
                if (string.IsNullOrEmpty(payRequest.Code))
                {
                    payResponse.ErrorCode.Add(new ErrorDetails { Code = "10501", Description = "Please provide the code." });
                }

            }
            else
            {
                payResponse.Acknowledge = AcknowledgeType.Success;
            }
            return payResponse;

        }
        #endregion

    }
}
