﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class AttributeManager : IAttributeManager
    {
        private readonly IAttributeRepository attributeRepository;
        public AttributeManager(IAttributeRepository attributeRepository)
        {
            this.attributeRepository = attributeRepository;
        }

        public AttributeResponse GetDemandLetterList(AttributeRequest attribute, string employeeId, CancellationToken cancellationToken)
        {
            AttributeResponse response = ValidateRequest(attribute);
            if (response.Acknowledge != AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return attributeRepository.GetDemandLetterList(attribute,employeeId);
            }
        }

        public CountryResponse GetCountryCode(CountryRequest country, CancellationToken cancellationToken)
        {
            return attributeRepository.GetCountryCode(country);
        }

        public AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId, CancellationToken cancellationToken)
        {
            AttributeResponse response = ValidateRequest(policy);
            if (response.Acknowledge != AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return attributeRepository.GetPolicyList(policy,employeeId);
            }
        }

        #region Validation

        private AttributeResponse ValidateRequest(AttributeRequest newCase)
        {
            AttributeResponse attributeResponse = new AttributeResponse();

            if (string.IsNullOrEmpty(newCase.Code))
            {
                attributeResponse.Acknowledge = AcknowledgeType.Failure;
                attributeResponse.Message = "Invalid Inputs";
                attributeResponse.ErrorCode.Add(new ErrorDetails { Code = "10001", Description = "Please provide the Code." });
            }
            else
            {
                attributeResponse.Acknowledge = AcknowledgeType.Success;
            }

            return attributeResponse;
        }

        #endregion
    }
}
