﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
   public class MoodMeterManager : IMoodMeterManager
    {
        private readonly IMoodMeterRepository moodmeterRepository;
        public MoodMeterManager(IMoodMeterRepository moodmeterRepository)
        {
            this.moodmeterRepository = moodmeterRepository;
        }

        public MoodMeterResponse GetMoodMeterAttributes(string employeeId, CancellationToken cancellationToken)
        {
            return moodmeterRepository.GetMoodMeterAttributes(employeeId);
        }

        public SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating, string employeeId, CancellationToken cancellationToken)
        {
            SubmitMoodMeterResponse RatingResponse = validateRating(submitrating);
            if (RatingResponse.Acknowledge != AcknowledgeType.Success)
            {
                return RatingResponse;
            }
            else
            {
                return moodmeterRepository.SubmitMoodMeterRating(submitrating,employeeId);
            }
        }

        #region Validation
        private SubmitMoodMeterResponse validateRating(SubmitMoodMeterRequest ratingrequest)
        {
            SubmitMoodMeterResponse ratingResponse = new SubmitMoodMeterResponse();

            if (ratingrequest.MoodMeterID == 0)
            {
                ratingResponse.Acknowledge = AcknowledgeType.Failure;
                ratingResponse.Message = "Invalid Inputs";

                ratingResponse.ErrorCode.Add(new ErrorDetails { Code = "10451", Description = "Mood Meter attribute to be selected!!" });

            }
            else
            {
                ratingResponse.Acknowledge = AcknowledgeType.Success;
            }
            return ratingResponse;

        }
        #endregion

    }
}
