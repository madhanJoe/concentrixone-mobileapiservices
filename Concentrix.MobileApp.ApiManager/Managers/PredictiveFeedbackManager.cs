﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
  public  class PredictiveFeedbackManager : IPredictiveFeedbackManager
    {
        private readonly IPredictiveFeedbackRepository predictivefeedbackRepository;
        public PredictiveFeedbackManager(IPredictiveFeedbackRepository predictivefeedbackRepository)
        {
            this.predictivefeedbackRepository = predictivefeedbackRepository;
        }


        public PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId, CancellationToken cacellationToken)
        {

            return predictivefeedbackRepository.GetPredictiveFeedback(employeeId);
        }
    }
}
