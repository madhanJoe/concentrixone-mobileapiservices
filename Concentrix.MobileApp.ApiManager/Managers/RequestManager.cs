﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Request;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class RequestManager : IRequestManager
    {
        private readonly IRequestRepository requestRepository;

        public RequestManager(IRequestRepository requestRepository)
        {
            this.requestRepository = requestRepository;
        }

        public GetPfResponse GetPfDetails(GetPfRequest pfRequest, string employeeId, CancellationToken cancellationToken)
        {
            GetPfResponse response = ValidateRequest(pfRequest);

            if (response.Acknowledge != AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return requestRepository.GetPfDetails(pfRequest, employeeId);
            }
        }

        public RequestListResponse GetRequestList(string employeeId, CancellationToken cancellationToken)
        {
            return requestRepository.GetRequestList(employeeId);
        }

        #region Validation

        private GetPfResponse ValidateRequest(GetPfRequest pfRequest)
        {
            GetPfResponse pfResponse = new GetPfResponse();

            if (string.IsNullOrEmpty(pfRequest.CODE))
            {
                pfResponse.Acknowledge = AcknowledgeType.Failure;
                pfResponse.Message = "Invalid Inputs";

                pfResponse.ErrorCode.Add(new ErrorDetails { Code = "10201", Description = "Please provide the Code." });
            }
            else
            {
                pfResponse.Acknowledge = AcknowledgeType.Success;
            }

            return pfResponse;
        }

        #endregion
    }
}
