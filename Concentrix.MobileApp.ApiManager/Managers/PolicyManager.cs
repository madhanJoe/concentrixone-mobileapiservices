﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Policy;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
   public class PolicyManager : IPolicyManager
    {
        private readonly IPolicyRepository policyRepository;
        public PolicyManager(IPolicyRepository policyRepository)
        {
            this.policyRepository = policyRepository;
        }

        public AddPolicyResponse RequestPolicy(AddPolicyRequest policyrequest, string employeeId, CancellationToken cancellationToken)
        {
            AddPolicyResponse policyResponse = validatePolicy(policyrequest);
            if (policyResponse.Acknowledge != AcknowledgeType.Success)
            {
                return policyResponse;
            }
            else
            {
                return policyRepository.RequestPolicy(policyrequest,employeeId);
            }
        }


        #region Validation
        private AddPolicyResponse validatePolicy(AddPolicyRequest policyRequest)
        {
            AddPolicyResponse policyResponse = new AddPolicyResponse();

            if (policyRequest.PolicyId <= 0)
            {
                policyResponse.Acknowledge = AcknowledgeType.Failure;
                policyResponse.Message = "Invalid Inputs";

                policyResponse.ErrorCode.Add(new ErrorDetails { Code = "10551", Description = "Please provide the Policy Id." });

            }
            else
            {
                policyResponse.Acknowledge = AcknowledgeType.Success;
            }
            return policyResponse;

        }
        #endregion
    }
}
