﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
 public   class BenefitManager : IBenefitManager
    {
        private readonly IBenefitRepository benefitRepository;
        public BenefitManager(IBenefitRepository benefitRepository)
        {
            this.benefitRepository = benefitRepository;
        }

        public BenefitsResponse GetBenefits(string employeeId, string languageCode, CancellationToken cancellationToken)
        {
            return benefitRepository.GetBenefits(employeeId,languageCode);
        }

       

    }
}
