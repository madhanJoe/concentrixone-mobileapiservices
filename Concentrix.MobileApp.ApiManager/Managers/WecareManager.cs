﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class WecareManager : IWecareManager
    {

        private readonly IWecareRepository wecareRepository;
        public WecareManager(IWecareRepository wecareRepository)
        {
            this.wecareRepository = wecareRepository;
        }

        public CaseCategoryResponse GetCaseCategory(string employeeId, CancellationToken cancellationToken)
        {

            return wecareRepository.GetCaseCategory(employeeId);

        }

        public NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode, CancellationToken cancellationToken)
        {
            NewCaseResponse newcaseResponse = validateCalendar(newcase);
            if (newcaseResponse.Acknowledge != AcknowledgeType.Success)
            {
                return newcaseResponse;
            }
            else
            {
                return wecareRepository.CreateNewCase(newcase,employeeId,languageCode);
            }
        }
        public CaseHistoryResponse GetCaseHistory(string employeeId, CancellationToken cancellationToken)
        {
            return wecareRepository.GetCaseHistory(employeeId);
        }


        #region Validation
        private NewCaseResponse validateCalendar(NewCaseRequest caseRequest)
        {
            NewCaseResponse caseResponse = new NewCaseResponse();

            if (string.IsNullOrEmpty(caseRequest.Category) || string.IsNullOrEmpty(caseRequest.SubCategory)
                || string.IsNullOrEmpty(caseRequest.EmailId) || string.IsNullOrEmpty(caseRequest.Phone)
                || string.IsNullOrEmpty(caseRequest.Subject) || string.IsNullOrEmpty(caseRequest.Details))
            {
                caseResponse.Acknowledge = AcknowledgeType.Failure;
                caseResponse.Message = "Invalid Inputs";

                if (string.IsNullOrEmpty(caseRequest.Category))
                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11001", Description = "Please provide the Category." });
                }
                if (string.IsNullOrEmpty(caseRequest.SubCategory))
                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11002", Description = "Please provide the SubCategory." });
                }

                if (string.IsNullOrEmpty(caseRequest.EmailId))
                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11003", Description = "Please provide the EmailId" });

                }

                if (string.IsNullOrEmpty(caseRequest.Phone))
                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11004", Description = "Please provide the Phone Number" });

                }
                if (string.IsNullOrEmpty(caseRequest.Subject))

                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11005", Description = "Please provide the Subject. " });

                }
                if (string.IsNullOrEmpty(caseRequest.Details))
                {
                    caseResponse.ErrorCode.Add(new ErrorDetails { Code = "11006", Description = "Please provide the Details." });

                }

            }
            else
            {
                caseResponse.Acknowledge = AcknowledgeType.Success;
            }
            return caseResponse;

        }
        #endregion


    }
}
