﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class CallBackManager : ICallBackManager
    {
        private readonly ICallBackRepository callbackRepository;
        public CallBackManager(ICallBackRepository callbackRepository)
        {
            this.callbackRepository = callbackRepository;
        }

        public CallBackResponse RequestCallback(CallBackRequest callback, string employeeId, CancellationToken cancellationToken)
        {
            CallBackResponse response = validateRequest(callback);
            if (response.Acknowledge != AcknowledgeType.Success)
            {
                return response;
            }
            else
            {
                return callbackRepository.RequestCallback(callback, employeeId);
            }
        }

        #region Validation
        private CallBackResponse validateRequest(CallBackRequest callback)
        {
            CallBackResponse callbackResponse = new CallBackResponse();

            if (string.IsNullOrEmpty(callback.CallBack.EmailAddress) || string.IsNullOrEmpty(callback.CallBack.CountryCode))
            {
                callbackResponse.Acknowledge = AcknowledgeType.Failure;
                callbackResponse.Message = "Invalid Inputs";
                if (string.IsNullOrEmpty(callback.CallBack.EmailAddress))
                {
                    callbackResponse.ErrorCode.Add(new ErrorDetails { Code = "10101", Description = "Invalid email format, please provide the valid input. " });
                }
                if (string.IsNullOrEmpty(callback.CallBack.CountryCode))
                {
                    callbackResponse.ErrorCode.Add(new ErrorDetails { Code = "10102", Description = "Please select country code." });
                }
            }

            else
            {
                callbackResponse.Acknowledge = AcknowledgeType.Success;
            }

            return callbackResponse;

        }


        #endregion
    }
}
