﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Test;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class TestManager : ITestManager
    {
        private readonly ITestRepository testRepository;

        public TestManager(ITestRepository testRepository)
        {
            this.testRepository = testRepository;
        }

        public HelloWorld DisplayHelloWorld()
        {
            return testRepository.DisplayHelloWorld();
        }

        public TestLogin AuthenticateUser(string username, string password, CancellationToken cancellationToken)
        {
            return testRepository.AuthenticateUser(username, password);
        }

        public ADResponse AuthenticateLogin(TestLogin login, CancellationToken cancellationToken)
        {
            return testRepository.AuthenticateLogin(login);
        }
    }
}
