﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class RuleEngineManager : IRuleEngineManager
    {
        private readonly IRuleEngineRepository ruleEngineRepository;

        public RuleEngineManager(IRuleEngineRepository ruleEngineRepository)
        {
            this.ruleEngineRepository = ruleEngineRepository;
        }

        public FeatureResponse GenericFeatures(FeatureRequest reqFeature, CancellationToken cacellationToken)
        {
            FeatureResponse featureResponse = ValidateFeatures(reqFeature);

            if (featureResponse.Acknowledge != AcknowledgeType.Success)
            {
                return featureResponse;
            }
            else
            {
                return ruleEngineRepository.GenericFeatures(reqFeature);
            }
        }

        #region Validation
        private FeatureResponse ValidateFeatures(FeatureRequest featureDetail)
        {
            var featuresResponse = new FeatureResponse();

            if (string.IsNullOrEmpty(featureDetail.FeatureCode))
                featuresResponse.ErrorCode.Add(new ErrorDetails { Code = "10659", Description = "Please provide a feature code" });

            if (featuresResponse.ErrorCode.Count > 0)
            {
                featuresResponse.Acknowledge = AcknowledgeType.Failure;
                featuresResponse.Message = "Feature Code is missing";
            }
            else
            {
                featuresResponse.Acknowledge = AcknowledgeType.Success;
            }

            return featuresResponse;
        }
        #endregion
    }
}
