﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Query;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class QueryManager : IQueryManager
    {

        private readonly IQueryRepository queryRepository;
        public QueryManager(IQueryRepository queryRepository)
        {
            this.queryRepository = queryRepository;
        }


        public GetAllQueryForEmployeeResponse GetQueryList(string employeeId, CancellationToken cacellationToken)
        {

            return queryRepository.GetQueryList(employeeId);
        }

        public SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string employeeId, CancellationToken cacellationToken)
        {
            SubmitQueryResponse QueryResponse = validateQuery(submitquery);
            if (QueryResponse.Acknowledge != AcknowledgeType.Success)
            {
                return QueryResponse;
            }
            else
            {

                return queryRepository.SubmitQuery(submitquery,employeeId);
            }
        }

        #region Validation
        private SubmitQueryResponse validateQuery(SubmitQueryRequest queryRequest)
        {
            SubmitQueryResponse queryResponse = new SubmitQueryResponse();

            if (string.IsNullOrEmpty(queryRequest.QueryText))
            {
                queryResponse.Acknowledge = AcknowledgeType.Failure;
                queryResponse.Message = "Invalid Inputs";

                queryResponse.ErrorCode.Add(new ErrorDetails { Code = "10701", Description = "Query text cannot be empty!!" });
               

            }
            else
            {
                queryResponse.Acknowledge = AcknowledgeType.Success;
            }
            return queryResponse;

        }
        #endregion
    }
}
