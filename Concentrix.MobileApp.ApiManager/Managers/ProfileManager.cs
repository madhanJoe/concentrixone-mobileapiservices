﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Helper.Validation;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class ProfileManager : IProfileManager
    {
        private readonly IProfileRepository profileRepository;

        public ProfileManager(IProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public ProfileSaveResponse RegisterUser(RegisterUserRequest register, CancellationToken cacellationToken)
        {
            ProfileSaveResponse registerResponse = ValidateRegisterUser(register);

            if (registerResponse.Acknowledge != AcknowledgeType.Success)
            {
                return registerResponse;
            }
            else
            {
                return profileRepository.RegisterUser(register);
            }
        }

        public ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId, CancellationToken cacellationToken)
        {
            ProfileSaveResponse profileresponse = ValidateSaveProfile(saveprofile);

            if (profileresponse.Acknowledge != AcknowledgeType.Success)
            {
                return profileresponse;
            }
            else
            {
                return profileRepository.SaveProfile(saveprofile, employeeId);
            }
        }

        public ProfileResponse GetProfile(string employeeId, string languageCode, CancellationToken cacellationToken)
        {
            return profileRepository.GetProfile(employeeId, languageCode);
        }

        public ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe, CancellationToken cancellationToken)
        {
            return profileRepository.SaveProfileAboutMe(aboutMe);
        }

        public ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility, CancellationToken cancellationToken)
        {
            return profileRepository.UpdateProfileVisibility(profileVisibility);
        }

        public VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail, CancellationToken cancellationToken)
        {
            var visaresponse = ValidateVisaDetail(visaDetail);

            if (visaresponse.Acknowledge != AcknowledgeType.Success)
            {
                return visaresponse;
            }
            else
            {
                return profileRepository.UpdateVisaDetail(visaDetail);
            }
        }

        public EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile, CancellationToken cancellationToken)
        {
            var editableResponse = ValidateEditableProfile(editableProfile);

            if (editableResponse.Acknowledge != AcknowledgeType.Success)
                return editableResponse;
            else
                return profileRepository.UpdateEditableProfile(editableProfile);
        }

        public LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languageDetail, CancellationToken cancellationToken)
        {
            var validationResponse = ValidateLanguageDetail(languageDetail);

            if (validationResponse.Acknowledge != AcknowledgeType.Success)
                return validationResponse;
            else
            {
                StringBuilder SbLanguageList = new StringBuilder();

                if (languageDetail != null && languageDetail.Languages.Count > 0)
                {
                    SbLanguageList.AppendLine("<?xml version=\"1.0\" ?>");

                    foreach (var lan in languageDetail.Languages)
                    {
                        SbLanguageList.AppendLine("<LanguageData>");
                        SbLanguageList.AppendLine(" <Language>");
                        SbLanguageList.AppendLine("  <EmployeeId>" + languageDetail.EmployeeId + "</EmployeeId>");
                        SbLanguageList.AppendLine("  <LanguageId>" + lan.LanguageId + "</LanguageId>");
                        SbLanguageList.AppendLine("  <LanguageApprovedId>" + lan.LanguageApprovedId + "</LanguageApprovedId>");
                        SbLanguageList.AppendLine("  <Spoken>" + lan.Spoken + "</Spoken>");
                        SbLanguageList.AppendLine("  <Written>" + lan.Written + "</Written>");
                        SbLanguageList.AppendLine(" </Language>");
                        SbLanguageList.AppendLine("</LanguageData>");
                    }
                }

                languageDetail.LanguageXML = SbLanguageList;

                return profileRepository.UpdateLanguageDetail(languageDetail);
            }
        }

        public ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName)
        {
            return profileRepository.UpdateProfileImageName(imageName);
        }

        #region Validation
        private ProfileSaveResponse ValidateRegisterUser(RegisterUserRequest registerUserRequest)
        {
            var validationResponse = new ProfileSaveResponse();

            if (string.IsNullOrEmpty(registerUserRequest.PersonalEmail))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10651", Description = "Please provide a valid Email" });
            else
            {
                if (!Validate.IsValidEmail(registerUserRequest.PersonalEmail))
                    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10662", Description = "Please provide a valid Email" });
            }

            if (registerUserRequest.UserType != 2)
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10652", Description = "Please provide the valid UserType" });

            if (string.IsNullOrEmpty(registerUserRequest.Password))
                validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10653", Description = "Please provide the Password" });

            //    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10663", Description = "Please provide the Mobile Number" });
            //else

            if (registerUserRequest.Mobile != null)
            {
                if (!string.IsNullOrEmpty(registerUserRequest.Mobile.PhoneNumber))
                {
                    if (!Validate.IsValidMobileNumber(registerUserRequest.Mobile.PhoneNumber))
                        validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10664", Description = "Please provide a valid Mobile Number" });
                }

                if (string.IsNullOrEmpty(registerUserRequest.Mobile.ISDCode))
                    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10675", Description = "Please provide a valid ISD Code" });

                if (string.IsNullOrEmpty(registerUserRequest.Mobile.CountryCode))
                    validationResponse.ErrorCode.Add(new ErrorDetails { Code = "10676", Description = "Please provide a valid Country Code" });
            }

            if (validationResponse.ErrorCode.Count > 0)
            {
                validationResponse.Acknowledge = AcknowledgeType.Failure;
                validationResponse.Message = "Missing inputs for Registering the user";
            }
            else
            {
                validationResponse.Acknowledge = AcknowledgeType.Success;
            }

            return validationResponse;
        }

        private ProfileSaveResponse ValidateSaveProfile(GuestUserInfoRequest saveRequest)
        {
            ProfileSaveResponse saveResponse = new ProfileSaveResponse();

            if (!Validate.IsValidMobileNumber(saveRequest.Mobile))
                saveResponse.ErrorCode.Add(new ErrorDetails { Code = "10654", Description = "Please provide a valid Mobile Number" });

            if ((string.IsNullOrEmpty(saveRequest.PersonalEmail)) || (string.IsNullOrEmpty(saveRequest.OfficeEmail)))
                saveResponse.ErrorCode.Add(new ErrorDetails { Code = "10655", Description = "Please provide a valid personal or office Email." });
            else
            {
                if (!Validate.IsValidEmail(saveRequest.PersonalEmail))
                    saveResponse.ErrorCode.Add(new ErrorDetails { Code = "10656", Description = "Please provide a valid Personal Email" });

                if (!Validate.IsValidEmail(saveRequest.OfficeEmail))
                    saveResponse.ErrorCode.Add(new ErrorDetails { Code = "10657", Description = "Please provide a valid Office Email" });
            }

            if (saveResponse.ErrorCode.Count > 0)
            {
                saveResponse.Acknowledge = AcknowledgeType.Failure;
                saveResponse.Message = "Invalid Inputs";
            }
            else
            {
                saveResponse.Acknowledge = AcknowledgeType.Success;
            }

            return saveResponse;
        }

        private VisaDetailResponse ValidateVisaDetail(VisaDetailRequest visaDetail)
        {
            var visaResponse = new VisaDetailResponse();

            if ((visaDetail.Mode == ModeType.Update || visaDetail.Mode == ModeType.Delete) && (visaDetail.VisaId <= 0))
                visaResponse.ErrorCode.Add(new ErrorDetails { Code = "10658", Description = "Please provide a valid VisaId" });

            if (string.IsNullOrEmpty(visaDetail.VisaCountryCode))
                visaResponse.ErrorCode.Add(new ErrorDetails { Code = "10659", Description = "Please provide a valid Country" });

            if (string.IsNullOrEmpty(visaDetail.TypeOfVisa))
                visaResponse.ErrorCode.Add(new ErrorDetails { Code = "10660", Description = "Please provide a valid Visa Type" });

            DateTime validDate;

            if (!DateTime.TryParse(visaDetail.VisaExpiryDate, out validDate))
                visaResponse.ErrorCode.Add(new ErrorDetails { Code = "10661", Description = "Please provide a valid Visa Expiry Date" });

            if (string.IsNullOrEmpty(visaDetail.VisaCountryName))
                visaResponse.ErrorCode.Add(new ErrorDetails { Code = "10674", Description = "Please provide a valid Visa Country Name" });

            if (visaResponse.ErrorCode.Count > 0)
            {
                visaResponse.Acknowledge = AcknowledgeType.Failure;
                visaResponse.Message = "Visa related information is missing";
            }
            else
                visaResponse.Acknowledge = AcknowledgeType.Success;

            return visaResponse;
        }

        private LanguageDetailResponse ValidateLanguageDetail(LanguageDetailRequest languageDetail)
        {
            var languageResponse = new LanguageDetailResponse();

            if (languageDetail.Languages != null)
            {
                foreach(var lang in languageDetail.Languages)
                {
                    if (lang.LanguageId <= 0)
                    {
                        languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10667", Description = "Please provide a valid Language Id" });
                    }

                    if (string.IsNullOrEmpty(lang.LanguageName))
                    {
                        languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10668", Description = "Please provide a valid Language Name" });
                    }

                    if (string.IsNullOrEmpty(lang.Spoken))
                    {
                        languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10669", Description = "Please provide a valid spoken language" });
                    }
                    else
                    {
                        if (!((lang.Spoken.ToLower() == "true") || (lang.Spoken.ToLower() == "false")))
                        {
                            languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10670", Description = "Not a valid value for spoken language" });
                        }
                    }

                    if (string.IsNullOrEmpty(lang.Written))
                    {
                        languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10671", Description = "Please provide a valid written language" });
                    }
                    else
                    {
                        if (!((lang.Written.ToLower() == "true") || (lang.Written.ToLower() == "false")))
                        {
                            languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10672", Description = "Not a valid value for written language" });
                        }
                    }
                }
            }
            else
                languageResponse.ErrorCode.Add(new ErrorDetails { Code = "10673", Description = "Please provide a valid entries for language" });

            if (languageResponse.ErrorCode.Count > 0)
            {
                languageResponse.Acknowledge = AcknowledgeType.Failure;
                languageResponse.Message = "Language related information is missing";
            }
            else
            {
                languageResponse.Acknowledge = AcknowledgeType.Success;
            }

            return languageResponse;
        }

        private EditableProfileResponse ValidateEditableProfile(EditableProfileRequest editableRequest)
        {
            var editableResponse = new EditableProfileResponse();

            if (editableRequest.ContactNumber != null)
            {
                //    editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10681", Description = "Please provide the Phone Number" });
                //else

                if (!string.IsNullOrEmpty(editableRequest.ContactNumber.PhoneNumber))
                {
                    if (!Validate.IsValidMobileNumber(editableRequest.ContactNumber.PhoneNumber))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10682", Description = "Please provide a valid Phone Number" });

                    if (string.IsNullOrEmpty(editableRequest.ContactNumber.ISDCode))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10675", Description = "Please provide a valid ISD Code" });

                    if (string.IsNullOrEmpty(editableRequest.ContactNumber.CountryCode))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10676", Description = "Please provide a valid Country Code" });
                }
            }

            if (editableRequest.EmergencyContactNumber != null)
            {
                //    editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10677", Description = "Please provide the Emergency Phone Number" });
                //else

                if (!string.IsNullOrEmpty(editableRequest.EmergencyContactNumber.PhoneNumber))
                {
                    if (!Validate.IsValidMobileNumber(editableRequest.EmergencyContactNumber.PhoneNumber))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10678", Description = "Please provide a valid Emergency Phone Number" });

                    if (string.IsNullOrEmpty(editableRequest.EmergencyContactNumber.ISDCode))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10679", Description = "Please provide a valid Emergency ISD Code" });

                    if (string.IsNullOrEmpty(editableRequest.EmergencyContactNumber.CountryCode))
                        editableResponse.ErrorCode.Add(new ErrorDetails { Code = "10680", Description = "Please provide a valid Emergency Country Code" });
                }
            }

            if (editableResponse.ErrorCode.Count > 0)
            {
                editableResponse.Acknowledge = AcknowledgeType.Failure;
                editableResponse.Message = "Profile related information is missing";
            }
            else
            {
                editableResponse.Acknowledge = AcknowledgeType.Success;
            }

            return editableResponse;
        }
        #endregion
    }
}
