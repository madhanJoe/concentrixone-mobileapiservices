﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;

using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
   public class TrainingCalendarManager : ITrainingCalendarManager
    {
        private readonly ITrainingCalendarRepository trainingRepository;
        public TrainingCalendarManager(ITrainingCalendarRepository trainingRepository)
        {
            this.trainingRepository = trainingRepository;
        }

        public TrainingCalendarResponse GetTrainingCalendarList(string employeeId, CancellationToken cancellationToken)
        {
            return trainingRepository.GetTrainingCalendarList(employeeId);
        }
        public TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails, CancellationToken cancellationToken)
        {
            TrainingCalendarDetailsResponse calendarResponse = validateCalendar(trainingcalendardetails);
            if (calendarResponse.Acknowledge != AcknowledgeType.Success)
            {
                return calendarResponse;
            }
            else
            {
                return trainingRepository.GetTrainingCalendarDetails(trainingcalendardetails);
            }
        }
        #region Validation
        private TrainingCalendarDetailsResponse validateCalendar(TrainingCalendarDetailsRequest calendarRequest)
        {
            TrainingCalendarDetailsResponse calendarResponse = new TrainingCalendarDetailsResponse();

            if (string.IsNullOrEmpty(calendarRequest.TrainingCalendarId))
            {
                calendarResponse.Acknowledge = AcknowledgeType.Failure;
                calendarResponse.Message = "Invalid Inputs";

                calendarResponse.ErrorCode.Add(new ErrorDetails { Code = "10901", Description = "Please provide the TrainingCalendarId." });


            }
            else
            {
                calendarResponse.Acknowledge = AcknowledgeType.Success;
            }
               
            return calendarResponse;

        }
        #endregion
    }
}
