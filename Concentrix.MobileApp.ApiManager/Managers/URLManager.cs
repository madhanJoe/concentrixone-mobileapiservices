﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.URL;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
  public  class URLManager : IURLManager
    {
        private readonly IURLRepository urlRepository;
        public URLManager(IURLRepository urlRepository)
        {
            this.urlRepository = urlRepository;
        }

        public URLResponse GetURL(URLRequest url, CancellationToken cancellationToken)
        {
            return urlRepository.GetURL(url);
        }
    }
}
