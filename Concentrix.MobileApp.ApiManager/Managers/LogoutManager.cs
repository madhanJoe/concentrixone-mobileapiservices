﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Logout;
using Concentrix.MobileApp.Helper.Validation;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class LogoutManager : ILogoutManager
    {
        private readonly ILogoutRepository logoutRepository;
        public LogoutManager(ILogoutRepository logoutRepository)
        {
            this.logoutRepository = logoutRepository;
        }

        public LogoutResponse LogoutDevice(LogoutRequest logout, string employeeId, CancellationToken cancellationToken)
        {
            LogoutResponse deviceResponse = ValidateLogout(logout);

            if (deviceResponse.Acknowledge != AcknowledgeType.Success)
                return deviceResponse;
            else
                return logoutRepository.LogoutDevice(logout, employeeId);
        }

        #region Validation
        private LogoutResponse ValidateLogout(LogoutRequest devicerequest)
        {
            LogoutResponse deviceResponse = new LogoutResponse();

            if (!(devicerequest.UserType >= 1 && devicerequest.UserType <= 2))
                deviceResponse.ErrorCode.Add(new ErrorDetails { Code = "10353", Description = "Not a valid UserType" });

            if (devicerequest.UserType == 2)
            {
                if (string.IsNullOrEmpty(devicerequest.EmailId))
                    deviceResponse.ErrorCode.Add(new ErrorDetails { Code = "10351", Description = "Please provide the EmailId" });
                else if (!Validate.IsValidEmail(devicerequest.EmailId))
                    deviceResponse.ErrorCode.Add(new ErrorDetails { Code = "10354", Description = "Please provide a valid EmailId" });
            }

            if (string.IsNullOrEmpty(devicerequest.DeviceId))
            {
                deviceResponse.ErrorCode.Add(new ErrorDetails { Code = "10352", Description = "Please provide the DeviceId." });
            }

            if (deviceResponse.ErrorCode.Count > 0)
            {
                deviceResponse.Acknowledge = AcknowledgeType.Failure;
                deviceResponse.Message = "Invalid Inputs";
            }
            else
            {
                deviceResponse.Acknowledge = AcknowledgeType.Success;
            }

            return deviceResponse;
        }

        #endregion
    }
}
