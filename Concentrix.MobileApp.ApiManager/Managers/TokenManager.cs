﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using System.Threading;
using Concentrix.MobileApp.Repository.Interfaces;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.Domain.DomainModels.Log;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class TokenManager : ITokenManager
    {
        private readonly ITokenRepository tokenRepository;

        public TokenManager()
        {
            tokenRepository = DIServiceLocator.Resolve<ITokenRepository>();
        }

        public TokenDetailsResponse GetTokenDetails(TokenRequest token)
        {
            return tokenRepository.GetTokenDetails(token);
        }
        
        public TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog)
        {
            return tokenRepository.ValidateAppNameToken(token, serviceLog);
        }

        public void UpdateServiceLog(long serviceLogId, string acknowledge, string message)
        {
            tokenRepository.UpdateServiceLog(serviceLogId, acknowledge, message);
        }

        public int LogToDatabase(ErrorLog errorLog)
        {
            return tokenRepository.LogToDatabase(errorLog);
        }
    }
}
