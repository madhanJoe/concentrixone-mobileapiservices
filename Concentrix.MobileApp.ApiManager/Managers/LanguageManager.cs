﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.Language;
using Concentrix.MobileApp.Repository.Interfaces;
using Concentrix.MobileApp.Repository.Repositories;
using System.Threading;

namespace Concentrix.MobileApp.ApiManager.Managers
{
    public class LanguageManager : ILanguageManager
    {
        private readonly ILanguageRepository languageRepository;

        public LanguageManager(ILanguageRepository LanguageRepository)
        {
            languageRepository = LanguageRepository;
        }

        public LanguageResponse GetLanguages(LanguageRequest langugae, CancellationToken cancellationToken)
        {
            return languageRepository.GetLanguages(langugae);
        }
    }
}
