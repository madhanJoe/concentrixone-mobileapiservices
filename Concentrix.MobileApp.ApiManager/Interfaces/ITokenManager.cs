﻿using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface ITokenManager
    {
        TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog);
        TokenDetailsResponse GetTokenDetails(TokenRequest token);
        void UpdateServiceLog(long serviceLogId, string acknowledge, string message);
        int LogToDatabase(ErrorLog errorLog);
    }
}
