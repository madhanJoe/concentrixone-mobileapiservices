﻿using Concentrix.MobileApp.Domain.DomainModels.URL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface IURLManager
    {
        URLResponse GetURL(URLRequest url, CancellationToken cacellationToken);

    }
}
