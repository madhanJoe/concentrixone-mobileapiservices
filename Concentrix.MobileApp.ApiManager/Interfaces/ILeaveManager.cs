﻿using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
 public   interface ILeaveManager
    {
        LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leavebalance,string employeeId, CancellationToken cacellationToken);

    }
}
