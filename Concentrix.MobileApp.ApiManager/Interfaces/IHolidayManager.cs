﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface IHolidayManager
    {
        HolidayResponse GetHolidays(string employeeId, string LanguageCode, CancellationToken cacellationToken);

    }
}
