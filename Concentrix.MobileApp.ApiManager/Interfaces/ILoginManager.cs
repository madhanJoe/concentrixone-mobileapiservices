﻿using Concentrix.MobileApp.Domain.DomainModels.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface ILoginManager
    {
        LoginADResponse ADAuthentication(LoginADRequest Login, CancellationToken cancellationToken);
        LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial, CancellationToken cacellationToken);
        LoginADResponse ValidateGuest(LoginADRequest guestlogin, CancellationToken cacellationToken);
        ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotpassword, CancellationToken cacellationToken);
    }
}
