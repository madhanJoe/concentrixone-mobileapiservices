﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
  public  interface ILocationManager
    {
       LocationResponse GetLocations(string employeeId, string locationCode, string languageCode, CancellationToken cacellationToken);

    }
}
