﻿using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface IAttributeManager
    {
        AttributeResponse GetDemandLetterList(AttributeRequest attributevalue, string employeeId, CancellationToken cacellationToken);

        CountryResponse GetCountryCode(CountryRequest country, CancellationToken cacellationToken);

        AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId, CancellationToken cacellationToken);


    }
}
