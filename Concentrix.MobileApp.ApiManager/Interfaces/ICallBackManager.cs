﻿using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface ICallBackManager
    {
        CallBackResponse RequestCallback(CallBackRequest callback,string employeeId, CancellationToken cacellationToken);
    }
}
