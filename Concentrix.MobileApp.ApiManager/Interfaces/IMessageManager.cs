﻿using Concentrix.MobileApp.Domain.DomainModels.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface IMessageManager
    {
        DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple,string employeeId, CancellationToken cacellationToken);

        DeleteAllMessagesResponse DeleteAllMessage(string employeeId, CancellationToken cacellationToken);

        DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage, CancellationToken cacellationToken);

        UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId, CancellationToken cacellationToken);

        MessageResponse GetMessage(MessageRequest message, string employeeId, CancellationToken cacellationToken);

    }

}
