﻿using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface IQuizManager
    {
        PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string employeeId, CancellationToken cacellationToken);

        GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string employeeId, CancellationToken cacellationToken);

        SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string employeeId, CancellationToken cacellationToken);

        QuizSummaryResponse QuizSummary(QuizSummaryRequest quizsummary, string employeeId, CancellationToken cacellationToken);

    }
}
