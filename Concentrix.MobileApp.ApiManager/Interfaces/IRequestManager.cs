﻿using Concentrix.MobileApp.Domain.DomainModels.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
  public  interface IRequestManager
    {
        GetPfResponse GetPfDetails(GetPfRequest pf,string employeeId, CancellationToken cacellationToken);
        RequestListResponse GetRequestList(string employeeId, CancellationToken cacellationToken);


    }
}
