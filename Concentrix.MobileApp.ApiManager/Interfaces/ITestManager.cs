﻿using Concentrix.MobileApp.Domain.DomainModels.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface ITestManager
    {
        HelloWorld DisplayHelloWorld();
        TestLogin AuthenticateUser(string username, string password, CancellationToken cancellationToken);
        ADResponse AuthenticateLogin(TestLogin login, CancellationToken cancellationToken);
    }
}
