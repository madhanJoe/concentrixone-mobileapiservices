﻿using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
  public  interface IMoodMeterManager
    {
       MoodMeterResponse GetMoodMeterAttributes(string employeeId, CancellationToken cacellationToken);

        SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating, string employeeId, CancellationToken cacellationToken);


        
    }
}
