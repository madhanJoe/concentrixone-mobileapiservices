﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface ITrainingCalendarManager
    {
    TrainingCalendarResponse GetTrainingCalendarList(string employeeId, CancellationToken cacellationToken);
        TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails, CancellationToken cacellationToken);

    }
}
