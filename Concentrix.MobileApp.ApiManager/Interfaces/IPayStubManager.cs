﻿using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
  public  interface IPayStubManager
    {
        PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId, CancellationToken cancellationToken);

        PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId, CancellationToken cancellationToken);
        PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId, CancellationToken cancellationToken);
    }
}
