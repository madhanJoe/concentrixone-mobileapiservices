﻿using Concentrix.MobileApp.Domain.DomainModels.Logout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface ILogoutManager
    {
        LogoutResponse LogoutDevice(LogoutRequest logout,string employeeId, CancellationToken cacellationToken);

    }
}
