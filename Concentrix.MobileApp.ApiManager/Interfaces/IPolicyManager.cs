﻿using Concentrix.MobileApp.Domain.DomainModels.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface IPolicyManager
    {
        AddPolicyResponse RequestPolicy(AddPolicyRequest ploicyrequest, string employeeId, CancellationToken cacellationToken);

    }
}
