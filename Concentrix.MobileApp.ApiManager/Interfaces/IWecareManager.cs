﻿using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
  public  interface IWecareManager
    {
        CaseCategoryResponse GetCaseCategory(string employeeId, CancellationToken cacellationToken);

        NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode, CancellationToken cacellationToken);

        CaseHistoryResponse GetCaseHistory(string employeeId, CancellationToken cacellationToken);

    }
}
