﻿using Concentrix.MobileApp.Domain.DomainModels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
 public   interface IQueryManager
    {
        GetAllQueryForEmployeeResponse GetQueryList(string employeeId, CancellationToken cacellationToken);

        SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string employeeId, CancellationToken cacellationToken);

    }
}
