﻿using Concentrix.MobileApp.Domain.DomainModels.Survey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface ISurveyManager
    {

        PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId, CancellationToken cacellationToken);

        PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId, CancellationToken cacellationToken);

        GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId, CancellationToken cacellationToken);

        SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId, CancellationToken cacellationToken);


    }
}
