﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;
using System.Threading;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface IFeatureDepartmentManager
    {
        GetFeatureDepartmentResponse GetSurveyDeparments(string EmployeeId, string LanguageCode, CancellationToken cacellationToken);
    }
}
