﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface ITrainingVisibleManager
    {
        TrainingVisibleResponse TrainingVisible(string employeeId, CancellationToken cacellationToken);

    }
}
