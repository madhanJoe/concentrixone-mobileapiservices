﻿using Concentrix.MobileApp.Domain.DomainModels.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface IProfileManager
    {
        ProfileSaveResponse RegisterUser(RegisterUserRequest register, CancellationToken cancellationToken);
        ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId, CancellationToken cancellationToken);
        ProfileResponse GetProfile(string employeeId, string languageCode, CancellationToken cancellationToken);
        ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe, CancellationToken cancellationToken);
        ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility, CancellationToken cancellationToken);
        VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail, CancellationToken cancellationToken);
        EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile, CancellationToken cancellationToken);
        LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languagedetail, CancellationToken cancellationToken);
        ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName);
    }
}
