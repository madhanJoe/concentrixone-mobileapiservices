﻿using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
   public interface IPredictiveFeedbackManager
    {

        PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId, CancellationToken cancellationtoken);
    }
}
