﻿using Concentrix.MobileApp.Domain.DomainModels.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface ILanguageManager
    {
        LanguageResponse GetLanguages(LanguageRequest language, CancellationToken cancellationToken);
    }
}
