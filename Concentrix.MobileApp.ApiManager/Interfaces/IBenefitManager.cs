﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.ApiManager.Interfaces
{
    public interface IBenefitManager
    {
        BenefitsResponse GetBenefits(string employeeId, string languageCode, CancellationToken cacellationToken);

    }
}
