﻿using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using Concentrix.MobileApp.Domain.DomainModels.Language;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Login;
using Concentrix.MobileApp.Domain.DomainModels.Logout;
using Concentrix.MobileApp.Domain.DomainModels.Message;
using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using Concentrix.MobileApp.Domain.DomainModels.Policy;
using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Domain.DomainModels.Query;
using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using Concentrix.MobileApp.Domain.DomainModels.Request;
using Concentrix.MobileApp.Domain.DomainModels.Survey;
using Concentrix.MobileApp.Domain.DomainModels.Test;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;
using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using Concentrix.MobileApp.Domain.DomainModels.URL;
using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IEntityFrameDBAccess
    {
        HelloWorld DisplayHelloWorld();

        TestLogin AuthenticateUser(string username, string password);

        ADResponse AuthenticateLogin(TestLogin login);

        TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog);

        void UpdateServiceLog(long serviceLogId, string acknowledge, string message);

        int LogToDatabase(ErrorLog errorLog);

        TokenDetailsResponse GetTokenDetails(TokenRequest token);

        LoginADResponse GenerateToken(LoginADRequest login);

        PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId);

        PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId);

        PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId);

        AttributeResponse GetDemandLetterList(AttributeRequest attribute, string employeeId);

        TrainingVisibleResponse TrainingVisible(string employeeId);

        TrainingCalendarResponse GetTrainingCalendarList(string employeeId);

        TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails);

        MoodMeterResponse GetMoodMeterAttributes(string employeeId);

        SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating, string employeeId);

        PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string employeeId);

        GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string employeeId);

        SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string employeeId);

        QuizSummaryResponse QuizSummary(QuizSummaryRequest updatequizanswer, string employeeId);

        GetAllQueryForEmployeeResponse GetQueryList(string employeeId);

        SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string employeeId);

        PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId);

        PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId);

        PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId);

        GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId);

        SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId);

        BenefitsResponse GetBenefits(string employeeId, string languageCode);

        CaseCategoryResponse GetCaseCategory(string employeeId);

        NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode);

        CaseHistoryResponse GetCaseHistory(string employeeId);

        LocationResponse GetLocations(string employeeId, string locationCode, string languageCode);
        CountryResponse GetCountryCode(CountryRequest country);

        AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId);

        AddPolicyResponse RequestPolicy(AddPolicyRequest policyrequest, string employeeId);

        DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple, string employeeId);

        DeleteAllMessagesResponse DeleteAllMessage(string employeeID);

        DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage);

        UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId);

        MessageResponse GetMessage(MessageRequest message, string employeeId);

        ProfileSaveResponse RegisterUser(RegisterUserRequest register);

        ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId);
        ProfileResponse GetProfile(string EmployeeId, string LanguageCode);
        ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe);
        ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility);
        VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail);
        EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile);
        ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName);

        URLResponse GetURL(URLRequest url);

        LogoutResponse LogoutDevice(LogoutRequest logout, string employeeID);

        CallBackResponse RequestCallback(CallBackRequest callback, string employeeId);

        LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial);
        LoginADResponse ValidateUser(LoginADRequest login);
        LoginADResponse ValidateGuest(LoginADRequest guestlogin);
        ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotpassword);

        GetPfResponse GetPfDetails(GetPfRequest pf, string employeeId);

        RequestListResponse GetRequestList(string employeeId);

        LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leavebalance, string employeeId);

        HolidayResponse GetHolidays(string employeeId, string LanguageCode);
        LanguageResponse GetLanguages(LanguageRequest language);
        LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languagedetail);
        FeatureResponse GenericFeatures(FeatureRequest feature);
        GetFeatureDepartmentResponse GetSurveyDeparments(string employeeId, string LanguageCode);
    }
}
