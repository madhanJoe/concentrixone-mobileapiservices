﻿using Concentrix.MobileApp.Domain.DomainModels.URL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
  public  interface IURLRepository
    {
        URLResponse GetURL(URLRequest url);
    }
}
