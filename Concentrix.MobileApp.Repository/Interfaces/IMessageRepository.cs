﻿using Concentrix.MobileApp.Domain.DomainModels.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface IMessageRepository
    {
        DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple, string employeeId);

        DeleteAllMessagesResponse DeleteAllMessage(string employeeID);

        DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage);

        UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId);

        MessageResponse GetMessage(MessageRequest message, string employeeId);

    }
}
