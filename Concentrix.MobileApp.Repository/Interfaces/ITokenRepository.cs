﻿using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface ITokenRepository
    {
        TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog);
        TokenDetailsResponse GetTokenDetails(TokenRequest token);
        void UpdateServiceLog(long serviceLogId, string acknowledge, string message);
        int LogToDatabase(ErrorLog errorLog);
    }
}
