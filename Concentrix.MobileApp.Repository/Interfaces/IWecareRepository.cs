﻿using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
  public  interface IWecareRepository
    {
        CaseCategoryResponse GetCaseCategory(string employeeId);

        NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode);

        CaseHistoryResponse GetCaseHistory(string employeeId);

    }
}
