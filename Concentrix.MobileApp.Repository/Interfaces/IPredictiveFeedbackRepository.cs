﻿using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
  public  interface IPredictiveFeedbackRepository
    {

        PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId);
    }
}
