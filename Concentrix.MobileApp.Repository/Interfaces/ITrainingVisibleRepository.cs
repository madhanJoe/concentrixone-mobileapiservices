﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface ITrainingVisibleRepository
    {
        TrainingVisibleResponse TrainingVisible(string employeeId);

    }
}
