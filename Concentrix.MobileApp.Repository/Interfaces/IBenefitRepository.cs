﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
 public   interface IBenefitRepository
    {
        BenefitsResponse GetBenefits(string employeeId, string languageCode);

    }
}
