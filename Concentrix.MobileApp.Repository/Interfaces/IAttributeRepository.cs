﻿using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IAttributeRepository
    {
        AttributeResponse GetDemandLetterList(AttributeRequest paystub, string employeeId);
        CountryResponse GetCountryCode(CountryRequest country);
        AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId);
    }
}
