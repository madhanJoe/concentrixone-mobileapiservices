﻿using Concentrix.MobileApp.Domain.DomainModels.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IProfileRepository
    {
        ProfileSaveResponse RegisterUser(RegisterUserRequest register);
        ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId);
        ProfileResponse GetProfile(string employeeId, string languageCode);
        ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe);
        ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility);
        VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail);
        EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile);
        LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languagedetail);
        ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName);
    }
}
