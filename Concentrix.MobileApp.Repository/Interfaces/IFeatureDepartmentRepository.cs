﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IFeatureDepartmentRepository
    {
        GetFeatureDepartmentResponse GetSurveyDeparments(string employeeId, string LanguageCode);
    }
}
