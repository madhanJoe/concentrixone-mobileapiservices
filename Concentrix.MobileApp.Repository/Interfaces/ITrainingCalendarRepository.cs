﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface ITrainingCalendarRepository
    {
        TrainingCalendarResponse GetTrainingCalendarList(string employeeId);

        TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails);

    }
}
