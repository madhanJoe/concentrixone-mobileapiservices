﻿using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IRuleEngineRepository
    {
        FeatureResponse GenericFeatures(FeatureRequest reqFeature);
    }
}
