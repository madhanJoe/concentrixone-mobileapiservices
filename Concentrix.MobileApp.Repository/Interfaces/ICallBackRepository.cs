﻿using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface ICallBackRepository
    {
        CallBackResponse RequestCallback(CallBackRequest callback, string employeeId);

    }
}
