﻿using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface ILeaveRepository
    {
        LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leavebalance, string employeeId);


    }
}
