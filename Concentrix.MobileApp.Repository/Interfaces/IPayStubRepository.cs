﻿using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface IPayStubRepository
    {
        PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId);

        PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId);
        PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId);
    }
}
