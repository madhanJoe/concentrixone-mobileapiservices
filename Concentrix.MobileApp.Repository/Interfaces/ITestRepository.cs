﻿using Concentrix.MobileApp.Domain.DomainModels.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface ITestRepository
    {
        HelloWorld DisplayHelloWorld();
        TestLogin AuthenticateUser(string username, string password);
        ADResponse AuthenticateLogin(TestLogin login);
    }
}
