﻿using Concentrix.MobileApp.Domain.DomainModels.Survey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface ISurveyRepository
    {
        PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId);
        PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId);
        GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId);
        SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId);



    }
}
