﻿using Concentrix.MobileApp.Domain.DomainModels.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface IPolicyRepository
    {
        AddPolicyResponse RequestPolicy(AddPolicyRequest policyrequest,string employeeId);
    }
}
