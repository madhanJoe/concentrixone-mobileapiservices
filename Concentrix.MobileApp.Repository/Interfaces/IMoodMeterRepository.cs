﻿using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface IMoodMeterRepository
    {
        MoodMeterResponse GetMoodMeterAttributes(string employeeId);

        SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating, string employeeId);


    }
}
