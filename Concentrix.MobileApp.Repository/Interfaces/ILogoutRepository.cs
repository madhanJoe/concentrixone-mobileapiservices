﻿using Concentrix.MobileApp.Domain.DomainModels.Logout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
  public  interface ILogoutRepository
    {
        LogoutResponse LogoutDevice(LogoutRequest logout, string employeeId);

    }
}
