﻿using Concentrix.MobileApp.Domain.DomainModels.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IRequestRepository 
    {
        GetPfResponse GetPfDetails(GetPfRequest pf, string EmployeeId);

        RequestListResponse GetRequestList(string EmployeeId);

    }
}
