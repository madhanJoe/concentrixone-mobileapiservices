﻿using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface IQuizRepository
    {
        PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string EmployeeId);
        GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string EmployeeId);

        SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string EmployeeId);
        QuizSummaryResponse QuizSummary(QuizSummaryRequest updatequizanswer, string EmployeeId);



    }
}
