﻿using Concentrix.MobileApp.Domain.DomainModels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
   public interface IQueryRepository
    {
        GetAllQueryForEmployeeResponse GetQueryList(string EmployeeId);

        SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string EmployeeId);

    }
}
