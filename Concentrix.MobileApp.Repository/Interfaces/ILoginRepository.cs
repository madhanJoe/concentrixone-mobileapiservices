﻿using Concentrix.MobileApp.Domain.DomainModels.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Interfaces
{
    public interface ILoginRepository
    {
        LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial);
        LoginADResponse ValidateUser(LoginADRequest login);
        LoginADResponse ValidateGuest(LoginADRequest guestlogin);
        ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotpassword);
        LoginADResponse GenerateToken(LoginADRequest login);
    }
}
