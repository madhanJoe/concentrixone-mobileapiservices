﻿using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class PayStubRepository : IPayStubRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public PayStubRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId)
        {
            return entityDBAccess.GetPaySlip(payslip, employeeId);
        }

        public PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId)
        {
            return entityDBAccess.GetDemandLetter(demandletter, employeeId);
        }

        public PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId)
        {
            return entityDBAccess.GetPayStubDetails(paystub,employeeId);
        }
    }
}
