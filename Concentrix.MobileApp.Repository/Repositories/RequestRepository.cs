﻿using Concentrix.MobileApp.Domain.DomainModels.Request;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class RequestRepository : IRequestRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public RequestRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public GetPfResponse GetPfDetails(GetPfRequest pf, string employeeId)
        {
            return entityDBAccess.GetPfDetails(pf, employeeId);
        }
        public RequestListResponse GetRequestList(string employeeId)
        {
            return entityDBAccess.GetRequestList(employeeId);
        }
    }
}
