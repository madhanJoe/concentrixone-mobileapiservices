﻿using Concentrix.MobileApp.Domain.DomainModels.URL;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
 public   class URLRepository : IURLRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public URLRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public URLResponse GetURL(URLRequest url)
        {
            return entityDBAccess.GetURL(url);
        }
    }
}
