﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
 public   class LocationRepository : ILocationRepository
    {

        private readonly IEntityFrameDBAccess entityDBAccess;

        public LocationRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public LocationResponse GetLocations(string employeeId, string locationCode, string languageCode)

        {
            return entityDBAccess.GetLocations(employeeId,locationCode, languageCode);
        }
    }
}
