﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class FeatureDepartmentRepository : IFeatureDepartmentRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public FeatureDepartmentRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public GetFeatureDepartmentResponse GetSurveyDeparments(string employeeId, string LanguageCode)
        {
            return entityDBAccess.GetSurveyDeparments(employeeId, LanguageCode);
        }
    }
}
