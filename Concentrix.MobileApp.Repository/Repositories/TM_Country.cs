//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Country
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Geography_Code { get; set; }
        public Nullable<System.DateTime> Creation_Date { get; set; }
        public string Creation_User_Id { get; set; }
        public Nullable<System.Guid> OneviewId { get; set; }
        public string Active { get; set; }
        public string ISDCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
