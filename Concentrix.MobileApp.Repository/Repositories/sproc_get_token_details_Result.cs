//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    
    public partial class sproc_get_token_details_Result
    {
        public int TokenId { get; set; }
        public string TokenValue { get; set; }
        public string UserId { get; set; }
        public int UserType { get; set; }
        public string LanguageCode { get; set; }
        public string DeviceId { get; set; }
        public string LoginType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public System.DateTime TokenCreatedOn { get; set; }
        public System.DateTime TokenExpiresOn { get; set; }
        public string MobileRegion { get; set; }
        public string MobileCountry { get; set; }
        public string MobileCity { get; set; }
        public bool IsActive { get; set; }
    }
}
