﻿using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Repository.Interfaces;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class ProfileRepository : IProfileRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public ProfileRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public ProfileSaveResponse RegisterUser(RegisterUserRequest register)
        {
            return entityDBAccess.RegisterUser(register);
        }

        public ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId)
        {
            return entityDBAccess.SaveProfile(saveprofile,employeeId);
        }

        public ProfileResponse GetProfile(string employeeId, string languageCode)
        {
            return entityDBAccess.GetProfile(employeeId, languageCode);
        }

        public ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe)
        {
            return entityDBAccess.SaveProfileAboutMe(aboutMe);
        }

        public ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility)
        {
            return entityDBAccess.UpdateProfileVisibility(profileVisibility);
        }

        public VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail)
        {
            return entityDBAccess.UpdateVisaDetail(visaDetail);
        }

        public EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile)
        {
            return entityDBAccess.UpdateEditableProfile(editableProfile);
        }

        public LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languagedetail)
        {
            return entityDBAccess.UpdateLanguageDetail(languagedetail);
        }

        public ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName)
        {
            return entityDBAccess.UpdateProfileImageName(imageName);
        }
    }
}
