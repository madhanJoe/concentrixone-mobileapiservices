﻿using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class CallBackRepository : ICallBackRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public CallBackRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public CallBackResponse RequestCallback(CallBackRequest callback, string employeeId)

        {

            //waiting for Genesys entity 
            return entityDBAccess.RequestCallback(callback,employeeId);
        }
    }
}
