﻿using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
  public  class PredictiveFeedbackRepository : IPredictiveFeedbackRepository
    {

        private readonly IEntityFrameDBAccess entityDBAccess;

        public PredictiveFeedbackRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId)
        {
            return entityDBAccess.GetPredictiveFeedback(employeeId);
        }
    }
}
