﻿using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
  public  class WecareRepository : IWecareRepository
    {

        private readonly IEntityFrameDBAccess entityDBAccess;

        public WecareRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public CaseCategoryResponse GetCaseCategory(string employeeId)
        {
            return entityDBAccess.GetCaseCategory(employeeId);
        }
        public NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode)
        {
            return entityDBAccess.CreateNewCase(newcase, employeeId,languageCode);
        }

        public CaseHistoryResponse GetCaseHistory(string employeeId)
        {
            return entityDBAccess.GetCaseHistory(employeeId);
        }
    }
}
