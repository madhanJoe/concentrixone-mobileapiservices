﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;

using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class TrainingCalendarRepository : ITrainingCalendarRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public TrainingCalendarRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public TrainingCalendarResponse GetTrainingCalendarList(string employeeId)
        {
            return entityDBAccess.GetTrainingCalendarList(employeeId);
        }
        public TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails)
        {
            return entityDBAccess.GetTrainingCalendarDetails(trainingcalendardetails);
        }
    }
}
