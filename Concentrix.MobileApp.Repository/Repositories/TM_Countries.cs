//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Countries
    {
        public string vc_Country_Name { get; set; }
        public string vc_Country_Code { get; set; }
        public string vc_ISD_Code { get; set; }
        public long bint_CountryID { get; set; }
        public Nullable<int> Geo_Id { get; set; }
    }
}
