﻿using Concentrix.MobileApp.Domain.DomainModels.Logout;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
  public  class LogoutRepository : ILogoutRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public LogoutRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public LogoutResponse LogoutDevice(LogoutRequest logout, string employeeId)
        {
            return entityDBAccess.LogoutDevice(logout,employeeId);
        }
    }
}
