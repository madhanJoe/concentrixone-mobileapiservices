﻿using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using Concentrix.MobileApp.Domain.DomainModels.Language;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Login;
using Concentrix.MobileApp.Domain.DomainModels.Logout;
using Concentrix.MobileApp.Domain.DomainModels.Message;
using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using Concentrix.MobileApp.Domain.DomainModels.Policy;
using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Domain.DomainModels.Query;
using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using Concentrix.MobileApp.Domain.DomainModels.Request;
using Concentrix.MobileApp.Domain.DomainModels.Survey;
using Concentrix.MobileApp.Domain.DomainModels.Test;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;
using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using Concentrix.MobileApp.Domain.DomainModels.URL;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;
using Concentrix.MobileApp.Domain.DomainModels.User;
using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using Concentrix.MobileApp.Repository.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using System.Configuration;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class EntityFrameDBAccess : IEntityFrameDBAccess
    {
        private ConcentrixOneEntities context;

        private void CreateContext()
        {
            if (context == null)
            {
                context = new ConcentrixOneEntities();
            }
        }

        #region Nanda R

        public HelloWorld DisplayHelloWorld()
        {
            CreateContext();

            return new HelloWorld
            {
                Message = "Welcome to One Concentrix!!!"
            };
        }

        public TestLogin AuthenticateUser(string username, string password)
        {
            var login = new TestLogin();
            login.UserName = username + " - Get Success - No AD Authentication";
            login.Password = password + " - Get Success - No AD Authentication";

            return login;
        }

        public ADResponse AuthenticateLogin(TestLogin login)
        {
            return new ADResponse
            {
                Status = "Success",
                Code = 0,
                Message = "Success"
            };
        }

        public TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog)
        {
            var tokenValidity = new TokenValidityResponse();

            CreateContext();

            var response = context.sproc_validate_appname_token(token.AppName, token.Token, token.LanguageCode).FirstOrDefault();

            var serviceLogId = context.usp_insert_service_log(serviceLog.ClientId, serviceLog.URL, serviceLog.ControllerName, serviceLog.ActionName,
                                                              serviceLog.Token, serviceLog.Request, serviceLog.Acknowledge, serviceLog.Message).FirstOrDefault();

            if (serviceLogId == null)
                serviceLogId = 0;

            if (response != null)
            {
                tokenValidity = new TokenValidityResponse
                {
                    Code = response.ReturnValue.ToString(),
                    Message = response.ReturnMessage,
                    ServiceLogId = (long)serviceLogId
                };
            }
            else
            {
                tokenValidity = new TokenValidityResponse
                {
                    Code = "100200",
                    Message = "Validation of Token failed",
                    ServiceLogId = (long)serviceLogId
                };
            }

            return tokenValidity;
        }

        public void UpdateServiceLog(long serviceLogId, string acknowledge, string message)
        {
            CreateContext();
            context.usp_update_service_log(serviceLogId, acknowledge, message);
            return;
        }

        public int LogToDatabase(Domain.DomainModels.Log.ErrorLog errorLog)
        {
            CreateContext();
            var response = context.usp_insert_error_log(errorLog.URL, errorLog.Message, errorLog.Source, errorLog.InnerException, errorLog.StackTrace);
            return response;
        }

        public TokenDetailsResponse GetTokenDetails(TokenRequest token)
        {
            var tokenDetails = new TokenDetailsResponse();

            CreateContext();

            var response = context.sproc_get_token_details(token.Token, token.LanguageCode).FirstOrDefault();

            if (response != null)
            {
                tokenDetails = new TokenDetailsResponse
                {
                    TokenId = response.TokenId,
                    TokenValue = response.TokenValue,
                    UserId = response.UserId,
                    UserType = response.UserType,
                    LanguageCode = response.LanguageCode,
                    DeviceId = response.DeviceId,
                    LoginType = response.LoginType,
                    Latitude = response.Latitude,
                    Longitude = response.Longitude,
                    TokenCreatedOn = response.TokenCreatedOn,
                    TokenExpiresOn = response.TokenExpiresOn,
                    MobileRegion = response.MobileRegion,
                    MobileCountry = response.MobileCountry,
                    MobileCity = response.MobileCity,
                    IsActive = response.IsActive,
                    Acknowledge = AcknowledgeType.Success,
                    Message = "Success",
                    ErrorCode = null
                };
            }
            else
            {
                tokenDetails = new TokenDetailsResponse
                {
                    Acknowledge = AcknowledgeType.Failure,
                    Message = "Failure",
                    ErrorCode = new List<ErrorDetails>
                    {
                        new ErrorDetails
                        {
                            Code = "100201",
                            Description = "Invalid Token or Token Expired"
                        }
                    }
                };
            }

            return tokenDetails;
        }

        public LoginADResponse GenerateToken(LoginADRequest login)
        {
            var employeeImagePath = string.Empty;

            employeeImagePath = ConfigurationManager.AppSettings["ProfileWebImagePath"];

            if (string.IsNullOrEmpty(employeeImagePath))
                employeeImagePath = "http://missing-config-link/";

            var adResponse = new LoginADResponse();

            CreateContext();

            var response = context.usp_User_GetSingle(login.UserName, login.Password, login.Email, login.DeviceId,
                                                      login.UserType, login.Language, login.Latitude, login.Longitude).FirstOrDefault();

            if (response != null)
            {
                adResponse = new LoginADResponse
                {
                    EmployeeId = response.EmployeeId,
                    FirstName = response.FirstName,
                    LastName = response.LastName,
                    ContactNumber = response.ContactNumber,
                    EmployeeName = response.EmployeeName,
                    Designation = response.Designation,
                    Project = response.Project,
                    Location = response.Location,
                    EmployeeEmailId = response.EmployeeEmailId,
                    Token = response.Token,
                    ProfileImageUrl = employeeImagePath + response.EmployeeId + "/Approved/" + response.ProfileImageUrl,
                    Acknowledge = AcknowledgeType.Success,
                    Message = "Success",
                    ErrorCode = null
                };
            }
            else
            {
                adResponse.Acknowledge = AcknowledgeType.Failure;
                adResponse.Message = "Token generation failed";
                adResponse.ErrorCode.Add(new ErrorDetails { Code = "100100", Description = "Token generation failed" });
            }

            return adResponse;
        }

        public FeatureResponse GenericFeatures(FeatureRequest feature)
        {
            var featureResponse = new FeatureResponse();
            featureResponse.FeatureContent = new List<FeatureList>();
            CreateContext();

            var featureSettings = context.USP_GetAll_FeatureSettings_FeatureCode(feature.FeatureCode, feature.LanguageCode).ToList();

            if (featureSettings != null && featureSettings.Count > 0)
            {
                var lstFeatureSettings = featureSettings.Select(s =>
                                                           new FeatureSettingList
                                                           {
                                                               FeatureCode = s.FeatureCode,
                                                               Key = s.SettingKey,
                                                               Value = s.SettingValue
                                                           }).ToList();

                var featureCode = lstFeatureSettings.GroupBy(x => x.FeatureCode).Select(x => x.First()).ToList();
                featureResponse.FeatureSettings = lstFeatureSettings.Where(q => q.Key != null && q.Value != null).ToList();

                foreach (var code in featureCode)
                {
                    var response = context.USP_GET_GenericFeaturesData_Employee(code.FeatureCode, feature.EmployeeId, feature.LanguageCode).ToList();

                    if (response != null && response.Count > 0)
                    {
                        var resContent = response.Select(f =>
                                                    new FeatureList
                                                    {
                                                        ContentType = f.ContentType,
                                                        ConcentTitle = f.Title,
                                                        ContentDescription = f.Description,
                                                        Content = f.ContentDetail,
                                                        BackRoundImageURL = f.ImageUrl
                                                    }).ToList();
                        featureResponse.FeatureContent.AddRange(resContent);
                        featureResponse.Acknowledge = AcknowledgeType.Success;
                        featureResponse.Message = "Success";
                    }
                }
            }
            else
            {
                featureResponse.Acknowledge = AcknowledgeType.Failure;
                featureResponse.Message = "Failed to get the data for the Feature Code '" + feature.FeatureCode + "'";
            }
            return featureResponse;
        }

        #endregion

        #region TAT Related Calls
        public CallBackResponse RequestCallback(CallBackRequest callback, string employeeId)
        {
            //waiting for Genesys entity 
            return null;
        }

        // TAT SP
        public TrainingVisibleResponse TrainingVisible(string employeeId)
        {
            CreateContext();
            //var response = context.USP_GetActiveBatchAgents(training.EmployeeID);

            return null;
        }

        // TAT SP
        public PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId)
        {
            return null;
        }

        // TAT SP
        public PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId)
        {
            return null;
        }

        public GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId)
        {
            return null;
        }

        // TAT SP
        public SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId)
        {
            return null;
        }

        public LogoutResponse LogoutDevice(LogoutRequest logout, string employeeId)
        {
            var logoutResponse = new LogoutResponse();

            CreateContext();

            var response = context.USP_EmployeeDevice_Delete(employeeId, logout.DeviceId, logout.EmailId, logout.UserType).FirstOrDefault();

            if (response != null && (response.ReturnValue > 0))
            {
                logoutResponse.Acknowledge = AcknowledgeType.Success;
                logoutResponse.Message = "Successfully logged out the Device";
            }
            else
            {
                logoutResponse.Acknowledge = AcknowledgeType.Failure;
                logoutResponse.Message = response.ReturnMessage;
                logoutResponse.ErrorCode.Add(new ErrorDetails { Code = "100504", Description = "Invalid Device to logout or Device is already logged out" });
            }

            return logoutResponse;
        }

        // TAT SP
        public LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial)
        {
            var loginSocialResponse = new LoginADResponse();

            CreateContext();

            var response = context.usp_RegisterUsers_Guest_From_Socail_Networks(guestsocial.UserName, guestsocial.Email, guestsocial.UserType, guestsocial.LoginType,
                                                                            guestsocial.DeviceId, guestsocial.Language, guestsocial.Latitude, guestsocial.Longitude).FirstOrDefault();

            if (response != null)
            {
                loginSocialResponse = new LoginADResponse
                {
                    EmployeeId = response.EmployeeId,
                    FirstName = response.FirstName,
                    LastName = response.LastName,
                    ContactNumber = response.ContactNumber,
                    EmployeeName = response.EmployeeName,
                    Designation = response.Designation,
                    Project = response.Project,
                    Location = response.Location,
                    EmployeeEmailId = response.EmployeeEmailId,
                    Token = response.Token,
                    Acknowledge = AcknowledgeType.Success,
                    Message = "Success",
                    ErrorCode = null
                };
            }
            else
            {
                loginSocialResponse.Acknowledge = AcknowledgeType.Failure;
                loginSocialResponse.Message = "Token generation failed for Social Guest User";
            }

            return loginSocialResponse;
        }
        // TAT SP
        public LoginADResponse ValidateUser(LoginADRequest login)
        {
            return null;
        }
        // TAT SP
        public LoginADResponse ValidateGuest(LoginADRequest guestlogin)
        {
            LoginADResponse guestResponse = new LoginADResponse();

            CreateContext();

            var response = context.usp_User_GetSingle(guestlogin.UserName, guestlogin.Password, guestlogin.Email, guestlogin.DeviceId, guestlogin.UserType, guestlogin.Language, guestlogin.Latitude, guestlogin.Longitude).FirstOrDefault();

            if (response != null)
            {
                guestResponse = new LoginADResponse
                {
                    EmployeeId = response.EmployeeId,
                    FirstName = response.FirstName,
                    LastName = response.LastName,
                    ContactNumber = response.ContactNumber,
                    EmployeeName = response.EmployeeName,
                    Designation = response.Designation,
                    Project = response.Project,
                    Location = response.Location,
                    EmployeeEmailId = response.EmployeeEmailId,
                    Token = response.Token,
                    Acknowledge = AcknowledgeType.Success,
                    Message = "Success",
                    ErrorCode = null
                };
            }
            else
            {
                guestResponse = new LoginADResponse
                {
                    Acknowledge = AcknowledgeType.Failure,
                    Message = "Login failed. Incorrect username or password",
                    ErrorCode = new List<ErrorDetails>
                    {
                        new ErrorDetails
                        {
                            Code = "100100",
                            Description = "Login failed. Incorrect username or password"
                        }
                    }
                };
            }

            return guestResponse;
        }

        // TAT SP
        public ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotPassword)
        {
            ForgotPasswordResponse forgotPasswordResponse = new ForgotPasswordResponse();

            CreateContext();

            var strPwd = context.USP_GetPassword(forgotPassword.GuestEmail);

            if (String.IsNullOrEmpty(Convert.ToString(strPwd)))
            {
                forgotPasswordResponse.Message = "Email Id Not found";
                forgotPasswordResponse.Acknowledge = AcknowledgeType.Failure;
            }
            else
            {
                forgotPasswordResponse.Acknowledge = AcknowledgeType.Success;
                forgotPasswordResponse.Message = "Password is sent to your registered Email";
            }

            return forgotPasswordResponse;
        }

        #endregion

        #region Madhan M

        public AddPolicyResponse RequestPolicy(AddPolicyRequest policyrequest, string employeeId)
        {
            CreateContext();
            AddPolicyResponse addPolicyResponse = new AddPolicyResponse();
            int res = context.USP_EmpolyeePolicies_Insert(employeeId, policyrequest.PolicyId, policyrequest.Comments, null);
            if (res > 0)
            {
                addPolicyResponse.Acknowledge = AcknowledgeType.Success;
                addPolicyResponse.Message = "The URL / link to download the requested policy has been sent your Inbox.";

            }
            else
            {
                addPolicyResponse.Acknowledge = AcknowledgeType.Failure;
                addPolicyResponse.Message = "There is some issue for raise a request";

            }
            return addPolicyResponse;
        }



        //lat db
        public SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating, string employeeId)
        {//sp changes required.not available 
         //context.USP_SubmitMoodMeterRating


            return null;

        }
        //lat db
        public MoodMeterResponse GetMoodMeterAttributes(string employeeId)
        {//sp not available
         //context.USP_GetMoodMeterAttributes(


            return null;

        }
        public MessageResponse GetMessage(MessageRequest message, string employeeId)
        {
            CreateContext();
            //sp changes required.not available
            MessageResponse messageResponse = new MessageResponse();
            System.Data.Entity.Core.Objects.ObjectParameter output = new System.Data.Entity.Core.Objects.ObjectParameter("totalrecords", typeof(int));
            var msg = context.usp_Messages_GetPaged(message.PageNumber, output, employeeId, message.EmailId);
            messageResponse.SMS = new List<Domain.DomainModels.Message.EmployeeSMS>();
            messageResponse.SMS = msg.Select(r => new EmployeeSMS
            {
                MessageId = Convert.ToInt32(r.MessageId),
                MessageContent = r.MessageContent,
                ReadStatus = Convert.ToInt16(r.ReadStatus),
                SenderName = r.CreatedBy,
                Subject = r.Subject
            }).ToList();
            messageResponse.Acknowledge = AcknowledgeType.Success;
            messageResponse.Message = "Success";
            return messageResponse;
        }


        public UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId)
        {
            CreateContext();
            UpdateMessageResponse updateMessageResponse = new UpdateMessageResponse();
            var response = context.USP_Update_Message_Status(updatemessage.MessageId, employeeId).FirstOrDefault();
            if (response.Execution_Status > 0)
            {

                updateMessageResponse.Message = response.Display_Message;
                updateMessageResponse.Acknowledge = AcknowledgeType.Success;
                return updateMessageResponse;
            }
            else
            {


                updateMessageResponse.Acknowledge = AcknowledgeType.Failure;
                return updateMessageResponse;

            }
        }
        public DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage)
        {
            CreateContext();

            var response = context.usp_Message_Delete(deletemessage.MessageId).FirstOrDefault();

            DeleteMessageResponse deleteMessageResponse = new DeleteMessageResponse();

            if (response != null && (response.ReturnValue > 0))
            {
                deleteMessageResponse.Acknowledge = AcknowledgeType.Success;
                deleteMessageResponse.Message = "Successfully deleted the Message";
            }
            else
            {
                deleteMessageResponse.Acknowledge = AcknowledgeType.Failure;
                deleteMessageResponse.Message = "Failed";
                deleteMessageResponse.ErrorCode.Add(new ErrorDetails { Code = "100502", Description = "Invalid Message to delete or Message is already deleted" });
            }

            return deleteMessageResponse;
        }


        public DeleteAllMessagesResponse DeleteAllMessage(string employeeID)
        {
            CreateContext();
            int recordCount = context.USP_DeleteAllMessages(employeeID);
            DeleteAllMessagesResponse deleteAllMessagesResponse = new DeleteAllMessagesResponse();
            if (recordCount > 0)
            {
                deleteAllMessagesResponse.Acknowledge = AcknowledgeType.Success;
                deleteAllMessagesResponse.Message = "Success";
            }
            else
            {
                deleteAllMessagesResponse.Acknowledge = AcknowledgeType.Failure;
                deleteAllMessagesResponse.Message = "0 Records Deleted";
            }

            return deleteAllMessagesResponse;
        }

        public DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple, string employeeID)
        {
            CreateContext();
            int recordCount = context.USP_DeleteMultipleMessages(string.Join(",", deletemultiple.MessageIds));
            DeleteMultipleMessagesResponse deleteMultipleMessagesResponse = new DeleteMultipleMessagesResponse();
            if (recordCount > 0)
            {
                deleteMultipleMessagesResponse.Acknowledge = AcknowledgeType.Success;
                deleteMultipleMessagesResponse.Message = "Success";
            }
            else
            {
                deleteMultipleMessagesResponse.Acknowledge = AcknowledgeType.Failure;
                deleteMultipleMessagesResponse.Message = "0 Records Deleted";
            }

            return deleteMultipleMessagesResponse;
        }

        public LocationResponse GetLocations(string employeeId, string locationCode, string languageCode)
        {
            CreateContext();
            LocationResponse locationResponse = new LocationResponse();
            List<Location> lstLocation = new List<Location>();

            // context.usp_Get_Locations_All(locationCode);

            lstLocation = context.VW_SiteHirarchy.Where(x => x.LanguageCode == languageCode && x.LocationCode == locationCode)
                                    .Select(s =>

                                        new Location
                                        {
                                            Id = s.LocationID,
                                            Name = s.Location,
                                            Address = s.LocationDescription,
                                            ContactNumber = s.PhoneNumber,
                                            EmailAddress = s.EmailAddress,
                                            FaxNumber = s.FaxNumber,
                                            City = s.City,
                                            State = "",
                                            PinCode = s.PinCode,
                                            Country = s.Country,
                                            Latitude = s.LocationLatitude,
                                            Longitude = s.LocationLongitude,
                                        }).ToList();

            foreach (string strCountry in lstLocation.Select(x => x.Country).Distinct().ToList())
            {
                var lstCity = lstLocation.Where(y => y.Country.Equals(strCountry))
                                        .ToList();
                Country objCountry = new Country();
                objCountry.CountryName = strCountry;
                objCountry.Cities = lstCity;
                locationResponse.Countries.Add(objCountry);


            }
            locationResponse.Acknowledge = AcknowledgeType.Success;
            locationResponse.Message = "Success";
            return locationResponse;
        }

        public LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leavebalance, string employeeId)
        {
            CreateContext();
            var lstLeaveBalance = context.USP_Get_SMS_Response_App(employeeId, leavebalance.CODE);
            LeaveBalanceResponse responseLeaveBalance = new LeaveBalanceResponse();
            responseLeaveBalance.Acknowledge = AcknowledgeType.Success;
            responseLeaveBalance.Message = "Your leave details have been sent to your Inbox.";
            return null;
        }

        public HolidayResponse GetHolidays(string employeeId, string LanguageCode)
        {
            var holidayResponse = new HolidayResponse();

            CreateContext();

            var holidays = context.USP_Get_Holiday_ByEmployee(employeeId, LanguageCode).ToList();

            if (holidays != null && holidays.Count() > 0)
            {
                holidayResponse.Holidays = holidays.Select(r =>
                                                new Holiday
                                                {
                                                    Date = r.HolidayDate.ToString("MM/dd/yyyy"),
                                                    DayName = r.DayName,
                                                    HolidayName = r.HolidayName,
                                                    HolidayType = r.HolidayType
                                                }).ToList();

                holidayResponse.Acknowledge = AcknowledgeType.Success;
                holidayResponse.Message = "Success";
            }
            else
            {
                holidayResponse.Acknowledge = AcknowledgeType.Failure;
                holidayResponse.Message = "Failed to get the Holiday List";
            }

            return holidayResponse;
        }

        public BenefitsResponse GetBenefits(string employeeId, string languageCode)
        {
            CreateContext();
            var benefits = context.USP_GetBenefits(employeeId, languageCode).ToList();

            List<BenefitsLists> benefitsList = new List<BenefitsLists>();

            foreach (var benefit in benefits)
            {
                benefitsList.Add(new BenefitsLists()
                {
                    // EmployeeId = dr["EmployeeId"].AsString(),
                    AppId = benefit.AppId,
                    AppTitle = benefit.AppTitle,
                    AppCode = benefit.AppCode,
                    IsApp = benefit.IsApp,
                    WebUrl = benefit.WebUrl,
                    AndroidPackageName = benefit.AndroidPackageName,
                    AndroidMarketUrl = benefit.AndroidMarketUrl,
                    IOSUrlScheme = benefit.IOSUrlScheme,
                });
            }
            BenefitsResponse benefitsResponse = new BenefitsResponse();
            benefitsResponse.Benefits.BenefitsList = benefitsList;
            return benefitsResponse;
        }

        private AttributeResponse GetAttributeResponse(AttributeRequest attribute, string employeeId)
        {
            CreateContext();

            var response = context.usp_AttributeValue_ByAttribute_Get(Convert.ToInt32(attribute.Code), employeeId).ToList();

            AttributeResponse responseAttribute = new AttributeResponse();

            if (response != null && response.Count > 0)
            {
                responseAttribute.AttributeValues = response.Select(a =>
                                                                new AttributeValue
                                                                {
                                                                    AttributeValueId = a.AttributeValueId,
                                                                    Code = a.Code,
                                                                    Name = a.AttributeValue,
                                                                    Description = a.AttributeValueDesc
                                                                }).ToList();

                responseAttribute.Acknowledge = AcknowledgeType.Success;
            }
            else
            {
                responseAttribute.Acknowledge = AcknowledgeType.Failure;
                responseAttribute.Message = "Failure";
                responseAttribute.ErrorCode.Add(new ErrorDetails { Code = "100500", Description = "Not able to get the required information." });
            }

            return (responseAttribute);
        }


        public AttributeResponse GetDemandLetterList(AttributeRequest attribute, string employeeId)
        {
            CreateContext();
            /*//List<AttributeResponse> _olstAttributes = new List<AttributeResponse>();

            CreateContext();

            var response = context.usp_AttributeValue_ByAttribute_Get(Convert.ToInt32(attribute.Code), attribute.EmployeeID).ToList();

            AttributeResponse responseAttribute = new AttributeResponse();
            foreach (var resp in response)
            {

                responseAttribute.AttributeValues.Add(new AttributeValue
                { 
                AttributeValueId = resp.AttributeValueId,
                Code = resp.Code,
                Name = resp.AttributeValue,
                Description = resp.AttributeValueDesc});

            }*/
            return (GetAttributeResponse(attribute, employeeId));
        }


        public CountryResponse GetCountryCode(CountryRequest country)
        {
            var countryResponse = new CountryResponse();

            CreateContext();

            var response = context.usp_get_list_of_countries().ToList();

            if (response != null && response.Count > 0)
            {
                countryResponse.Countries = response.Select(c =>
                                                new CountryDetail
                                                {
                                                    CountryCode = c.CountryCode,
                                                    CountryName = c.CountryName,
                                                    ISDCode = c.ISDCode
                                                }).ToList();

                countryResponse.Acknowledge = AcknowledgeType.Success;
                countryResponse.Message = "Success";
            }
            else
            {
                countryResponse.Acknowledge = AcknowledgeType.Failure;
                countryResponse.Message = "Failed to get the list of countries";
            }

            return countryResponse;
        }

        public AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId)
        {
            CreateContext();
            return (GetAttributeResponse(policy, employeeId));
        }

        #endregion

        #region Mathivanan M

        public CaseCategoryResponse GetCaseCategory(string employeeId)
        {
            CreateContext();
            CaseCategoryResponse caseCategoryResponse = new CaseCategoryResponse();
            var response = context.USP_GetCategories(employeeId);
            var employee = context.VW_Employee.FirstOrDefault(x => x.PS_Emp_Id == employeeId);
            caseCategoryResponse.CategoryList = new List<CaseCategory>();

            caseCategoryResponse.CategoryList = context.TM_Category.Where(x => x.Location == employee.vc_country_name)
                                                        .Select(y => new CaseCategory
                                                        {
                                                            Category = y.CategoryName,
                                                            CategoryID = y.CategoryID,
                                                            SubCategory = new List<CaseSubCategory>()
                                                        }
                                                                )
                                                        .ToList();

            foreach (CaseCategory caseCat in caseCategoryResponse.CategoryList)
            {
                caseCat.SubCategory = context.TM_SubCategory.Where(x => x.Location == employee.vc_country_name && x.CategoryID == caseCat.CategoryID)
                                                            .Select(r => new CaseSubCategory
                                                            {
                                                                SubCategory = r.SubCategoryName,
                                                                SubCategoryID = r.SubCategoryID

                                                            }
                                                                    )
                                                             .ToList();
            }



            return null;


        }

        public NewCaseResponse CreateNewCase(NewCaseRequest newcase, string employeeId, string languageCode)
        {
            CreateContext();

            var response = context.usp_User_GetSingle_ByEmpId(employeeId, languageCode);

            //WeCare emailLog = new WeCare();
            //emailLog.UserName = moUser.FullName;
            //string[] NTID = moUser.UserName.Split('\\');
            //emailLog.WindowsNTID = NTID.Length > 1 ? NTID[1] : NTID[0];

            response.Select(r =>
                         new Wecare
                         {
                             EmployeeId = r.PS_Emp_Id,
                             UserName = r.FullName,
                             WindowsNTID = r.Windows_NT_Id,
                         }).ToList();

            Wecare objwecare = new Wecare();
            string[] ntID = objwecare.WindowsNTID.Split('\\');
            objwecare.WindowsNTID = ntID.Length > 1 ? ntID[1] : ntID[0];
            int caseId = context.mihr_SubmitUserDetails(employeeId, objwecare.UserName, newcase.Category, newcase.SubCategory,
                 newcase.Subject, newcase.Details, objwecare.WindowsNTID, newcase.EmailId, newcase.Phone);

            NewCaseResponse newCaseResponse = new NewCaseResponse();
            newCaseResponse.Message = "Case created successfully.You will be receiving a mail with the case ID shortly.";
            newCaseResponse.Acknowledge = AcknowledgeType.Success;
            return newCaseResponse;
        }

        public CaseHistoryResponse GetCaseHistory(string employeeId)
        {
            return null;
        }

        public URLResponse GetURL(URLRequest url)
        {
            var urlResponse = new URLResponse();

            CreateContext();

            var response = context.USP_Description_By_Code_Get(url.Code.ToUpper()).FirstOrDefault();

            urlResponse.URL = response.AttributeValue;
            urlResponse.Acknowledge = AcknowledgeType.Success;
            urlResponse.Message = "Success";

            return urlResponse;
        }

        public TrainingCalendarResponse GetTrainingCalendarList(string employeeId)
        {
            TrainingCalendarResponse calenderResponse = new TrainingCalendarResponse();

            CreateContext();

            var response = context.USP_TrainingCalendar_GetByEmpId(employeeId).ToList();

            calenderResponse.TrainingCalendarList = response.Select(r =>
                                                       new TrainingCalendar
                                                       {
                                                           TrainingCalendarId = r.Id.ToString(),
                                                           Date = r.Date,
                                                           Title = r.Title
                                                       }).ToList();
            if (response.Count > 0)
            {
                calenderResponse.Acknowledge = AcknowledgeType.Success;
                calenderResponse.Message = "Success";
            }
            else
            {
                calenderResponse.Acknowledge = AcknowledgeType.Success;
                calenderResponse.Message = "No Training is available";
            }

            return calenderResponse;
        }


        public TrainingCalendarDetailsResponse GetTrainingCalendarDetails(TrainingCalendarDetailsRequest trainingcalendardetails)
        {
            TrainingCalendarDetailsResponse calendarDetails = new TrainingCalendarDetailsResponse();

            CreateContext();

            var response = context.USP_TrainingCalendar_GetdetailsById(trainingcalendardetails.TrainingCalendarId).ToList();

            calendarDetails.AllTrainingDetails = response.Select(r =>
                                                     new TrainingCalenderDetails
                                                     {
                                                         Id = r.Id,
                                                         Date = r.Date,
                                                         Venue = r.Venue,
                                                         StartTime = r.StartTime,
                                                         EndTime = r.EndTime,
                                                         Participants = r.Participants,
                                                         Facilitator = r.Facilitator,
                                                         Objectives = r.Objectives,
                                                         Site = r.Site,
                                                         Location = r.Location,
                                                         Title = r.Title,

                                                     }).ToList();

            calendarDetails.Acknowledge = AcknowledgeType.Success;
            calendarDetails.Message = "Success";

            return calendarDetails;
        }

        public PayStubResponse GetPaySlip(PayStubRequest payslip, string employeeId)
        {
            CreateContext();

            var response = GetDemandPayStubDetails(payslip, employeeId);

            return response;
        }

        private PayStubResponse GetDemandPayStubDetails(PayStubRequest detailRequest, string employeeId)
        {
            CreateContext();
            string message = string.Empty;
            var returnvalue = new PayStubResponse();
            var response = context.usp_RequestFor_PayORDemandLetter(employeeId, detailRequest.Code);

            switch (detailRequest.Code)
            {
                case "PAY STUB":
                    message = "Your request for pay-stub has been registered and it will be sent to your email shortly.";
                    break;

                case "PAY SLIP":
                    message = "Your request for pay-slip has been registered and it will be sent to your email shortly.";
                    break;

                case "Demand Letter":
                    message = "Your request for Demand Letter has been registered and miHR team will revert on it shortly.";
                    break;
                default:
                    message = "Invalid Code as you provided...";
                    break;
            }
            returnvalue.Message = message;
            returnvalue.Acknowledge = AcknowledgeType.Success;

            return returnvalue;
        }

        public PayStubResponse GetDemandLetter(PayStubRequest demandletter, string employeeId)
        {
            CreateContext();
            var response = GetDemandPayStubDetails(demandletter, employeeId);
            return response;
        }

        public PayStubResponse GetPayStubDetails(PayStubRequest paystub, string employeeId)
        {
            CreateContext();

            var response = GetDemandPayStubDetails(paystub, employeeId);
            return response;
        }
        #endregion

        #region Ram
        public ProfileSaveResponse RegisterUser(RegisterUserRequest register)
        {
            ProfileSaveResponse objProfileSaveResponse = new ProfileSaveResponse();

            CreateContext();

            System.Data.Entity.Core.Objects.ObjectParameter ErrorMessage = new System.Data.Entity.Core.Objects.ObjectParameter("ErrorMessage", typeof(string));

            var response = context.usp_RegisterUsers_Guest(register.FirstName, register.LastName, register.PersonalEmail, register.Password, register.UserType, register.Mobile.PhoneNumber, register.Mobile.CountryCode,  ErrorMessage).FirstOrDefault();

            if (response != null && response > 0)
                objProfileSaveResponse.Acknowledge = AcknowledgeType.Success;
            else
                objProfileSaveResponse.Acknowledge = AcknowledgeType.Failure;

            objProfileSaveResponse.Message = ErrorMessage.Value.ToString();

            return objProfileSaveResponse;
        }

        //Doubt in employeeid parameter.
        public ProfileSaveResponse SaveProfile(GuestUserInfoRequest saveprofile, string employeeId)
        {
            CreateContext();

            ProfileSaveResponse objProfileSaveResponse = new ProfileSaveResponse();
            context.usp_MobileProfile_Update(employeeId, saveprofile.Mobile, saveprofile.PersonalEmail, saveprofile.OfficeEmail, saveprofile.FacebookId, saveprofile.TwitterId);
            objProfileSaveResponse.Acknowledge = AcknowledgeType.Success;
            objProfileSaveResponse.Message = "Profile updated succesfully. The changes will get reflected once your manager/administrator has approved it.";

            return objProfileSaveResponse;
        }

        public ProfileResponse GetProfile(string EmployeeId, string LanguageCode)
        {
            var employeeImagePath = string.Empty;

            employeeImagePath = ConfigurationManager.AppSettings["ProfileWebImagePath"];

            if (string.IsNullOrEmpty(employeeImagePath))
                employeeImagePath = "http://missing-config-link/";

            CreateContext();

            var profileResponse = new ProfileResponse();

            //var objresponce = context.usp_User_GetSingle_ByEmpId(EmployeeId, LanguageCode).FirstOrDefault();

            var mainResponse = context.usp_get_myprofile_details(EmployeeId, LanguageCode).FirstOrDefault();

            if (mainResponse != null)
            {
                var visaResponse = context.usp_get_myprofile_visa_details(EmployeeId).ToList();
                var languageResponse = context.usp_get_myprofile_language_details(EmployeeId).ToList();
                var visibilityResponse = context.usp_get_myprofile_visibility_details(EmployeeId).FirstOrDefault();

                profileResponse = new ProfileResponse
                {
                    EmployeeId = mainResponse.EmployeeId,
                    FirstName = mainResponse.FirstName,
                    MiddleName = mainResponse.MiddleName,
                    LastName = mainResponse.LastName,
                    FullName = mainResponse.FullName,
                    ProfileImageUrl = employeeImagePath + mainResponse.EmployeeId + "/Approved/" + mainResponse.ProfileImageUrl,
                    EmployeeEmail = mainResponse.EmployeeEmail,
                    Location = mainResponse.Location,
                    Department = mainResponse.Department,
                    SupervisorName = mainResponse.SupervisorName,
                    Designation = mainResponse.Designation,
                    Address = mainResponse.Address,
                    EmergencyContactPerson = mainResponse.EmergencyContactPerson,
                    AboutMe = mainResponse.AboutMe,
                    SkypeId = mainResponse.SkypeId,
                    WhatsAppId = mainResponse.WhatsAppId,
                    Acknowledge = AcknowledgeType.Success,
                    Message = "Success"
                };

                profileResponse.EmployeeContactNumber.CountryCode = mainResponse.EmployeeCountryCode;
                profileResponse.EmployeeContactNumber.ISDCode = mainResponse.EmployeeISDCode;
                profileResponse.EmployeeContactNumber.PhoneNumber = mainResponse.EmployeeContactNumber;
                profileResponse.EmergencyContactNumber.CountryCode = mainResponse.EmergencyCountryCode;
                profileResponse.EmergencyContactNumber.ISDCode = mainResponse.EmergencyISDCode;
                profileResponse.EmergencyContactNumber.PhoneNumber = mainResponse.EmergencyContactNumber;

                if (visaResponse != null)
                {
                    profileResponse.VisaDetails = visaResponse.Select(v =>
                                                    new VisaDetail
                                                    {
                                                        VisaId = v.VisaId,
                                                        VisaCountryCode = v.VisaCountryCode,
                                                        VisaCountryName = v.VisaCountryName,
                                                        VisaNumber = v.VisaNumber,
                                                        TypeOfVisa = v.TypeOfVisa,
                                                        VisaExpiryDate = v.VisaExpiryDate.ToShortDateString()
                                                    }).ToList();
                }

                if (languageResponse != null)
                {
                    profileResponse.Languages = languageResponse.Select(l =>
                                                    new LanguageDetail
                                                    {
                                                        LanguageId = l.LanguageId,
                                                        LanguageApprovedId = l.ProfileLanguageId,
                                                        LanguageName = l.LanguageName,
                                                        Spoken = l.CanSpeak.ToString().ToLower(),
                                                        Written = l.CanWrite.ToString().ToLower()
                                                    }).ToList();
                }

                if (visibilityResponse != null)
                {
                    profileResponse.ProfileVisibility.Address = visibilityResponse.AddressVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.EmergencyContactPerson = visibilityResponse.EmergencyContactPersonVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.EmergencyContactNumber = visibilityResponse.EmergencyContactNumberVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.PhoneNumber = visibilityResponse.PhoneNoVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.SkypeId = visibilityResponse.SkypeIdVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.WhatsAppId = visibilityResponse.WhatsAppIdVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.AboutMe = visibilityResponse.AboutMeVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.Language = visibilityResponse.LanguageVisible.ToString().ToLower();
                    profileResponse.ProfileVisibility.Visa = visibilityResponse.VisaVisible.ToString().ToLower();
                }
            }
            else
            {
                profileResponse.Acknowledge = AcknowledgeType.Failure;
                profileResponse.Message = "Failed to get the details of My Profile";
            }

            return profileResponse;
        }

        public ProfileAboutMeResponse SaveProfileAboutMe(ProfileAboutMeRequest aboutMe)
        {
            var aboutMeResponse = new ProfileAboutMeResponse();

            CreateContext();

            var response = context.usp_update_profile_aboutme(aboutMe.EmployeeId, aboutMe.AboutMe).FirstOrDefault();

            if (response > 0)
            {
                aboutMeResponse.Acknowledge = AcknowledgeType.Success;
                aboutMeResponse.Message = "Profile Description updated successfully. The changes will get reflected once your manager/administrator has approved it.";
            }
            else
            {
                aboutMeResponse.Acknowledge = AcknowledgeType.Failure;
                aboutMeResponse.Message = "Failed to update Profile Description";
            }

            return aboutMeResponse;
        }

        public ProfileImageResponse UpdateProfileImageName(ProfileImageRequest imageName)
        {
            var imageNameResponse = new ProfileImageResponse();

            CreateContext();

            var response = context.usp_update_profile_image_name(imageName.EmployeeId, imageName.ImageName).FirstOrDefault();

            if (response > 0)
            {
                imageNameResponse.Acknowledge = AcknowledgeType.Success;
                imageNameResponse.Message = "Profile image uploaded successfully. The changes will get reflected once your manager/administrator has approved it.";
            }
            else
            {
                imageNameResponse.Acknowledge = AcknowledgeType.Failure;
                imageNameResponse.Message = "Failed to upload the profile image";
            }

            return imageNameResponse;
        }

        public ProfileVisibilityResponse UpdateProfileVisibility(ProfileVisibilityRequest profileVisibility)
        {
            var profileVisibilityResponse = new ProfileVisibilityResponse();

            bool addressVisible, emergencyContactPersonVisible, emergencyContactNumberVisible, phoneNoVisible, skypeIdVisible, whatsAppIdVisible, aboutMeVisible, languageVisible, visaVisible;

            addressVisible = emergencyContactPersonVisible = emergencyContactNumberVisible = phoneNoVisible = skypeIdVisible = whatsAppIdVisible = aboutMeVisible = languageVisible = visaVisible = false;

            if (profileVisibility.Address.ToLower() == "true")
                addressVisible = true;

            if (profileVisibility.EmergencyContactPerson.ToLower() == "true")
                emergencyContactPersonVisible = true;

            if (profileVisibility.EmergencyContactNumber.ToLower() == "true")
                emergencyContactNumberVisible = true;

            if (profileVisibility.PhoneNumber.ToLower() == "true")
                phoneNoVisible = true;

            if (profileVisibility.SkypeId.ToLower() == "true")
                skypeIdVisible = true;

            if (profileVisibility.WhatsAppId.ToLower() == "true")
                whatsAppIdVisible = true;

            if (profileVisibility.AboutMe.ToLower() == "true")
                aboutMeVisible = true;

            if (profileVisibility.Language.ToLower() == "true")
                languageVisible = true;

            if (profileVisibility.Visa.ToLower() == "true")
                visaVisible = true;

            CreateContext();

            var response = context.usp_insert_update_profile_visibility(profileVisibility.EmployeeId, addressVisible, emergencyContactPersonVisible, emergencyContactNumberVisible,
                                                                        phoneNoVisible, skypeIdVisible, whatsAppIdVisible, aboutMeVisible, languageVisible, visaVisible).FirstOrDefault();

            if (response != null && response > 0)
            {
                profileVisibilityResponse.Acknowledge = AcknowledgeType.Success;
                profileVisibilityResponse.Message = "Profile Visibility updated successfully";
            }
            else
            {
                profileVisibilityResponse.Acknowledge = AcknowledgeType.Failure;
                profileVisibilityResponse.Message = "Failed to update Profile Visibility";
            }

            return profileVisibilityResponse;
        }

        public VisaDetailResponse UpdateVisaDetail(VisaDetailRequest visaDetail)
        {
            var visaRespone = new VisaDetailResponse();

            DateTime validDate;

            DateTime.TryParse(visaDetail.VisaExpiryDate, out validDate);

            CreateContext();

            var response = context.usp_update_myprofile_visa_details(visaDetail.EmployeeId, visaDetail.VisaId, visaDetail.VisaNumber, visaDetail.VisaCountryCode, visaDetail.TypeOfVisa, validDate, (int) visaDetail.Mode).FirstOrDefault();

            if (response != null && response > 0)
            {
                visaRespone.Acknowledge = AcknowledgeType.Success;
                visaRespone.Message = "Visa details has been updated. The changes will get reflected once your manager/administrator has approved it.";
            }
            else
            {
                visaRespone.Acknowledge = AcknowledgeType.Failure;
                visaRespone.Message = "Failed to update Visa details";
            }

            return visaRespone;
        }

        public EditableProfileResponse UpdateEditableProfile(EditableProfileRequest editableProfile)
        {
            var editableProfileResponse = new EditableProfileResponse();

            CreateContext();

            var response = context.usp_update_myprofile_editable_profile(editableProfile.EmployeeId, editableProfile.Address, editableProfile.ContactNumber.PhoneNumber,
                                                                         editableProfile.ContactNumber.ISDCode, editableProfile.ContactNumber.CountryCode,
                                                                         editableProfile.EmergencyContactPerson, editableProfile.EmergencyContactNumber.PhoneNumber,
                                                                         editableProfile.EmergencyContactNumber.ISDCode, editableProfile.EmergencyContactNumber.CountryCode,
                                                                         editableProfile.SkypeId, editableProfile.WhatsAppId).FirstOrDefault();

            if (response != null && response > 0)
            {
                editableProfileResponse.Acknowledge = AcknowledgeType.Success;
                editableProfileResponse.Message = "Profile has been updated. The changes will get reflected once your manager/administrator has approved it.";
            }
            else
            {
                editableProfileResponse.Acknowledge = AcknowledgeType.Failure;
                editableProfileResponse.Message = "Failed to update the Profile";
            }

            return editableProfileResponse;
        }

        //SP USP_GetPredictiveFeedback is not available in the database.
        public PredictiveFeedbackResponse GetPredictiveFeedback(string employeeId)
        {
            CreateContext();
            PredictiveFeedbackResponse objPredictiveFeedbackResponse = new PredictiveFeedbackResponse();
            // context.USP_GetPredictiveFeedback()
            return objPredictiveFeedbackResponse;
        }

        //SP USP_GetListofQueries is not available in Database.
        public GetAllQueryForEmployeeResponse GetQueryList(string EmployeeId)
        {
            CreateContext();
            //var response = context.USP_GetListofQueries(training.EmployeeID);

            return null;
        }
        //SP USP_SUBMITQUERY is not available in Database.
        public SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string EmployeeId)
        {
            CreateContext();
            //var response = context.USP_SUBMITQUERY(training.EmployeeID);

            return null;
        }

        //Need to modify Employeed id data type in store procedur paramaetr
        public PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string EmployeeId)
        {
            CreateContext();
            PendingQuizResponse objPendingQuizResponse = new PendingQuizResponse();
            //var objresponse = context.USP_GetPendingQuiz(EmployeeId,EmployeeId,quizlist.CurrentDate);

            return objPendingQuizResponse;

        }


        //Doubt in Responce because Sp is returning int value and in code need to bind list
        public GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string EmployeeId)
        {
            CreateContext();
            GetQuizResponse objGetQuizResponse = new GetQuizResponse();
            var objResponse = context.USP_GetSpotQuiz(EmployeeId, EmployeeId, requestquizquestions.QuizId, requestquizquestions.QuizName, Convert.ToDateTime(requestquizquestions.CurrentDate));
            objGetQuizResponse.Acknowledge = AcknowledgeType.Success;


            return objGetQuizResponse;
        }

        //Doubt in QuizXML paramater of Storeprocedure.
        public SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string EmployeeId)
        {
            CreateContext();
            SubmitQuizResponse objSubmitQuizResponse = new SubmitQuizResponse();
            int res = context.USP_tx_AgentQuiz_QuestionMaster_Update(EmployeeId, EmployeeId, updatequizanswer.QuizId, updatequizanswer.QuizName, Convert.ToDateTime(updatequizanswer.CurrentDate), "");
            if (res > 0)
            {
                objSubmitQuizResponse.Acknowledge = AcknowledgeType.Success;
                objSubmitQuizResponse.Message = "Quiz Updated Sucessfully";
            }
            else
            {
                objSubmitQuizResponse.Acknowledge = AcknowledgeType.Failure;
                objSubmitQuizResponse.Message = "Quiz couldn't be Updated";
            }

            return objSubmitQuizResponse;
        }
        //SP USP_GetActiveBatchAgents is not available in Database.
        public QuizSummaryResponse QuizSummary(QuizSummaryRequest quizsummary, string EmployeeId)
        {
            CreateContext();
            //var response = context.USP_GetActiveBatchAgents(training.EmployeeID);

            return null;
        }

        public GetPfResponse GetPfDetails(GetPfRequest pf, string EmployeeId)
        {
            CreateContext();
            GetPfResponse objGetPfResponse = new GetPfResponse();
            List<PfDetails> objPfDetailsList = new List<PfDetails>();

            // var objresponce = context.USP_Get_SMS_Response_App(EmployeeId, pf.CODE).ToList();

            //foreach (var pfList in objresponce)
            //{
            //objPfDetailsList.Add(new PfDetails()
            //{



            //}
            // );
            // }
            objGetPfResponse.PfDetail = objPfDetailsList;
            return objGetPfResponse;
        }

        public RequestListResponse GetRequestList(string EmployeeId)
        {
            CreateContext();

            RequestListResponse objRequestListResponse = new RequestListResponse();
            List<RequestList> objRequestList = new List<RequestList>();

            var objResponce = context.USP_RequestList_GET_BYLocation(EmployeeId).ToList();

            if (objResponce != null && objResponce.Count > 0)
            {
                foreach (var rList in objResponce)
                {
                    objRequestList.Add(new RequestList()
                    {
                        IsAllowed = rList.IsAllowed ?? false,
                        RequestCode = rList.RequestCode,
                        RequestId = rList.RequestListId,
                        RequestTitle = rList.RequestTitle
                    });
                }

                objRequestListResponse.RequestList = objRequestList;
                objRequestListResponse.Acknowledge = AcknowledgeType.Success;
            }
            else
            {
                objRequestListResponse.Acknowledge = AcknowledgeType.Failure;
                objRequestListResponse.Message = "Failure";
                objRequestListResponse.ErrorCode.Add(new ErrorDetails { Code = "100501", Description = "Not able to get the required information." });
            }

            return objRequestListResponse;
        }

        public LanguageResponse GetLanguages(LanguageRequest language)
        {
            var languageResponse = new LanguageResponse();

            CreateContext();

            var response = context.usp_get_global_languages().ToList();

            if (response != null && response.Count > 0)
            {
                languageResponse.Languages = response.Select(r =>
                                              new GlobalLanguage
                                              {
                                                  LanguageId = r.LanguageId,
                                                  LanguageName = r.LanguageName
                                              }).ToList();

                languageResponse.Acknowledge = AcknowledgeType.Success;
                languageResponse.Message = "Success";
            }
            else
            {
                languageResponse.Acknowledge = AcknowledgeType.Failure;
                languageResponse.Message = "Failed to get the list of Languages";
            }

            return languageResponse;
        }

        public LanguageDetailResponse UpdateLanguageDetail(LanguageDetailRequest languageDetail)
        {
            var languageResponce = new LanguageDetailResponse();

            CreateContext();

            var response = context.usp_update_myprofile_language_details(languageDetail.LanguageXML.ToString()).FirstOrDefault();

            if (response != null && response > 0)
            {
                languageResponce.Acknowledge = AcknowledgeType.Success;
                languageResponce.Message = "Language details updated successfully. The changes will get reflected once your manager/administrator has approved it.";
            }
            else
            {
                languageResponce.Acknowledge = AcknowledgeType.Failure;
                languageResponce.Message = "Failed to update Language details";
            }

            return languageResponce;
        }
        #endregion   
        public GetFeatureDepartmentResponse GetSurveyDeparments(string employeeId, string LanguageCode)
        {
            var featureDepartmentResponse = new GetFeatureDepartmentResponse();

            CreateContext();

            var featureDepartments = context.sproc_FeatureDepartment_GetAll(employeeId, LanguageCode).ToList();
            if (featureDepartments != null && featureDepartments.Count() > 0)
            {
                featureDepartmentResponse.FeatureDepartments = featureDepartments.Select(r =>
                                                new FeatureDept
                                                {
                                                    FeatureDepartmentId = r.FeatureDepartmentId, //r.HolidayDate.ToString("dd/MM/yyyy"),
                                                    DepartmentName = r.DepartmentName,
                                                    DepartmentCode = r.DepartmentCode,
                                                    IsActive = r.IsActive,
                                                    IsDeleted = r.IsDeleted,
                                                    CreatedBy = r.CreatedBy,
                                                    ModifiedBy = Convert.ToInt32(r.ModifiedBy),
                                                    CreatedOn = r.CreatedOn.ToString("dd/MM/yyyy"),
                                                    ModifiedOn =Convert.ToDateTime(r.ModifiedOn).ToString("dd/MM/yyyy")                                                 
                                                }).ToList();

                featureDepartmentResponse.Acknowledge = AcknowledgeType.Success;
                featureDepartmentResponse.Message = "Success";
            }
            else
            {
                featureDepartmentResponse.Acknowledge = AcknowledgeType.Failure;
                featureDepartmentResponse.Message = "Failed to get the Holiday List";
            }

            return featureDepartmentResponse;
        }
    }
    
}