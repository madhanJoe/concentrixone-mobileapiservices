﻿using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
public    class TrainingVisibleRepository : ITrainingVisibleRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public TrainingVisibleRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public TrainingVisibleResponse TrainingVisible(string employeeId)
        {
            return entityDBAccess.TrainingVisible(employeeId);
        }

    }
}
