﻿using Concentrix.MobileApp.Domain.DomainModels.Policy;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
public  class PolicyRepository : IPolicyRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public PolicyRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public AddPolicyResponse RequestPolicy(AddPolicyRequest policyrequest, string employeeId)
        {
            return entityDBAccess.RequestPolicy(policyrequest, employeeId);
        }
    }
}
