﻿using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
  public  class LeaveRepository : ILeaveRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public LeaveRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }
        public LeaveBalanceResponse GetLeaveBalance(LeaveBalanceRequest leaveBalance, string employeeId)
        {
            return entityDBAccess.GetLeaveBalance(leaveBalance, employeeId);
        }
    }
}
