﻿using Concentrix.MobileApp.Domain.DomainModels.Survey;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class SurveyRepository : ISurveyRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public SurveyRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public PendingSurveyResponse GetTrainingSurveyList(PendingSurveyRequest surveylist, string employeeId)
        {
            return entityDBAccess.GetTrainingSurveyList(surveylist,employeeId);
        }

        public PendingSurveyResponse GetmiHRSurveyList(PendingSurveyRequest mihrsurveylist, string employeeId)
        {
            return entityDBAccess.GetmiHRSurveyList(mihrsurveylist,employeeId);
        }

        public GetSurveyResponse RequestSurveyQuestions(GetSurveyRequest surveyquestions, string employeeId)
        {
            return entityDBAccess.RequestSurveyQuestions(surveyquestions,employeeId);
        }

        public SubmitSurveyResponse UpdateSurveyAnswer(SubmitSurveyRequest submitsurvey, string employeeId)
        {
            return entityDBAccess.UpdateSurveyAnswer(submitsurvey,employeeId);
        }
    }
}
