﻿using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class QuizRepository : IQuizRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public QuizRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public PendingQuizResponse GetQuizList(PendingQuizRequest quizlist, string employeeId)
        {
            return entityDBAccess.GetQuizList(quizlist, employeeId);
        }

        public GetQuizResponse RequestQuizQuestions(GetQuizRequest requestquizquestions, string employeeId)
        {
            return entityDBAccess.RequestQuizQuestions(requestquizquestions, employeeId);
        }

        public SubmitQuizResponse UpdateQuizAnswer(SubmitQuizRequest updatequizanswer, string employeeId)
        {
            return entityDBAccess.UpdateQuizAnswer(updatequizanswer, employeeId);
        }


        public QuizSummaryResponse QuizSummary(QuizSummaryRequest quizsummary, string employeeId)
        {
            return entityDBAccess.QuizSummary(quizsummary, employeeId);
        }
    }
}
