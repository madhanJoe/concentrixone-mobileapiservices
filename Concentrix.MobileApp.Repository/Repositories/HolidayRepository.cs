﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class HolidayRepository : IHolidayRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public HolidayRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public HolidayResponse GetHolidays(string employeeId, string LanguageCode)
        {
            return entityDBAccess.GetHolidays(employeeId, LanguageCode);
        }
    }
}
