﻿using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class RuleEngineRepository : IRuleEngineRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public RuleEngineRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }
        public FeatureResponse GenericFeatures(FeatureRequest reqFeature)
        {
            return entityDBAccess.GenericFeatures(reqFeature);
        }
    }
}
