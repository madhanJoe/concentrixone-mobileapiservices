//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Employee
    {
        public int ID { get; set; }
        public string Employee_Id { get; set; }
        public Nullable<int> UserType { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string DisplayName { get; set; }
        public string DOB { get; set; }
        public string Grade { get; set; }
        public string Designation { get; set; }
        public string Project_Code { get; set; }
        public string Location_Code { get; set; }
        public string Windows_NT_Id { get; set; }
        public string Supervisor_Id { get; set; }
        public string Supervisor_Name { get; set; }
        public string Supervisor_EmailId { get; set; }
        public string Status { get; set; }
        public string Phone_No { get; set; }
        public string Alternate_Phone_No { get; set; }
        public string Email_Id { get; set; }
        public string Alternate_Email_Id { get; set; }
        public string Twitter_Id { get; set; }
        public string Facebook_Id { get; set; }
        public string PF_No { get; set; }
        public string PF_UAN { get; set; }
        public string Password { get; set; }
        public string Hire_DT { get; set; }
        public Nullable<int> LoginType { get; set; }
        public Nullable<bool> Is_Mail_Active { get; set; }
        public string Sex { get; set; }
        public Nullable<System.DateTime> Creation_Date { get; set; }
        public string Creation_User_Id { get; set; }
        public Nullable<System.DateTime> Modification_Date { get; set; }
        public string Modification_User_Id { get; set; }
        public Nullable<System.Guid> OneviewId { get; set; }
        public Nullable<bool> Active { get; set; }
        public string DepartmentCode { get; set; }
        public string DesignationCode { get; set; }
    }
}
