﻿using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.Language;
using System.Threading;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public LanguageRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public LanguageResponse GetLanguages(LanguageRequest language)
        {
            return entityDBAccess.GetLanguages(language);
        }
    }
}

