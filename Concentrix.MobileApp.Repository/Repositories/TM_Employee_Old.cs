//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Employee_Old
    {
        public int ID { get; set; }
        public Nullable<int> UserType { get; set; }
        public string SecurityToken { get; set; }
        public Nullable<decimal> Record_Id { get; set; }
        public string PS_Emp_Id { get; set; }
        public string Alter_Emp_Id { get; set; }
        public string Windows_NT_Id { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Display_Name { get; set; }
        public string DOB { get; set; }
        public string Grade { get; set; }
        public string Designation { get; set; }
        public string Project_Description { get; set; }
        public string Site_Description { get; set; }
        public string Supervisor_Id { get; set; }
        public string Supervisor_Name { get; set; }
        public string Phone_No { get; set; }
        public string Alternate_Phone_No { get; set; }
        public string Email_Id { get; set; }
        public string Alternate_Email_Id { get; set; }
        public string HR_Status { get; set; }
        public string Twitter_Id { get; set; }
        public string Facebook_Id { get; set; }
        public string PF_No { get; set; }
        public Nullable<System.DateTime> PF_Last_Payment_Date { get; set; }
        public string PF_Tran_No { get; set; }
        public string PF_CRN_No { get; set; }
        public string PF_UAN { get; set; }
        public Nullable<double> Casual_Leave { get; set; }
        public Nullable<double> Sick_Leave { get; set; }
        public Nullable<double> Privilege_Leave { get; set; }
        public Nullable<System.DateTime> Goal_Setting_Date { get; set; }
        public Nullable<int> Direct_Reportees { get; set; }
        public Nullable<double> Goal_Setting_Completion_Percentage { get; set; }
        public Nullable<System.DateTime> Creation_Date { get; set; }
        public string Creation_User_Id { get; set; }
        public Nullable<System.DateTime> Modification_Date { get; set; }
        public string Modification_User_Id { get; set; }
        public Nullable<System.DateTime> Deletion_Date { get; set; }
        public string Deletion_User_Id { get; set; }
        public string Status { get; set; }
        public string vc_country_name { get; set; }
        public string PASSSWORD { get; set; }
        public string HIRE_DT { get; set; }
        public string LAST_HIRE_DT { get; set; }
        public Nullable<int> LoginType { get; set; }
        public string CITY { get; set; }
        public string COUNTRY { get; set; }
        public string JOBTITLE { get; set; }
        public Nullable<System.DateTime> LAST_UPDATED { get; set; }
        public Nullable<bool> Is_Mail_Active { get; set; }
        public string PHY_WRKLOC { get; set; }
        public string UserRole { get; set; }
        public string Geography { get; set; }
        public string SEX { get; set; }
        public string REG_TEMP { get; set; }
        public string FULL_PART_TIME { get; set; }
        public string TERMINATION_TYPE { get; set; }
        public string TERMINATION_REASON { get; set; }
        public Nullable<System.DateTime> LAST_DATE_WORKED { get; set; }
        public Nullable<decimal> HOURLY_RT { get; set; }
        public string CURRENCY_CD { get; set; }
    }
}
