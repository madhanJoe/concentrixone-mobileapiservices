﻿using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using Concentrix.MobileApp.Domain.DomainModels.Log;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class TokenRepository : ITokenRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public TokenRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public TokenDetailsResponse GetTokenDetails(TokenRequest token)
        {
            return entityDBAccess.GetTokenDetails(token);
        }

        public TokenValidityResponse ValidateAppNameToken(TokenRequest token, LogServiceCalls serviceLog)
        {
            return entityDBAccess.ValidateAppNameToken(token, serviceLog);
        }

        public void UpdateServiceLog(long serviceLogId, string acknowledge, string message)
        {
            entityDBAccess.UpdateServiceLog(serviceLogId, acknowledge, message);
            return;
        }

        public int LogToDatabase(Domain.DomainModels.Log.ErrorLog errorLog)
        {
            return entityDBAccess.LogToDatabase(errorLog);
        }
    }
}
