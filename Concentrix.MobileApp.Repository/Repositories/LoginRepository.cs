﻿using Concentrix.MobileApp.Domain.DomainModels.Login;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class LoginRepository : ILoginRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public LoginRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public LoginADResponse ValidateGuestSocial(LoginSocialRequest guestsocial)
        {
            return entityDBAccess.ValidateGuestSocial(guestsocial);
        }
        public LoginADResponse ValidateUser(LoginADRequest login)
        {
            return entityDBAccess.ValidateUser(login);
        }

        public LoginADResponse ValidateGuest(LoginADRequest guestlogin)
        {
            return entityDBAccess.ValidateGuest(guestlogin);
        }
        public ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest forgotpassword)
        {
            return entityDBAccess.ForgotPassword(forgotpassword);
        }
        public LoginADResponse GenerateToken(LoginADRequest login)
        {
            return entityDBAccess.GenerateToken(login);
        }
    }
}
