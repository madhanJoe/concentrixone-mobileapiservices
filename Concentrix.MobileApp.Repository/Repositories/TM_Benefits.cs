//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Benefits
    {
        public int AppId { get; set; }
        public string AppTitle { get; set; }
        public string AppCode { get; set; }
        public bool IsApp { get; set; }
        public string WebUrl { get; set; }
        public string AndroidPackageName { get; set; }
        public string AndroidMarketUrl { get; set; }
        public string IOSUrlScheme { get; set; }
        public int IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EmployeeId { get; set; }
        public string Location { get; set; }
    }
}
