﻿using Concentrix.MobileApp.Domain.DomainModels.Test;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class TestRepository : ITestRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public TestRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public HelloWorld DisplayHelloWorld()
        {
            return entityDBAccess.DisplayHelloWorld();
        }

        public TestLogin AuthenticateUser(string username, string password)
        {
            return entityDBAccess.AuthenticateUser(username, password);
        }

        public ADResponse AuthenticateLogin(TestLogin login)
        {
            return entityDBAccess.AuthenticateLogin(login);
        }
    }
}
