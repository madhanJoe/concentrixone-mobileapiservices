﻿using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class AttributeRepository : IAttributeRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public AttributeRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public AttributeResponse GetDemandLetterList(AttributeRequest attribute, string employeeId)
        {

            return entityDBAccess.GetDemandLetterList(attribute, employeeId);
        }

        public CountryResponse GetCountryCode(CountryRequest country)
        {
            return entityDBAccess.GetCountryCode(country);
        }

        public AttributeResponse GetPolicyList(AttributeRequest policy, string employeeId)
        {
            return entityDBAccess.GetPolicyList(policy, employeeId);
        }
    }
}
