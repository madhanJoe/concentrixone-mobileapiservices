﻿using Concentrix.MobileApp.Domain.DomainModels.Message;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
  public  class MessageRepository : IMessageRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public MessageRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public DeleteMultipleMessagesResponse DeleteMultipleMessage(DeleteMultipleMessagesRequest deletemultiple, string employeeId)
        {
            return entityDBAccess.DeleteMultipleMessage(deletemultiple, employeeId);
        }

        public DeleteAllMessagesResponse DeleteAllMessage(string employeeId)
        {
            return entityDBAccess.DeleteAllMessage(employeeId);
        }

        public DeleteMessageResponse DeleteMessage(DeleteMessageRequest deletemessage)
        {
            return entityDBAccess.DeleteMessage(deletemessage);
        }

        public UpdateMessageResponse UpdateMessageRead(UpdateMessageRequest updatemessage, string employeeId)

        {
            return entityDBAccess.UpdateMessageRead(updatemessage, employeeId);
        }

        public MessageResponse GetMessage(MessageRequest message, string employeeId)
        {
            return entityDBAccess.GetMessage(message, employeeId);
        }
    }
}
