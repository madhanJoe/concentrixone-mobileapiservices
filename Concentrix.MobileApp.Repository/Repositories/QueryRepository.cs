﻿using Concentrix.MobileApp.Domain.DomainModels.Query;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class QueryRepository : IQueryRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public QueryRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public GetAllQueryForEmployeeResponse GetQueryList(string employeeId)
        {
            return entityDBAccess.GetQueryList(employeeId);
        }
        public SubmitQueryResponse SubmitQuery(SubmitQueryRequest submitquery, string employeeId)
        {
            return entityDBAccess.SubmitQuery(submitquery, employeeId);
        }
    }
}

  
