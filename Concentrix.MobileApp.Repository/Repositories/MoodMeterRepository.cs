﻿using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
    public class MoodMeterRepository : IMoodMeterRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

    public MoodMeterRepository(IEntityFrameDBAccess entityFrameDBAccess)
    {
        this.entityDBAccess = entityFrameDBAccess;
    }

    public MoodMeterResponse GetMoodMeterAttributes(string employeeId)

    {
        return entityDBAccess.GetMoodMeterAttributes(employeeId);
    }

        public SubmitMoodMeterResponse SubmitMoodMeterRating(SubmitMoodMeterRequest submitrating,string employeeId)

        {
            return entityDBAccess.SubmitMoodMeterRating(submitrating, employeeId);
        }

    }
}
