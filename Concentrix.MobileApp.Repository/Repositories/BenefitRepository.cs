﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using Concentrix.MobileApp.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Repository.Repositories
{
   public class BenefitRepository : IBenefitRepository
    {
        private readonly IEntityFrameDBAccess entityDBAccess;

        public BenefitRepository(IEntityFrameDBAccess entityFrameDBAccess)
        {
            this.entityDBAccess = entityFrameDBAccess;
        }

        public BenefitsResponse GetBenefits(string employeeId, string languageCode)

        {
            return entityDBAccess.GetBenefits(employeeId, languageCode);
        }
    }
}
