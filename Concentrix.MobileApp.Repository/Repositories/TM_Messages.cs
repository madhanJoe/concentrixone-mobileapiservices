//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class TM_Messages
    {
        public long MessageId { get; set; }
        public string MessageContent { get; set; }
        public Nullable<int> ReadStatus { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> DeletedOn { get; set; }
        public string DeletedBy { get; set; }
        public string Status { get; set; }
        public string Subject { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> MessageType { get; set; }
        public string Notify_Type { get; set; }
        public Nullable<bool> IsNotified { get; set; }
        public string Survey_Type { get; set; }
        public string Quiz_Type { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
