//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Concentrix.MobileApp.Repository.Repositories
{
    using System;
    
    public partial class USP_GetBenefits_Result
    {
        public int AppId { get; set; }
        public string AppTitle { get; set; }
        public string AppCode { get; set; }
        public bool IsApp { get; set; }
        public string WebUrl { get; set; }
        public string AndroidPackageName { get; set; }
        public string AndroidMarketUrl { get; set; }
        public string IOSUrlScheme { get; set; }
    }
}
