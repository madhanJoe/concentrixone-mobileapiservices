﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Survey
{
 public   class SurveyTypes
    {
        public int SurveyUserId { get; set; }


        public int NoOfSurveys { get; set; }


        public string SurveyUserName { get; set; }


        public List<SurveyList> SurveyList { get; set; }

        public int SurveyisMapped { get; set; }
    }
}
