﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Survey
{
  public  class SurveyList
    {
        public int SurveyId { get; set; }

        public int NoOfQuestions { get; set; }

        public string SurveyTitle { get; set; }
    }
}
