﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Survey
{
  public  class GetSurveyRequest : BaseRequest
    { 
      //  public string EmployeeId { get; set; }

        public int SurveyId { get; set; }


        public int SurveyUserId { get; set; }

  
        public string SurveyName { get; set; }
    }
}
