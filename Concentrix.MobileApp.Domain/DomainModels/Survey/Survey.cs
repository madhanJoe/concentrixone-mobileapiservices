﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Survey
{
 public   class Survey
    {
        public int QuestionId { get; set; }


        public string Question { get; set; }

 
        public string Image { get; set; }

      
        public int Rating { get; set; }
    }
}
