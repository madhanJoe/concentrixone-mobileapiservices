﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Query
{
    public class SubmitQueryRequest : BaseRequest
    {
   
     //   public string EmployeeId { get; set; }

     
        public string QueryText { get; set; }
    }
}
