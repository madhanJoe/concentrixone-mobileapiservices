﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Query
{
  public  class Query
    {
        public int QueryId { get; set; }

 
        public string QueryText { get; set; }

     
        public string Status { get; set; }

      
        public int StatusId { get; set; }

    }
}
