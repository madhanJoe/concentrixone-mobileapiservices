﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Logout
{
  public  class LogoutRequest : BaseRequest
    {
      //  public string EmployeeId { get; set; }

        public string DeviceId { get; set; }
        public string EmailId { get; set; }
        public int UserType { get; set; }
    }
}
