﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Message
{
  public  class EmployeeSMS
    {
            public Int32 MessageId { get; set; }

         public string MessageContent { get; set; }
        public int MessageType { get; set; }
        public string SenderName { get; set; }

          public string Subject { get; set; }

          public int ReadStatus { get; set; }

       
    
         
    }
}
