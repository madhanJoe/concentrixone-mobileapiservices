﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Message
{
   public class DeleteMessageRequest : BaseRequest
    {
        public int MessageId { get; set; }
    }
}
