﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Message
{
    public class DeleteMultipleMessagesRequest : BaseRequest
    {
      
        public List<Int32> MessageIds { get; set; }
  
       // public string EmployeeId { get; set; }

    }
}
