﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Base
{
    public class BaseResponse
    {
        public AcknowledgeType Acknowledge { get; set; }
        public string Message { get; set; }
        public List<ErrorDetails> ErrorCode { get; set; } = new List<ErrorDetails>();
    }

    public enum AcknowledgeType
    {
        Failure = 0,
        Success = 1,
        Error = 2
    }

    public class ErrorDetails
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
