﻿
namespace Concentrix.MobileApp.Domain.DomainModels.Base
{
    public class BaseRequest
    {
        public string Token { get; set; }
        public string AppName { get; set; }
    }
}
