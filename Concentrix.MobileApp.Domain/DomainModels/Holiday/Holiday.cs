﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Holiday
{
    public class Holiday
    {
        public string Date { get; set; }
        public string DayName { get; set; }
        public string HolidayName { get; set; }
        public string HolidayType { get; set; }

        //public int Id { get; set; }
        //public string EmployeeId { get; set; }
        //public string Description { get; set; }
        //public string Date { get; set; }
        //public string Type { get; set; }
        //public string Month { get; set; }
        //public string DayName { get; set; }
        //public string Day { get; set; }
        //public string Year { get; set; }
        //public bool IsMandatory { get; set; }
    }
}
