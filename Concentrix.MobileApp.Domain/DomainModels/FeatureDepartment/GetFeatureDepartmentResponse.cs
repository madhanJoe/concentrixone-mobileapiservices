﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concentrix.MobileApp.Domain.DomainModels.Base;

namespace Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment
{
    public class GetFeatureDepartmentResponse :BaseResponse
    {
        public List<FeatureDept> FeatureDepartments { get; set; }
    }
}
