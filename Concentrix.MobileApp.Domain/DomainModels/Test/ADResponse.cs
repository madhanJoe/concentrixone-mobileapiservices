﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Test
{
    public class ADResponse
    {
        public string Status { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
