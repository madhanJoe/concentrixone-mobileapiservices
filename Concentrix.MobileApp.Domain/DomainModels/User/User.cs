﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.User
{
  public  class User
    {
        
        #region Public Properties

         public Int32 Id { get; set; }

        public string EmployeeId { get; set; }

        public string AlternateEmployeeId { get; set; }
        public string UserName { get; set; }

        public Profile Profile { get; set; }
        public string FullName { get; set; }

        
        public string FirstName { get; set; }

        
        public string LastName { get; set; }

        
        public string OfficeEmailAddress { get; set; }

        
        public string PersonalEmailAddress { get; set; }

        
        public string LastLoggedOn { get; set; }

        
        public Client Client { get; set; }

        
        public Program Program { get; set; }

       
        public string FederationId { get; set; }
        public string TwitterId { get; set; }

        public string FacebookId { get; set; }

        public string PhoneNo { get; set; }

        public string AlternatePhoneNo { get; set; }

        public string Designation { get; set; }

        public string Project { get; set; }

        public string Site { get; set; }

        public string Password { get; set; }

        public string Location { get; set; }

        public string DeviceId { get; set; }

        #endregion

    }
}
