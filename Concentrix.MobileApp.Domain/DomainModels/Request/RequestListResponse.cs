﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Request
{
  public  class RequestListResponse : BaseResponse
    {
        public List<RequestList> RequestList { get; set; }
    }
}
