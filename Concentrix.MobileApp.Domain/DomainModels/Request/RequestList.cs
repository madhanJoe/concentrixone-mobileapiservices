﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Request
{
    public class RequestList
    {
         
        public int RequestId { get; set; }

        
        public string RequestTitle { get; set; }

         
        public string RequestCode { get; set; }


         
        public bool IsAllowed { get; set; }
    }
}
