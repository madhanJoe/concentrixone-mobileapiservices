﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Request
{
    public class PfDetails
    {
        public string PF_No { get; set; }
        public string PF_Last_Payment_Date { get; set; }
        public string PF_Tran_No { get; set; }
        public string PF_CRN_No { get; set; }
        public string PF_UAN { get; set; }
    }
}
