﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Token
{
    public class TokenDetailsResponse : BaseResponse
    {
        public int TokenId { get; set; }
        public string TokenValue { get; set; }
        public string UserId { get; set; }
        public int UserType { get; set; }
        public string LanguageCode { get; set; }
        public string DeviceId { get; set; }
        public string LoginType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime TokenCreatedOn { get; set; }
        public DateTime TokenExpiresOn { get; set; }
        public string MobileRegion { get; set; }
        public string MobileCountry { get; set; }
        public string MobileCity { get; set; }
        public bool IsActive { get; set; }
    }
}
