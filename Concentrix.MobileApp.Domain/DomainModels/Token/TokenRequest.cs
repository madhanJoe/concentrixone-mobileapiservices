﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Token
{
    public class TokenRequest
    {
        public string Token { get; set; }
        public string AppName { get; set; }
        public string LanguageCode { get; set; }
    }
}
