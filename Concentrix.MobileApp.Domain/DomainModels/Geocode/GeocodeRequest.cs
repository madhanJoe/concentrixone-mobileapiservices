﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Geocode
{
    public class GeocodeRequest
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
