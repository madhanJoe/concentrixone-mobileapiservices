﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Geocode
{
    public class GeocodeResponse : BaseResponse
    {
        public string Country { get; set; }
        public string City { get; set; }
    }
}
