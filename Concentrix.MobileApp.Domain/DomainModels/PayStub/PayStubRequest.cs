﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.PayStub
{
    public class PayStubRequest : BaseRequest
    {
       // public string EmployeeID { get; set; }
        public string Code { get; set; }

    }
}
