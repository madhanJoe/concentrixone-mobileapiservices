﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Policy
{
 public  class AddPolicyRequest : BaseRequest
    {
     //   public string EmployeeId { get; set; }

      
        public string Comments { get; set; }

 
        public int PolicyId { get; set; }
    }
}
