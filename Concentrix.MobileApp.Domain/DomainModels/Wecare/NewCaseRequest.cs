﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Wecare
{
    public class NewCaseRequest : BaseRequest
    {
      //  public string EmployeeId { get; set; }


        public string Category { get; set; }
        
        public string SubCategory { get; set; }


        public string EmailId { get; set; }


        public string Phone { get; set; }



        public string Subject { get; set; }

        public string Details { get; set; }
    }
}
