﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Wecare
{
   public  class Wecare
    {
        
        public string EmployeeId { get; set; }
        
        public string Location { get; set; }
        
        public string Category { get; set; }
         
        public string SubCategory { get; set; }
         
        public string EmailId { get; set; }
        
        public string Phone { get; set; }
        
        public string Subject { get; set; }
         
        public string Details { get; set; }
         
        public string CaseID { get; set; }

 
        public string UserName { get; set; }

        
        public string WindowsNTID { get; set; }
    }
}
