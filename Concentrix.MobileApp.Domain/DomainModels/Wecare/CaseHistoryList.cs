﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Wecare
{
   public class CaseHistoryList
    {
        public string CaseId { get; set; }

   
        public string Status { get; set; }
      
        public string Source { get; set; }
      
        public string Category { get; set; }
      
        public string SubCategory { get; set; }

    }
}
