﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Wecare
{
   public class CaseCategory
    {
        public string Category { get; set; }
         
        public int CategoryID { get; set; }

         
        public string SubCategoryl { get; set; }
         public int SubCategoryID { get; set; }
         public List<CaseSubCategory> SubCategory { get; set; }


       
    }
}
