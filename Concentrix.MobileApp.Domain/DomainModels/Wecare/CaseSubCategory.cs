﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Wecare
{
   public class CaseSubCategory
    {
        public string SubCategory { get; set; }


        public int SubCategoryID { get; set; }
    }

}
