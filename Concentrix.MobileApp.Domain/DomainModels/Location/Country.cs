﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Location
{
   public class Country
    {
        public string CountryName { get; set; }


        public List<Location> Cities { get; set; }
    }
}
