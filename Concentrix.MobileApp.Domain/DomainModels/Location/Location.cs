﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Location
{
   public class Location
    {
        public int Id { get; set; }

        public string Name { get; set; }


        public string Address { get; set; }


        public string City { get; set; }


        public string State { get; set; }

        public string Country { get; set; }

        public string PinCode { get; set; }


        public string EmailAddress { get; set; }

        public string ContactNumber { get; set; }


        public string FaxNumber { get; set; }


        public string Latitude { get; set; }


        public string Longitude { get; set; }
    }
}
