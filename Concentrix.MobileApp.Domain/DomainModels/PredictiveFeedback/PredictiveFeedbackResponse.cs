﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback
{
   public class PredictiveFeedbackResponse : BaseResponse
    {
        public List<PredictiveFeedback> PredictiveFeedbackList { get; set; }
    }
}
