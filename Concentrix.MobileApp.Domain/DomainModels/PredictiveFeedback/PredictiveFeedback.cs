﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback
{
    public class PredictiveFeedback
    {
        public string PredictiveFeedbackText { get; set; }

        public string CreatedDate { get; set; }
    }
}
