﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Concentrix.MobileApp.Domain.DomainModels.Resource
{
    public class ResouceResponse : BaseResponse
    {
        public Newtonsoft.Json.Linq.JObject ResourceContent { get; set; }
    }
}
