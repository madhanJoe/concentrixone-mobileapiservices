﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
   public class QuizSummaryRequest : BaseRequest
    {
       
     //   public string EmployeeId { get; set; }

      
        public int QuizId { get; set; }

       
        public int QuizUserId { get; set; }

       
        public string QuizName { get; set; }

      
        public string CurrentDate { get; set; }
    }
}
