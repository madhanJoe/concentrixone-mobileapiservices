﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
  public  class QuizAnswers
    {
         
        public int AnswerId { get; set; }

        
        public int IsSelected { get; set; }

        
        public string Answer { get; set; }

         
        public int QuestionId { get; set; }

        
        public int Score { get; set; }
 
        public bool QuizTaken { get; set; }
    }
}
