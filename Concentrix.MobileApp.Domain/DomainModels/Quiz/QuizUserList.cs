﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
  public  class QuizUserList
    {
        public int QuizUserId { get; set; }

       
        public int NoOfQuizes { get; set; }

       
        public string QuizUserName { get; set; }

     
     
        public List<QuizList> QuizList { get; set; }

        public int QuizisMapped { get; set; }
        public int QuizUserIdmapped { get; set; }
        public string QuizUserNamemapped { get; set; }
    }
}
