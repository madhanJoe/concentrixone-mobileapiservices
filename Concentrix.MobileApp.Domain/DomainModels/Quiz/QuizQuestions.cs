﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
   public class QuizQuestions
    {
        
        public int QuestionId { get; set; }

         
        public string Question { get; set; }

         
        public int Navigation { get; set; }

         
        public List<QuizAnswers> Answers { get; set; }

         
        public string CorrectAnswer { get; set; }

        
        public List<int> CorrectAnswers { get; set; }

         
        public List<int> ChoicesSelected { get; set; }
 
        public int AnswerChoiceType { get; set; }

        
        public int NumberOfChoices { get; set; }

         
        public int CurrentScore { get; set; }

         
        public bool QuizTaken { get; set; }

         
    }
}
