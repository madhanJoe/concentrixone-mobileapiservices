﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
 public  class QuizList
    {
        public int QuizId { get; set; }

       
        public int NoOfQuestions { get; set; }

      
        public int QuestionsPerPage { get; set; }

       
        public string QuizTitle { get; set; }


        public int SurveyisMapped { get; set; }
    }
}
