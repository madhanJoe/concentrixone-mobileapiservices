﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Quiz
{
    public class PendingQuizResponse : BaseResponse
    {
        public List<QuizUserList> QuizUserList { get; set; }
    }
}
