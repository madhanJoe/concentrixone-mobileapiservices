﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Login
{
    public class LoginRequest : BaseRequest
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public int UserType { get; set; }

        public string Email { get; set; }

        public string DeviceId { get; set; }
    }
}
