﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Login
{
    public class ForgotPasswordRequest : BaseRequest
    {
        public string GuestEmail { get; set; }
        public int UserType { get; set; }
        public int LoginType { get; set; }
    }
}
