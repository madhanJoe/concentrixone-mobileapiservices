﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Login
{
    public class LoginSocialResponse : BaseResponse
    {
        public string Token { get; set; }
    }
}
