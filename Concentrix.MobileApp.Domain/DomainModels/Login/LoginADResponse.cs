﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Login
{
    public class LoginADResponse : BaseResponse
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Project { get; set; }
        public string Location { get; set; }
        public string EmployeeEmailId { get; set; }
        public string Token { get; set; }
        public string ProfileImageUrl { get; set; }
    }
}
