﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Login
{
    public class LoginADRequest : BaseRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Language { get; set; }
        public int UserType { get; set; }
        public string Email { get; set; }
        public string DeviceId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
