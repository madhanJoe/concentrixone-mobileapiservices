﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Attribute
{
    public class AttributeResponse : BaseResponse
    {
        public List<AttributeValue> AttributeValues { get; set; }
    }
}
