﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar
{
  public  class TrainingCalendarDetailsResponse : BaseResponse
    {

        public List<TrainingCalenderDetails> AllTrainingDetails { get; set; }
       
    }

    public class TrainingCalenderDetails
    {
        public int Id { get; set; }


        public string Date { get; set; }


        public string Venue { get; set; }


        public string StartTime { get; set; }

        public string EndTime { get; set; }


        public string Participants { get; set; }


        public string Facilitator { get; set; }


        public string Objectives { get; set; }


        public string Site { get; set; }


        public string Location { get; set; }


        public string Title { get; set; }


    }
}
