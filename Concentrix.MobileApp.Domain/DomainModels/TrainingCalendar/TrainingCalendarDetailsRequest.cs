﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar
{
   public class TrainingCalendarDetailsRequest : BaseRequest
    {
        public string TrainingCalendarId { get; set; }
    }
}
