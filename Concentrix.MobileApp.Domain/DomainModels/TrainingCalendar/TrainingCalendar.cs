﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar
{
   public class TrainingCalendar
    {
              
        public string TrainingCalendarId { get; set; }
         public string Date { get; set; }

         public string Title { get; set; }
    }
}
