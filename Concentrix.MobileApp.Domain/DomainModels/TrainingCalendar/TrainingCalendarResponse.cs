﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar
{
   public class TrainingCalendarResponse : BaseResponse
    {
        public List<TrainingCalendar> TrainingCalendarList { get; set; }
    }
}
