﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Log
{
    public class LogServiceCalls
    {
        public int ClientId { get; set; }
        public string URL { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Token { get; set; }
        public string Request { get; set; }
        public string Acknowledge { get; set; }
        public string Message { get; set; }
    }
}
