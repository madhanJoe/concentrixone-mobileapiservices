﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Log
{
    public class ErrorLog
    {
        public string URL { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string InnerException { get; set; }
        public string StackTrace { get; set; }
    }
}
