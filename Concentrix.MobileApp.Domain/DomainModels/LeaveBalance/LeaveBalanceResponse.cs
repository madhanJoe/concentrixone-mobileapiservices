﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.LeaveBalance
{
  public  class LeaveBalanceResponse : BaseResponse
    {
        public List<LeaveBalance> Leaves { get; set; }
    }
}
