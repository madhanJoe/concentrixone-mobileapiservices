﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class RegisterUserRequest : BaseRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalEmail { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public ContactNumber Mobile { get; set; }
        public string CountryId { get; set; }
    }
}
