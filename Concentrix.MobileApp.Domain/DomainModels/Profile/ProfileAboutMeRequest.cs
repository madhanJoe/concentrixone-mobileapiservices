﻿using Concentrix.MobileApp.Domain.DomainModels.Base;


namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class ProfileAboutMeRequest : BaseRequest
    {
        public string AboutMe { get; set; }
        public string EmployeeId { get; set; }
    }
}
