﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class VisaDetailRequest : BaseRequest
    {
        public string EmployeeId { get; set; }
        public int VisaId { get; set; }
        public string VisaCountryCode { get; set; }
        public string VisaCountryName { get; set; }
        public string VisaNumber { get; set; }
        public string TypeOfVisa { get; set; }
        public string VisaExpiryDate { get; set; }
        public ModeType Mode { get; set; }
    }

    public enum ModeType
    {
        Insert = 0,
        Update = 1,
        Delete = 2
    }
}