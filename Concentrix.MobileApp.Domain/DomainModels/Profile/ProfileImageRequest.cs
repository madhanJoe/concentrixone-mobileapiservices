﻿using Concentrix.MobileApp.Domain.DomainModels.Base;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class ProfileImageRequest : BaseRequest
    {
        public string EmployeeId { get; set; }
        public string ImageName { get; set; }

    }
}
