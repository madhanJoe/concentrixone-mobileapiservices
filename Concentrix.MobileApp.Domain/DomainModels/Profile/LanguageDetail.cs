﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class LanguageDetail
    {
        public int LanguageId { get; set; }
        public long LanguageApprovedId { get; set; }
        public string LanguageName { get; set; }
        public string Spoken { get; set; }
        public string Written { get; set; }
    }
}
