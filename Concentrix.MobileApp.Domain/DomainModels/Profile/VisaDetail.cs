﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class VisaDetail
    {
        public int VisaId { get; set; }
        public string VisaCountryCode { get; set; }
        public string VisaCountryName { get; set; }
        public string VisaNumber { get; set; }
        public string TypeOfVisa { get; set; }
        public string VisaExpiryDate { get; set; }
    }
}
