﻿using Concentrix.MobileApp.Domain.DomainModels.Base;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class EditableProfileRequest : BaseRequest
    {
        public string EmployeeId { get; set; }
        public ContactDetail ContactNumber { get; set; }
        public string Address { get; set; }
        public string EmergencyContactPerson { get; set; }
        public ContactDetail EmergencyContactNumber { get; set; }
        public string SkypeId { get; set; }
        public string WhatsAppId { get; set; }
    }

    public class ContactDetail
    {
        public string PhoneNumber { get; set; }
        public string ISDCode { get; set; }
        public string CountryCode { get; set; }
    }
}
