﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class LanguageDetailRequest : BaseRequest
    {
        public string EmployeeId { get; set; }
        public List<LanguageDetail> Languages { get; set; }
        public StringBuilder LanguageXML { get; set; }
    }
}
