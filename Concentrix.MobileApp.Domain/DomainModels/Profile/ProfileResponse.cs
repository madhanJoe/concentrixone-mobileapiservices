﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;


namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class ProfileResponse : BaseResponse
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string ProfileImageUrl { get; set; }
        public string EmployeeEmail { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public string SupervisorName { get; set; }
        public string Designation { get; set; }
        public ContactNumber EmployeeContactNumber { get; set; } = new ContactNumber();
        public string Address { get; set; }
        public string EmergencyContactPerson { get; set; }
        public ContactNumber EmergencyContactNumber { get; set; } = new ContactNumber();
        public string AboutMe { get; set; }
        public string SkypeId { get; set; }
        public string WhatsAppId { get; set; }
        public List<LanguageDetail> Languages { get; set; } = new List<LanguageDetail>();
        public List<VisaDetail> VisaDetails { get; set; } = new List<VisaDetail>();
        public ProfileVisibilityDetail ProfileVisibility { get; set; } = new ProfileVisibilityDetail();
    }

    public class ContactNumber
    {
        public string ISDCode { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }

}
