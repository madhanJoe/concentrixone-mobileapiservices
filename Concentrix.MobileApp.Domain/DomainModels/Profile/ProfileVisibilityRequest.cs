﻿using Concentrix.MobileApp.Domain.DomainModels.Base;

namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class ProfileVisibilityRequest : BaseRequest
    {
        public string EmployeeId { get; set; }
        public string Address { get; set; }
        public string EmergencyContactPerson { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string SkypeId { get; set; }
        public string WhatsAppId { get; set; }
        public string AboutMe { get; set; }
        public string Language { get; set; }
        public string Visa { get; set; }
    }
}