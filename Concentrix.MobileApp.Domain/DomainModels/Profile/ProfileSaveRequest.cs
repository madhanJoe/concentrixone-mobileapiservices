﻿using Concentrix.MobileApp.Domain.DomainModels.Base;


namespace Concentrix.MobileApp.Domain.DomainModels.Profile
{
    public class GuestUserInfoRequest : BaseRequest
    {
        public string Mobile { get; set; }
        public string TwitterId { get; set; }
        public string FacebookId { get; set; }
        public string OfficeEmail { get; set; }
        public string PersonalEmail { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public string CountryCode { get; set; }
    }
}
