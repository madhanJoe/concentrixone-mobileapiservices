﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Benefits
{
   public class BenefitsResponse : BaseResponse
    {
        public Benefits Benefits { get; set; }
    }
}
