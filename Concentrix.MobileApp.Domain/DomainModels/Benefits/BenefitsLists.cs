﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Benefits
{
   public class BenefitsLists
    {
        public string EmployeeId { get; set; }

        public int AppId { get; set; }

       
        public string AppTitle { get; set; }
     
        public string AppCode { get; set; }
     
        public bool IsApp { get; set; }
        
        public string WebUrl { get; set; }
   
        public string AndroidPackageName { get; set; }
    
        public string AndroidMarketUrl { get; set; }
       
        public string IOSUrlScheme { get; set; }

    }
}
