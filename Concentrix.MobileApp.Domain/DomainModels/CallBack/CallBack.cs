﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.CallBack
{
  public  class CallBack
    {
        public string EmailAddress { get; set; }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets or sets the FirstName.
        /// </summary>
        /// <value>The name of the user.</value>
        public string FirstName { get; set; }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets or sets the LastName.
        /// </summary>
        /// <value>The type of the user.</value>
        public string LastName { get; set; }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets or sets the CustomerName.
        /// </summary>
        /// <value>The type of the user.</value>
        public string CustomerName { get; set; }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets or sets the Comments.
        /// </summary>
        /// <value>The Comments.</value>
        public string Comments { get; set; }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets or sets the Reason.
        /// </summary>
        /// <value>The Reason.</value>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the contact number.
        /// </summary>
        /// <value>
        /// The contact number.
        /// </value>
        public string ContactNumber { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeId.
        /// </summary>
        /// <value>
        /// The contact number.
        /// </value>
      //  public string EmployeeId { get; set; }

        /// <summary>
        ///
        /// Gets or sets the CountryCode.
        /// </summary>
        /// <value>
        /// The Country Code.
        /// </value>
        public string CountryCode { get; set; }
    }
}
