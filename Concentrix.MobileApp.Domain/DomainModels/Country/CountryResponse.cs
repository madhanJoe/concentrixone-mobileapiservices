﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.Country
{
    public class CountryResponse : BaseResponse
    {
        public List<CountryDetail> Countries { get; set; }
    }

    public class CountryDetail
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ISDCode { get; set; }
    }
}
