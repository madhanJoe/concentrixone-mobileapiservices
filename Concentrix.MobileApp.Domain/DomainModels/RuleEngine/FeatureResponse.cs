﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.RuleEngine
{
   public  class FeatureResponse : Base.BaseResponse
    {
        public List<FeatureList> FeatureContent { get; set; }

        public List<FeatureSettingList> FeatureSettings { get; set; }

    }
}
