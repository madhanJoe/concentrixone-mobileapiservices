﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.RuleEngine
{
   public class FeatureList
    {
        public string ContentType { get; set; }
        public string ConcentTitle { get; set; }
        public string ContentDescription { get; set; }
        public string BackRoundImageURL { get; set; }
        public string Content { get; set; }
    }
}
