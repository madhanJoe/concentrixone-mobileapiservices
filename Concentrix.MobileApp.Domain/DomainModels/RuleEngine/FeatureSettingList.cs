﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.RuleEngine
{
  public class FeatureSettingList
    {
        public string FeatureCode { get; set; }
        public string Key{ get; set; }
        public string Value{ get; set; }
    }
}
