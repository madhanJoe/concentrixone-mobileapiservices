﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.RuleEngine
{
    public class FeatureRequest : Base.BaseRequest
    {
        public string FeatureCode { get; set; }
        public string EmployeeId { get; set; }
        public string LanguageCode { get; set; }
    }
}
