﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.TrainingVisible
{
    public class TrainingVisibleResponse : BaseResponse
    {
        public bool IsVisible { get; set; }
    }
}
