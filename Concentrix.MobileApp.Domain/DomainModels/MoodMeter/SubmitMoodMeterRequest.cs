﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.MoodMeter
{
  public  class SubmitMoodMeterRequest : BaseRequest
    {
      //  public string EmployeeID { get; set; }

     
        public int MoodMeterID { get; set; }
    }
}
