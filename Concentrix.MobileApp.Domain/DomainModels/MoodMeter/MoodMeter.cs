﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Domain.DomainModels.MoodMeter
{
    public class MoodMeter
    {
        public int MoodMeterAttributeId { get; set; }

        public string MoodMeterAttribute { get; set; }


        public string MoodMeterCode { get; set; }
    }
}
