﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Helper.DependecyInjection;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace Concentrix.MobileApp.WebApi.Handler
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        private readonly ITokenManager tokenManager;

        public GlobalExceptionHandler()
        {
            tokenManager = DIServiceLocator.Resolve<ITokenManager>();
        }
        public async override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            var url = string.Empty;
            var source = string.Empty;
            var stackTrace = string.Empty;
            var message = string.Empty;
            var innerException = string.Empty;

            var errorResponse = new BaseResponse
            {
                Acknowledge = AcknowledgeType.Error,
                Message = context.Exception.Message
            };

            if (context.Exception.InnerException != null)
            {
                innerException = context.Exception.InnerException.ToString();
                errorResponse.ErrorCode.Add(new ErrorDetails { Code = "99999", Description = innerException });
            }

            if (context.Exception.StackTrace != null)
            {
                stackTrace = context.Exception.StackTrace;
                errorResponse.ErrorCode.Add(new ErrorDetails { Code = "99998", Description = stackTrace });
            }

            if (context.Exception.Source != null)
                source = context.Exception.Source;

            if (context.Exception.Message != null)
                message = context.Exception.Message;

            url = context.Request.RequestUri.AbsoluteUri;

            var response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, errorResponse);

            var result = await LogToDatabase(url, message, source, innerException, stackTrace);

            //errorDetails.Headers.Add("MobileApp-Error", errorMessage);
            context.Result = new ResponseMessageResult(response);
        }

        private Task<bool> LogToDatabase(string url, string message, string source, string innerException, string stackTrace)
        {
            var errorLog = new ErrorLog { URL = url, Message = message, Source = source, InnerException = innerException, StackTrace = stackTrace };

            var response = tokenManager.LogToDatabase(errorLog);

            return Task.FromResult<bool>(true);
        }
    }
}