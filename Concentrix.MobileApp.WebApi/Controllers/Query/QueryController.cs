﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Query;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Query
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class QueryController : ApiBaseController
    {
        private readonly IQueryManager queryManager;
   
        public QueryController()
        {
            queryManager = DIServiceLocator.Resolve<IQueryManager>();
        }


        [HttpPost, Route("GetQueryList")]
        public IHttpActionResult GetQueryList([FromBody] GetAllQueryForEmployeeRequest query, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(query);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(query.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = queryManager.GetQueryList(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("SubmitQuery")]
        public IHttpActionResult SubmitQuery([FromBody] SubmitQueryRequest submitQuery, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(submitQuery);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(submitQuery.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = queryManager.SubmitQuery(submitQuery, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
