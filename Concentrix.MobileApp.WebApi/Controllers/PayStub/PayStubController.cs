﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.PayStub;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.PayStub
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class PayStubController : ApiBaseController
    {
        private readonly IPayStubManager paystubManager;
        
        public PayStubController()
        {
            paystubManager = DIServiceLocator.Resolve<IPayStubManager>();
        }

        [HttpPost, Route("PayStub/Payslip")]
        public IHttpActionResult GetPaySlip([FromBody] PayStubRequest paySlip, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(paySlip);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(paySlip.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = paystubManager.GetPaySlip(paySlip, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("PayStub/Demandletter")]
        public IHttpActionResult GetDemandLetter([FromBody] PayStubRequest demandLetter, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(demandLetter);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(demandLetter.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = paystubManager.GetDemandLetter(demandLetter, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("PayStub/Paystub")]
        public IHttpActionResult GetPayStubDetails([FromBody] PayStubRequest payStub, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(payStub);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(payStub.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = paystubManager.GetPayStubDetails(payStub, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

    }
}
