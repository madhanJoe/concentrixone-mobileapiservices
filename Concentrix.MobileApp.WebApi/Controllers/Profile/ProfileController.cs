﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml.Serialization;

namespace Concentrix.MobileApp.WebApi.Controllers.Profile
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class ProfileController : ApiBaseController
    {
        private readonly IProfileManager profileManager;

        public ProfileController()
        {
            profileManager = DIServiceLocator.Resolve<IProfileManager>();
        }

        [HttpPost, Route("RegisterUser")]
        public IHttpActionResult RegisterUser([FromBody] RegisterUserRequest guestInfo, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(guestInfo);

            long serviceLogId = 0;

            var tokenResponse = ValidateAppName(guestInfo.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, tokenResponse.Acknowledge.ToString(), tokenResponse.Message);
                return Ok(tokenResponse);
            }

            var response = profileManager.RegisterUser(guestInfo, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("SaveProfile")]
        public IHttpActionResult SaveProfile([FromBody] GuestUserInfoRequest saveProfile, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(saveProfile);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(saveProfile.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = profileManager.SaveProfile(saveProfile, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("GetProfile")]
        public IHttpActionResult GetProfile([FromBody] ProfileRequest profile, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(profile);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(profile.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = profileManager.GetProfile(tokenResponse.UserId, tokenResponse.LanguageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("SaveProfileDescription")]
        public IHttpActionResult SaveProfileAboutMe([FromBody] ProfileAboutMeRequest aboutMe, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(aboutMe);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(aboutMe.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            aboutMe.EmployeeId = tokenResponse.UserId;

            var response = profileManager.SaveProfileAboutMe(aboutMe, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateProfileVisibility")]
        public IHttpActionResult UpdateProfileVisibility([FromBody] ProfileVisibilityRequest profileVisibility, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(profileVisibility);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(profileVisibility.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            profileVisibility.EmployeeId = tokenResponse.UserId;

            var response = profileManager.UpdateProfileVisibility(profileVisibility, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateVisaDetail")]
        public IHttpActionResult UpdateVisaDetail([FromBody] VisaDetailRequest visaDetail, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(visaDetail);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(visaDetail.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            visaDetail.EmployeeId = tokenResponse.UserId;

            var response = profileManager.UpdateVisaDetail(visaDetail, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateEditableProfile")]
        public IHttpActionResult UpdateEditableProfile([FromBody] EditableProfileRequest editableProfile, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(editableProfile);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(editableProfile.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            editableProfile.EmployeeId = tokenResponse.UserId;

            var response = profileManager.UpdateEditableProfile(editableProfile, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateLanguageDetail")]
        public IHttpActionResult UpdateLanguageDetail([FromBody] LanguageDetailRequest languageDetail, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(languageDetail.Languages);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(languageDetail.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            languageDetail.EmployeeId = tokenResponse.UserId;

            var response = profileManager.UpdateLanguageDetail(languageDetail, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        #region Image Upload

        [HttpPost, Route("UploadProfileImage")]
        public IHttpActionResult UploadProfileImage()
        {
            var profileImageResponse = new ProfileImageResponse();

            var httpRequest = HttpContext.Current.Request;

            NameValueCollection formCollection = HttpContext.Current.Request.Form;

            var profileImage = new ProfileImageRequest();

            foreach (string key in formCollection.AllKeys)
            {
                PropertyInfo propInfo = profileImage.GetType().GetProperty(key, BindingFlags.Public | BindingFlags.Instance);

                if (propInfo != null)
                {
                    propInfo.SetValue(profileImage, formCollection[key], null);
                }
            }

            var requestJson = JsonConvert.SerializeObject(profileImage);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(profileImage.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                profileImageResponse = new ProfileImageResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, profileImageResponse.Acknowledge.ToString(), profileImageResponse.Message);

                return Ok(profileImageResponse);
            }

            profileImage.EmployeeId = tokenResponse.UserId;

            if (httpRequest.Files.Count > 0)
            {
                var profileImagePath = ConfigurationManager.AppSettings["ProfileImagePath"];

                if (string.IsNullOrEmpty(profileImagePath))
                {
                    profileImageResponse = new ProfileImageResponse
                    {
                        Acknowledge = AcknowledgeType.Failure,
                        Message = "Profile Image path is not configured"
                    };

                    UpdateServiceLog(serviceLogId, profileImageResponse.Acknowledge.ToString(), profileImageResponse.Message);

                    return Ok(profileImageResponse);
                }

                if ((string.IsNullOrEmpty(tokenResponse.UserId)) || (tokenResponse.UserId.Length <= 0))
                {
                    profileImageResponse = new ProfileImageResponse
                    {
                        Acknowledge = AcknowledgeType.Failure,
                        Message = "Invalid Employee Id"
                    };

                    UpdateServiceLog(serviceLogId, profileImageResponse.Acknowledge.ToString(), profileImageResponse.Message);

                    return Ok(profileImageResponse);
                }

                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];

                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1;     // Restricting to 1 MB

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };

                        var inputFileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.')).ToLower();

                        DateTime dt = DateTime.Now;//

                        string strFileName = postedFile.FileName.Replace(inputFileExtension, "");//
                        strFileName= strFileName+(dt.ToString("MMM-dd-yyyy") + dt.Hour.ToString() + dt.Minute.ToString() + dt.Millisecond.ToString() + dt.GetHashCode()).Replace("-", "");//

                        strFileName = strFileName + inputFileExtension;//


                        if (!AllowedFileExtensions.Contains(inputFileExtension))
                            profileImageResponse.ErrorCode.Add(new ErrorDetails { Code = "10665", Description = "Please upload image of type .jpg, .jpeg, .gif, .png" });

                        if (postedFile.ContentLength > MaxContentLength)
                            profileImageResponse.ErrorCode.Add(new ErrorDetails { Code = "10666", Description = "Please upload image file less than 1 MB" });

                        if (profileImageResponse.ErrorCode.Count > 0)
                        {
                            profileImageResponse.Acknowledge = AcknowledgeType.Failure;
                            profileImageResponse.Message = "Error on updating the profile image";
                        }
                        else
                        {
                            //var filePath = HttpContext.Current.Server.MapPath("~/MediaFiles/ProfileImages/" + postedFile.FileName + inputFileExtension);

                            var pathWithEmployeeId = profileImagePath + profileImage.EmployeeId + @"\Pending\";

                            if (!Directory.Exists(pathWithEmployeeId))
                            {
                                Directory.CreateDirectory(pathWithEmployeeId);
                            }

                            profileImage.ImageName = strFileName;//postedFile.FileName;

                            pathWithEmployeeId = pathWithEmployeeId + strFileName;// postedFile.FileName;

                            postedFile.SaveAs(pathWithEmployeeId);

                            var response = profileManager.UpdateProfileImageName(profileImage);

                            profileImageResponse.Acknowledge = response.Acknowledge;
                            profileImageResponse.Message = response.Message;

                            break;
                        }
                    }
                }
            }
            else
            {
                profileImageResponse.Acknowledge = AcknowledgeType.Failure;
                profileImageResponse.Message = "Please upload the profile image";
            }

            return Ok(profileImageResponse);
        }

        #endregion
    }
}
