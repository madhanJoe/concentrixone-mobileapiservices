﻿
using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Test;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Test
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class TestController : ApiBaseController
    {
        private readonly ITestManager testManager;

        public TestController()
        {
            testManager = DIServiceLocator.Resolve<ITestManager>();
        }

        [HttpGet, Route("helloworld")]
        public IHttpActionResult DisplayHelloWorld()
        {
            var response = testManager.DisplayHelloWorld();
            return Ok(response);
        }

        [HttpGet, Route("login/{username}/{password}")]
        public IHttpActionResult AuthenticateUser([FromUri] string username, string password, CancellationToken cancellationToken)
        {
            var response = testManager.AuthenticateUser(username, password, cancellationToken);
            return Ok(response);
        }

        [HttpPost, Route("login/concentrix")]
        public IHttpActionResult AuthenticateLogin([FromBody] TestLogin login, CancellationToken cancellationToken)
        {
            var response = testManager.AuthenticateLogin(login, cancellationToken);
            return Ok(response);
        }
    }
}
