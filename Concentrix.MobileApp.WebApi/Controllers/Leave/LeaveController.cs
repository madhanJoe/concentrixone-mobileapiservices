﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.LeaveBalance;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Leave
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class LeaveController : ApiBaseController
    {
        private readonly ILeaveManager leaveManager;

        public LeaveController()
        {
            leaveManager = DIServiceLocator.Resolve<ILeaveManager>();
        }


        [HttpPost, Route("GetLeaveBalance")]
        public IHttpActionResult GetLeaveBalance([FromBody] LeaveBalanceRequest leaveBalance, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(leaveBalance);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(leaveBalance.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = leaveManager.GetLeaveBalance(leaveBalance, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
