﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.MoodMeter;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.MoodMeter
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class MoodMeterController : ApiBaseController
    {

        private readonly IMoodMeterManager trainingManager;
     
        public MoodMeterController()
        {
            trainingManager = DIServiceLocator.Resolve<IMoodMeterManager>();
        }


        [HttpPost, Route("MoodMeterDetails")]
        public IHttpActionResult GetMoodMeterAttributes([FromBody] MoodMeterRequest mmAttributes, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(mmAttributes);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(mmAttributes.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = trainingManager.GetMoodMeterAttributes(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("SubmitMoodmeter")]
        public IHttpActionResult SubmitMoodMeterRating([FromBody] SubmitMoodMeterRequest submitRating, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(submitRating);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(submitRating.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = trainingManager.SubmitMoodMeterRating(submitRating, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
