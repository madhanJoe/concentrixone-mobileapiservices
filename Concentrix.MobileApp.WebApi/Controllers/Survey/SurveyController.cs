﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Survey;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Survey
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class SurveyController : ApiBaseController
    {
        private readonly ISurveyManager quizManager;

        public SurveyController()
        {
            quizManager = DIServiceLocator.Resolve<ISurveyManager>();
        }


        [HttpPost, Route("GetTrainingSurveyList")]
        public IHttpActionResult GetTrainingSurveyList([FromBody] PendingSurveyRequest surveyList, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(surveyList);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(surveyList.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.GetTrainingSurveyList(surveyList,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("GetmiHRSurveyList")]
        public IHttpActionResult GetmiHRSurveyList([FromBody] PendingSurveyRequest mihrSurveyList, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(mihrSurveyList);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(mihrSurveyList.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.GetmiHRSurveyList(mihrSurveyList, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("RequestSurveyQuestions")]
        public IHttpActionResult RequestSurveyQuestions([FromBody] GetSurveyRequest surveyQuestions, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(surveyQuestions);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(surveyQuestions.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.RequestSurveyQuestions(surveyQuestions, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateSurveyAnswer")]
        public IHttpActionResult UpdateSurveyAnswer([FromBody] SubmitSurveyRequest submitSurvey, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(submitSurvey);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(submitSurvey.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.UpdateSurveyAnswer(submitSurvey, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

    }
}
