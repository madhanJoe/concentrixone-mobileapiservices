﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.TrainingCalendar;

using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.TrainingCalendar
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class TrainingCalendarController : ApiBaseController
    {
        private readonly ITrainingCalendarManager trainingcalendarManager;
        string employeeId = string.Empty;

        public TrainingCalendarController()
        {
            trainingcalendarManager = DIServiceLocator.Resolve<ITrainingCalendarManager>();
        }

        [HttpPost, Route("TrainingCalendarList")]
        public IHttpActionResult GetTrainingCalendarList([FromBody] TrainingCalendarRequest trainingCalendarList, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(trainingCalendarList);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(trainingCalendarList.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = trainingcalendarManager.GetTrainingCalendarList(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("TrainingCalendarDetails")]
        public IHttpActionResult GetTrainingCalendarDetails([FromBody] TrainingCalendarDetailsRequest trainingCalendarDetails, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(trainingCalendarDetails);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(trainingCalendarDetails.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = trainingcalendarManager.GetTrainingCalendarDetails(trainingCalendarDetails, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
