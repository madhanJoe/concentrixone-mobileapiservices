﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.PredictiveFeedback;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.PredictiveFeedback
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class PredictiveFeedbackController : ApiBaseController
    {
        private readonly IPredictiveFeedbackManager predictivefeedbackManager;
 
        public PredictiveFeedbackController()
        {
            predictivefeedbackManager = DIServiceLocator.Resolve<IPredictiveFeedbackManager>();
        }

        [HttpPost, Route("GetPredictiveFeedback")]
        public IHttpActionResult GetPredictiveFeedback([FromBody] PredictiveFeedbackRequest predictiveFeedback, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(predictiveFeedback);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(predictiveFeedback.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = predictivefeedbackManager.GetPredictiveFeedback(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
