﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Language;
using Concentrix.MobileApp.Domain.DomainModels.Profile;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Language
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class LanguageController : ApiBaseController
    {
        private readonly ILanguageManager languageManager;

        public LanguageController()
        {
            languageManager = DIServiceLocator.Resolve<ILanguageManager>();
        }
        [HttpPost, Route("GetLanguages")]
        public IHttpActionResult GetLanguages([FromBody] LanguageRequest language, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(language);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(language.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = languageManager.GetLanguages(language, cancellationToken);

            return Ok(response);
        }

    }
}
