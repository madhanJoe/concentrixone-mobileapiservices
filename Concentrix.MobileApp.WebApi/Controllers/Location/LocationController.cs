﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Location;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Location
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class LocationController : ApiBaseController
    {
        private readonly ILocationManager benefitManager;

        string locationCode = string.Empty;
     
        public LocationController()
        {
            benefitManager = DIServiceLocator.Resolve<ILocationManager>();
        }

        [HttpPost, Route("GetLocations")]
        public IHttpActionResult GetLocations([FromBody] BaseRequest location, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(location);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(location.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = benefitManager.GetLocations(tokenResponse.UserId,locationCode, tokenResponse.LanguageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
