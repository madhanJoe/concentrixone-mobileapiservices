﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Quiz;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Quiz
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class QuizController : ApiBaseController
    {
        private readonly IQuizManager quizManager;
   
        public QuizController()
        {
            quizManager = DIServiceLocator.Resolve<IQuizManager>();
        }

        [HttpPost, Route("PendingQuiz")]
        public IHttpActionResult GetQuizList([FromBody] PendingQuizRequest quizList, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(quizList);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(quizList.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.GetQuizList(quizList,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("QuizQuestions")]
        public IHttpActionResult RequestQuizQuestions([FromBody] GetQuizRequest requestQuizQuestions, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(requestQuizQuestions);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(requestQuizQuestions.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.RequestQuizQuestions(requestQuizQuestions,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateQuizAnswer")]
        public IHttpActionResult UpdateQuizAnswer([FromBody] SubmitQuizRequest updateQuizAnswer, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(updateQuizAnswer);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(updateQuizAnswer.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.UpdateQuizAnswer(updateQuizAnswer,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("QuizSummary")]
        public IHttpActionResult QuizSummary([FromBody] QuizSummaryRequest quizSummary, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(quizSummary);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(quizSummary.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = quizManager.QuizSummary(quizSummary,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
