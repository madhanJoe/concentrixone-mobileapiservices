﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Geocode;
using Concentrix.MobileApp.Domain.DomainModels.Login;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Login
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class LoginController : ApiBaseController
    {
        private readonly ILoginManager loginManager;

        public LoginController()
        {
            loginManager = DIServiceLocator.Resolve<ILoginManager>();
        }

        [HttpPost, Route("ValidateGuestSocial")]
        public IHttpActionResult ValidateGuestSocial([FromBody] LoginSocialRequest guestSocial, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(guestSocial);

            long serviceLogId = 0;

            var tokenResponse = ValidateAppName(guestSocial.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, tokenResponse.Acknowledge.ToString(), tokenResponse.Message);
                return Ok(tokenResponse);
            }

            var response = loginManager.ValidateGuestSocial(guestSocial, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("ValidateUser")]
        public IHttpActionResult ADAuthentication([FromBody] LoginADRequest login, CancellationToken cancellationToken)
        {
            var ignorePassword = new LoginADRequest
                                    {
                                        UserName = login.UserName,
                                        Password = "",
                                        Language = login.Language,
                                        UserType = login.UserType,
                                        Email = login.Email,
                                        DeviceId = login.DeviceId,
                                        Latitude = login.Latitude,
                                        Longitude = login.Longitude
                                    };

            var requestJson = JsonConvert.SerializeObject(ignorePassword);

            long serviceLogId = 0;

            var tokenResponse = ValidateAppName(login.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, tokenResponse.Acknowledge.ToString(), tokenResponse.Message);
                return Ok(tokenResponse);
            }

            string applicationMode = string.Empty;

            var response = loginManager.ADAuthentication(login, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("ValidateGuest")]
        public IHttpActionResult ValidateGuest([FromBody] LoginADRequest guestLogin, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(guestLogin);

            long serviceLogId = 0;

            var tokenResponse = ValidateAppName(guestLogin.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, tokenResponse.Acknowledge.ToString(), tokenResponse.Message);
                return Ok(tokenResponse);
            }

            var response = loginManager.ValidateGuest(guestLogin, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("ForgotPassword")]
        public IHttpActionResult ForgotPassword([FromBody] ForgotPasswordRequest forgotPassword, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(forgotPassword);

            long serviceLogId = 0;

            var tokenResponse = ValidateAppName(forgotPassword.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != Domain.DomainModels.Base.AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, tokenResponse.Acknowledge.ToString(), tokenResponse.Message);
                return Ok(tokenResponse);
            }

            var response = loginManager.ForgotPassword(forgotPassword, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        private GeocodeResponse GetCountryCity(GeocodeRequest geocode)
        {
            var response = new GeocodeResponse();
            var geocodeUrl = ConfigurationManager.AppSettings["GoogleGeoCode"];
            // http://maps.googleapis.com/maps/api/geocode/json?latlng=12.9715987,77.5945627&sensor=true

            try
            {
                if ((string.IsNullOrEmpty(geocode.Latitude)) || (string.IsNullOrEmpty(geocode.Longitude)) || (string.IsNullOrEmpty(geocodeUrl)))
                {
                    response.Acknowledge = Domain.DomainModels.Base.AcknowledgeType.Failure;
                    response.Message = "Missing values of Latitude or Latitude or geocode URL";
                    return response;
                }

                var geocodeUrlValues = geocodeUrl + "?latlng=" + geocode.Latitude + "," + geocode.Longitude + "&sensor=true";


            }
            catch
            {

            }

            return response;
        }

        private HttpClient GetHttpClient(string baseUri)
        {
            HttpClient httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri(baseUri);

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }
    }
}
