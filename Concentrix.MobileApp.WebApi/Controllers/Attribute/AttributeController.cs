﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Attribute;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Country;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Attribute
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class AttributeController : ApiBaseController
    {
        private readonly IAttributeManager attributeManager;
       
        public AttributeController()
        {
            attributeManager = DIServiceLocator.Resolve<IAttributeManager>();
        }

        [HttpPost, Route("DemandLetterList")]
        public IHttpActionResult GetDemandLetterList([FromBody] AttributeRequest payStub, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(payStub);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(payStub.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);
                //Task.Run(() => UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message));

                return Ok(errorResponse);
            }

            var response = attributeManager.GetDemandLetterList(payStub, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }


        [HttpPost, Route("GetCountryCode")]
        public IHttpActionResult GetCountryCode([FromBody] CountryRequest country, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(country);

            long serviceLogId = 0;

            // var tokenResponse = GetTokenDetails(country.AppName, requestJson, out serviceLogId);
            var tokenResponse = ValidateAppName(country.AppName, requestJson, out serviceLogId);
            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = attributeManager.GetCountryCode(country, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("GetPolicyList")]
        public IHttpActionResult GetPolicyList([FromBody] AttributeRequest policy, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(policy);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(policy.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = attributeManager.GetPolicyList(policy, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
