﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.URL;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.URL
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class URLController : ApiBaseController
    {
        private readonly IURLManager urlManager;
        public URLController()
        {
            urlManager = DIServiceLocator.Resolve<IURLManager>();
        }


        [HttpPost, Route("GetURL")]
        public IHttpActionResult GetURL([FromBody] URLRequest url, CancellationToken cancellationToken)
        {
            var response = urlManager.GetURL(url, cancellationToken);
            return Ok(response);
        }
    }
}
