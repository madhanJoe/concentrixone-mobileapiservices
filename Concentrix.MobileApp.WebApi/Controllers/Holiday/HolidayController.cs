﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Holiday;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Holiday
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class HolidayController : ApiBaseController
    {
        private readonly IHolidayManager holidayManager;

        public HolidayController()
        {
            holidayManager = DIServiceLocator.Resolve<IHolidayManager>();
        }

        [HttpPost, Route("GetHolidays")]
        public IHttpActionResult GetHolidays([FromBody] HolidayRequest holiday, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(holiday);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(holiday.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = holidayManager.GetHolidays(tokenResponse.UserId, tokenResponse.LanguageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
