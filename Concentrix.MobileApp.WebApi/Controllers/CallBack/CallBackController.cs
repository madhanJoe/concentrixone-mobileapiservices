﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.CallBack;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.CallBack
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class CallBackController : ApiBaseController
    {
        private readonly ICallBackManager callbackManager;
        
        public CallBackController()
        {
            callbackManager = DIServiceLocator.Resolve<ICallBackManager>();
        }

        [HttpPost, Route("RequestCallback")]
        public IHttpActionResult RequestCallback([FromBody] CallBackRequest callback, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(callback);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(callback.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = callbackManager.RequestCallback(callback, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
