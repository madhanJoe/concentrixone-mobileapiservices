﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Message;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Message
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class MessageController : ApiBaseController
    {
        private readonly IMessageManager messageManager;
        string employeeId = string.Empty;
        public MessageController()
        {
            messageManager = DIServiceLocator.Resolve<IMessageManager>();
        }

        [HttpPost, Route("DeleteMultipleMessage")]
        public IHttpActionResult DeleteMultipleMessage([FromBody] DeleteMultipleMessagesRequest deleteMultiple, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(deleteMultiple);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(deleteMultiple.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = messageManager.DeleteMultipleMessage(deleteMultiple, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("DeleteAllMessage")]
        public IHttpActionResult DeleteAllMessage([FromBody] DeleteAllMessagesRequest deleteAll, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(deleteAll);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(deleteAll.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = messageManager.DeleteAllMessage(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("DeleteMessage")]
        public IHttpActionResult DeleteMessage([FromBody] DeleteMessageRequest deleteMessage, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(deleteMessage);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(deleteMessage.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = messageManager.DeleteMessage(deleteMessage, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("UpdateMessageRead")]
        public IHttpActionResult UpdateMessageRead([FromBody] UpdateMessageRequest updateMessage, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(updateMessage);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(updateMessage.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = messageManager.UpdateMessageRead(updateMessage, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("GetMessage")]
        public IHttpActionResult GetMessage([FromBody] MessageRequest message, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(message);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(message.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = messageManager.GetMessage(message, tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
