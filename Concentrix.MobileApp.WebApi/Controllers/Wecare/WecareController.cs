﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Wecare;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Wecare
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class WecareController : ApiBaseController
    {
        private readonly IWecareManager wecareManager;
        string employeeId = string.Empty;
        public WecareController()
        {
            wecareManager = DIServiceLocator.Resolve<IWecareManager>();
        }

        [HttpPost, Route("GetCaseCategory")]
        public IHttpActionResult GetCaseCategory([FromBody] CaseCategoryRequest caseCategory, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(caseCategory);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(caseCategory.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = wecareManager.GetCaseCategory(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("CreateNewCase")]
        public IHttpActionResult CreateNewCase([FromBody] NewCaseRequest newCase, string employeeId, string languageCode, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(newCase);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(newCase.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = wecareManager.CreateNewCase(newCase, tokenResponse.UserId, languageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }

        [HttpPost, Route("GetCaseHistory")]
        public IHttpActionResult GetCaseHistory([FromBody] CaseHistoryRequest caseHistory, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(caseHistory);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(caseHistory.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = wecareManager.GetCaseHistory(tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
