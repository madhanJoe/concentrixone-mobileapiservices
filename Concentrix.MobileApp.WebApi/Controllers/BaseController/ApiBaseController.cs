﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using Concentrix.MobileApp.Helper.DependecyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace Concentrix.MobileApp.WebApi.Controllers.BaseController
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ApiBaseController : ApiController
    {
        private readonly ITokenManager apiTokenManager;
        private string subRoutesKey = "MS_SubRoutes";
        private readonly int clientId = 1;

        public ApiBaseController()
        {
            apiTokenManager = DIServiceLocator.Resolve<ITokenManager>();
        }

        protected TokenDetailsResponse GetTokenDetails(string appName, string requestJson, out long serviceLogId)
        {
            var controllerActionName = GetControllerActionName();

            IEnumerable<string> tokenHeader;

            var tokenValue = string.Empty;

            var response = new TokenDetailsResponse();

            if (Request.Headers.TryGetValues("Token", out tokenHeader))
            {
                tokenValue = tokenHeader.FirstOrDefault();
            }

            //if (string.IsNullOrEmpty(tokenValue))
            //{
            //    response.Acknowledge = AcknowledgeType.Failure;
            //    response.Message = "Not a valid token";
            //    return response;
            //}

            var tokenRequest = new TokenRequest
            {
                AppName = appName,
                Token = tokenValue,
                LanguageCode = ""
            };

            var serviceLog = CreateServiceLogRequest(clientId, requestJson, tokenValue, controllerActionName);

            var appTokenResponse = apiTokenManager.ValidateAppNameToken(tokenRequest, serviceLog);

            serviceLogId = appTokenResponse.ServiceLogId;

            if (appTokenResponse.Code != "0")
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = appTokenResponse.Message;
                response.ErrorCode.Add(new ErrorDetails { Code = appTokenResponse.Code, Description = appTokenResponse.Message });

                return response;
            }

            return apiTokenManager.GetTokenDetails(tokenRequest);
        }

        protected BaseResponse ValidateAppName(string appName, string requestJson, out long serviceLogId)
        {
            var controllerActionName = GetControllerActionName();

            var response = new BaseResponse();

            //if (string.IsNullOrEmpty(appName))
            //{
            //    response.Acknowledge = AcknowledgeType.Failure;
            //    response.Message = "Not a valid App Name";
            //    return response;
            //}

            var tokenRequest = new TokenRequest
            {
                AppName = appName,
                Token = "",
                LanguageCode = ""
            };

            var serviceLog = CreateServiceLogRequest(clientId, requestJson, "", controllerActionName);

            var appTokenResponse = apiTokenManager.ValidateAppNameToken(tokenRequest, serviceLog);

            serviceLogId = appTokenResponse.ServiceLogId;

            if (appTokenResponse.Code == "50002" || appTokenResponse.Code == "0")
            {
                response.Acknowledge = AcknowledgeType.Success;
                response.Message = "Success";
            }
            else
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = appTokenResponse.Message;
                response.ErrorCode.Add(new ErrorDetails { Code = appTokenResponse.Code, Description = appTokenResponse.Message });
            }

            return response;
        }

        protected void UpdateServiceLog(long serviceLogId, string acknowledge, string message)
        {
            apiTokenManager.UpdateServiceLog(serviceLogId, acknowledge, message);
            return;
        }

        private ControllerAction GetControllerActionName()
        {
            var attributedRoutesData = HttpContext.Current.Request.RequestContext.RouteData.Values[subRoutesKey] as IEnumerable<IHttpRouteData>;
            var subRouteData = attributedRoutesData.FirstOrDefault();

            var actionDescriptor = (HttpActionDescriptor[])subRouteData.Route.DataTokens["actions"];
            var controllerName = actionDescriptor[0].ControllerDescriptor.ControllerName;
            var actionName = actionDescriptor[0].ActionName;

            return new ControllerAction
            {
                ControllerName = controllerName,
                ActionName = actionName
            };
        }

        private LogServiceCalls CreateServiceLogRequest(int clientId, string requestJson, string token, ControllerAction controllerAction)
        {
            return new LogServiceCalls
            {
                ClientId = clientId,
                URL = Request.RequestUri.AbsoluteUri.ToString(),
                ControllerName = controllerAction.ControllerName,
                ActionName = controllerAction.ActionName,
                Token = token,
                Request = requestJson,
                Acknowledge = null,
                Message = null
            };
        }
    }

    public class ControllerAction
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
}
