﻿using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Resource;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Resource
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class ResourceController : ApiBaseController
    {
        [HttpPost, Route("GetResource")]
        public IHttpActionResult GetResourceFile([FromBody] ResourceRequest resource, CancellationToken cancellationToken)
        {
            var resourceResponse = new ResouceResponse();
            JObject resourceKeyValuePair = null;

            var requestJson = JsonConvert.SerializeObject(resource);

            long serviceLogId = 0;

            var appResponse = ValidateAppName(resource.AppName, requestJson, out serviceLogId);

            if (appResponse.Acknowledge != AcknowledgeType.Success)
            {
                UpdateServiceLog(serviceLogId, appResponse.Acknowledge.ToString(), appResponse.Message);
                return Ok(appResponse);
            }

            if (string.IsNullOrEmpty(resource.Language))
            {
                resourceResponse.Acknowledge = AcknowledgeType.Failure;
                resourceResponse.Message = "Language Code is missing";
                resourceResponse.ErrorCode.Add(new ErrorDetails { Code = "20000", Description = "Missing Language Code" });

                UpdateServiceLog(serviceLogId, resourceResponse.Acknowledge.ToString(), resourceResponse.Message);

                return Ok(resourceResponse);
            }

            var resourcePath = ConfigurationManager.AppSettings["ResourceFilePath"];

            var pathWithFileName = resourcePath + resource.Language + ".json";

            if (!File.Exists(pathWithFileName))
            {
                resourceResponse.Acknowledge = AcknowledgeType.Failure;
                resourceResponse.Message = "Resource file not available";
                resourceResponse.ErrorCode.Add(new ErrorDetails { Code = "20001", Description = "Resource file is not available for the Language " + resource.Language });

                UpdateServiceLog(serviceLogId, resourceResponse.Acknowledge.ToString(), resourceResponse.Message);

                return Ok(resourceResponse);
            }

            try
            {
                resourceKeyValuePair = JObject.Parse(File.ReadAllText(pathWithFileName));

                if (resourceKeyValuePair != null)
                {
                    resourceResponse.ResourceContent = resourceKeyValuePair;
                    resourceResponse.Acknowledge = AcknowledgeType.Success;
                    resourceResponse.Message = "Success";
                }
                else
                {
                    resourceResponse.Acknowledge = AcknowledgeType.Failure;
                    resourceResponse.Message = "Failed to read the content of JSON";
                    resourceResponse.ErrorCode.Add(new ErrorDetails { Code = "20003", Description = "Invalid json file for the Language " + resource.Language });
                }
            }
            catch(Exception ex)
            {
                resourceResponse.Acknowledge = AcknowledgeType.Failure;
                resourceResponse.Message = "Not able to Parse the Json File for the Language " + resource.Language;
                resourceResponse.ErrorCode.Add(new ErrorDetails { Code = "20004", Description = ex.Message });
            }

            UpdateServiceLog(serviceLogId, resourceResponse.Acknowledge.ToString(), resourceResponse.Message);

            return Ok(resourceResponse);
        }
    }
}
