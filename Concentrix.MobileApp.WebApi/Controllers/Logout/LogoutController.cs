﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Logout;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Logout
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class LogoutController : ApiBaseController
    {
        private readonly ILogoutManager logoutManager;
        
        public LogoutController()
        {
            logoutManager = DIServiceLocator.Resolve<ILogoutManager>();
        }

        [HttpPost, Route("Logout")]
        public IHttpActionResult LogoutDevice([FromBody] LogoutRequest logout, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(logout);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(logout.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = logoutManager.LogoutDevice(logout,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
