﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.TrainingVisible;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.TrainingVisible
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class TrainingVisibleController : ApiBaseController
    {
        private readonly ITrainingVisibleManager trainingManager;
        string employeeId = string.Empty;
        public TrainingVisibleController()
        {
            trainingManager = DIServiceLocator.Resolve<ITrainingVisibleManager>();
        }


        [HttpPost, Route("TrainingVisible")]
        public IHttpActionResult TrainingVisible([FromBody] TrainingVisibleRequest payslip, CancellationToken cancellationToken)
        {
            var response = trainingManager.TrainingVisible(employeeId, cancellationToken);
            return Ok(response);
        }
    }
}
