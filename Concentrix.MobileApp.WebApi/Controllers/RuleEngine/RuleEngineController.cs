﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.RuleEngine;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.RuleEngine
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class RuleEngineController : ApiBaseController
    {
        private readonly IRuleEngineManager ruleManager;

        public RuleEngineController()
        {
            ruleManager = DIServiceLocator.Resolve<IRuleEngineManager>();
        }

        [HttpPost, Route("GenericFeatures")]
        public IHttpActionResult GenericFeatures([FromBody] FeatureRequest feature, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(feature);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(feature.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            feature.EmployeeId = tokenResponse.UserId;
            feature.LanguageCode = tokenResponse.LanguageCode;

            var response = ruleManager.GenericFeatures(feature, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
