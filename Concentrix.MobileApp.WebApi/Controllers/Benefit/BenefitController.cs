﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Benefits;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Benefit
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class BenefitController : ApiBaseController
    {

        private readonly IBenefitManager benefitManager;
      
        public BenefitController()
        {
            benefitManager = DIServiceLocator.Resolve<IBenefitManager>();
        }

        [HttpPost, Route("GetBenefits")]
        public IHttpActionResult GetBenefits([FromBody] BenefitsRequest benefit, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(benefit);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(benefit.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = benefitManager.GetBenefits(tokenResponse.UserId,tokenResponse.LanguageCode, cancellationToken);
            //var response = benefitManager.GetBenefits(employeeId,languageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
