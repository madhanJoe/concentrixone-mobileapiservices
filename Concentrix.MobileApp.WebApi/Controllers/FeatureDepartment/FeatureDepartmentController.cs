﻿using Newtonsoft.Json;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.FeatureDepartment;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;

namespace Concentrix.MobileApp.WebApi.Controllers.FeatureDepartment
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class FeatureDepartmentController : ApiBaseController
    {

        private readonly IFeatureDepartmentManager featureDepartmentManager;

        public FeatureDepartmentController()
        {
            featureDepartmentManager = DIServiceLocator.Resolve<IFeatureDepartmentManager>();
        }


        [HttpPost, Route("GetSurveyDeparments")]
        public IHttpActionResult GetSurveyDeparments([FromBody] GetFeatureDepartmentRequest featureDepartment, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(featureDepartment);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(featureDepartment.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = featureDepartmentManager.GetSurveyDeparments(tokenResponse.UserId, tokenResponse.LanguageCode, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
