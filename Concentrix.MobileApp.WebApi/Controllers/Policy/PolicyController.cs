﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Policy;
using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Controllers.BaseController;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Concentrix.MobileApp.WebApi.Controllers.Policy
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ConcentrixOne")]
    public class PolicyController : ApiBaseController
    {
        private readonly IPolicyManager policyManager;
      
        public PolicyController()
        {
            policyManager = DIServiceLocator.Resolve<IPolicyManager>();
        }


        [HttpPost, Route("RequestPolicy")]
        public IHttpActionResult RequestPolicy([FromBody] AddPolicyRequest policyRequest, CancellationToken cancellationToken)
        {
            var requestJson = JsonConvert.SerializeObject(policyRequest);

            long serviceLogId = 0;

            var tokenResponse = GetTokenDetails(policyRequest.AppName, requestJson, out serviceLogId);

            if (tokenResponse.Acknowledge != AcknowledgeType.Success)
            {
                var errorResponse = new BaseResponse
                {
                    Acknowledge = tokenResponse.Acknowledge,
                    Message = tokenResponse.Message,
                    ErrorCode = tokenResponse.ErrorCode
                };

                UpdateServiceLog(serviceLogId, errorResponse.Acknowledge.ToString(), errorResponse.Message);

                return Ok(errorResponse);
            }

            var response = policyManager.RequestPolicy(policyRequest,tokenResponse.UserId, cancellationToken);

            UpdateServiceLog(serviceLogId, response.Acknowledge.ToString(), response.Message);

            return Ok(response);
        }
    }
}
