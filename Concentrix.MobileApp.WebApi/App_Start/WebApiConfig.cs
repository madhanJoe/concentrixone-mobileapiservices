﻿using Concentrix.MobileApp.Helper.DependecyInjection;
using Concentrix.MobileApp.WebApi.Handler;
using Concentrix.MobileApp.WebApi.Injection;
using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Concentrix.MobileApp.WebApi.JsonContent;

namespace Concentrix.MobileApp.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // for CORS
            //var cors = new EnableCorsAttribute("*", "*", "*");
            //config.EnableCors(cors);

            //config.EnableCors();

            // Bootstrap the Unity Dependency Injection
            var unityContainer = UnityBootstrapper.CreateUnityContainer();
            if (unityContainer != null)
            {
                DIServiceLocator.Container = unityContainer;
                config.DependencyResolver = new UnityDependencyResolver(unityContainer);
            }
            else
                throw new Exception("MobileApp Web Api - Unity Container not created");

            config.Formatters.XmlFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            // Web API global Exception Handler Register
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            // convert all the response to JSON
            var jsonFormatter = new JsonMediaTypeFormatter();
            config.Services.Replace(typeof(IContentNegotiator), new JsonContentNegotiator(jsonFormatter));

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
