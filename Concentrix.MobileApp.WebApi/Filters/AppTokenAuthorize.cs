﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.Domain.DomainModels.Base;
using Concentrix.MobileApp.Domain.DomainModels.Log;
using Concentrix.MobileApp.Domain.DomainModels.Token;
using Concentrix.MobileApp.Helper.DependecyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Concentrix.MobileApp.WebApi.Filters
{
    public class AppTokenAuthorize : AuthorizeAttribute
    {
        private readonly ITokenManager tokenManager;

        public AppTokenAuthorize()
        {
            tokenManager = DIServiceLocator.Resolve<ITokenManager>();
        }
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            var tokenValue = filterContext.Request.Headers.GetValues("Token").First();

            var tokenRequest = new TokenRequest
            {
                AppName = "",
                Token = tokenValue,
                LanguageCode = ""
            };

            var serviceLog = new LogServiceCalls();

            var response = tokenManager.ValidateAppNameToken(tokenRequest, serviceLog);

            if (response.Code == "0")
                return;

            filterContext.Response =
                new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

            //HttpError myCustomError = new HttpError("The file has no content or rows to process.") { { "CustomErrorCode", 42 } };
            //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, myCustomError);


            //{
            //    Acknowledge = AcknowledgeType.Error,
            //    Message = context.Exception.Message,
            //    ErrorCode = errorDetails
            //}
        }
    }
}