﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace Concentrix.MobileApp.WebApi.Injection
{
    public class UnityDependencyResolver : IDependencyResolver
    {
        private readonly IUnityContainer unityContainer;

        public UnityDependencyResolver(IUnityContainer Container)
        {
            unityContainer = Container;
        }

        public IDependencyScope BeginScope()
        {
            var child = unityContainer.CreateChildContainer();
            return new UnityDependencyResolver(child);
        }

        public object GetService(Type serviceType)
        {
            return unityContainer.IsRegistered(serviceType) ? unityContainer.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return unityContainer.IsRegistered(serviceType) ? unityContainer.ResolveAll(serviceType) : new List<object>();
        }

        public void Dispose()
        {
            unityContainer.Dispose();
        }
    }
}