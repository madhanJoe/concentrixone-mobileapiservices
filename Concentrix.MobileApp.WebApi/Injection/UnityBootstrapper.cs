﻿using Concentrix.MobileApp.ApiManager.Interfaces;
using Concentrix.MobileApp.ApiManager.Managers;
using Concentrix.MobileApp.Repository.Interfaces;
using Concentrix.MobileApp.Repository.Repositories;
using Microsoft.Practices.Unity;

namespace Concentrix.MobileApp.WebApi.Injection
{
    public class UnityBootstrapper
    {
        private static IUnityContainer container;

        static UnityBootstrapper()
        {
            if (container == null)
                container = new UnityContainer();
        }

        public static IUnityContainer CreateUnityContainer()
        {
            return BuildUnityContainer();
        }

        private static IUnityContainer BuildUnityContainer()
        {
            if (container == null)
                container = new UnityContainer();
            RegisterTypes();
            return container;
        }

        private static void RegisterTypes()
        {
            container.RegisterType<ITestManager, TestManager>();
            container.RegisterType<ITestRepository, TestRepository>();
            container.RegisterType<IEntityFrameDBAccess, EntityFrameDBAccess>();
            container.RegisterType<IAttributeManager, AttributeManager>();
            container.RegisterType<IAttributeRepository, AttributeRepository>();
            container.RegisterType<IBenefitManager, BenefitManager>();
            container.RegisterType<IBenefitRepository, BenefitRepository>();
            container.RegisterType<ICallBackManager, CallBackManager>();
            container.RegisterType <ICallBackRepository, CallBackRepository>();
            container.RegisterType<IHolidayManager, HolidayManager>();
            container.RegisterType<IHolidayRepository, HolidayRepository>();
            container.RegisterType<ILeaveManager, LeaveManager>();
            container.RegisterType<ILeaveRepository, LeaveRepository>();
            container.RegisterType<ILocationManager, LocationManager>();
            container.RegisterType<ILocationRepository, LocationRepository>();
            container.RegisterType<ILoginManager, LoginManager>();
            container.RegisterType<ILoginRepository, LoginRepository>();
            container.RegisterType<ILogoutManager, LogoutManager>();
            container.RegisterType<ILogoutRepository, LogoutRepository>();
            container.RegisterType<IMessageManager, MessageManager>();
            container.RegisterType<IMessageRepository, MessageRepository>();
            container.RegisterType<IMoodMeterManager, MoodMeterManager>();
            container.RegisterType<IMoodMeterRepository, MoodMeterRepository>();
            container.RegisterType<IPayStubManager, PayStubManager>();
            container.RegisterType<IPayStubRepository, PayStubRepository>();
            container.RegisterType<IPolicyManager, PolicyManager>();
            container.RegisterType<IPolicyRepository, PolicyRepository>();
            container.RegisterType<IPredictiveFeedbackManager, PredictiveFeedbackManager>();
            container.RegisterType<IPredictiveFeedbackRepository, PredictiveFeedbackRepository>();
            container.RegisterType<IProfileManager, ProfileManager>();
            container.RegisterType<IProfileRepository, ProfileRepository>();
            container.RegisterType<IQueryManager, QueryManager>();
            container.RegisterType<IQueryRepository, QueryRepository>();
            container.RegisterType<IQuizManager, QuizManager>();
            container.RegisterType<IQuizRepository, QuizRepository>();
            container.RegisterType<IRequestManager, RequestManager>();
            container.RegisterType<IRequestRepository, RequestRepository>();
            container.RegisterType<ISurveyManager, SurveyManager>();
            container.RegisterType<ISurveyRepository, SurveyRepository>();
            container.RegisterType<ITrainingCalendarManager, TrainingCalendarManager>();
            container.RegisterType<ITrainingCalendarRepository, TrainingCalendarRepository>();
            container.RegisterType<ITrainingVisibleManager, TrainingVisibleManager>();
            container.RegisterType<ITrainingVisibleRepository, TrainingVisibleRepository>();
            container.RegisterType<IURLManager, URLManager>();
            container.RegisterType<IURLRepository, URLRepository>();
            container.RegisterType<IWecareManager, WecareManager>();
            container.RegisterType<IWecareRepository, WecareRepository>();
            container.RegisterType<ITokenManager, TokenManager>();
            container.RegisterType<ITokenRepository, TokenRepository>();
            container.RegisterType<ILanguageManager, LanguageManager>();
            container.RegisterType<ILanguageRepository, LanguageRepository>();
            container.RegisterType<IRuleEngineManager, RuleEngineManager>();
            container.RegisterType<IRuleEngineRepository, RuleEngineRepository>();
            container.RegisterType<IFeatureDepartmentManager, FeatureDepartmentManager>();
            container.RegisterType<IFeatureDepartmentRepository, FeatureDepartmentRepository>();
        }
    }
}