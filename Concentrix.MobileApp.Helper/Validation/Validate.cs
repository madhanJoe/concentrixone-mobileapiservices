﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Concentrix.MobileApp.Helper.Validation
{
    public static class Validate
    {
        public static bool IsValidMobileNumber(string mobileNumber)
        {
            long isValidMobileNumber;

            var isValid = long.TryParse(mobileNumber, out isValidMobileNumber);

            if (mobileNumber.Length <= 16)
                return isValid;
            else
                return false;

            //return Regex.Match(mobileNumber, @"^[1-9]\d{16}$").Success;
        }

        public static bool IsValidEmail(string emailId)
        {
            return Regex.IsMatch(emailId, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }
    }
}
