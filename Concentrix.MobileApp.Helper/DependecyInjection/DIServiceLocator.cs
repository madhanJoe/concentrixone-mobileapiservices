﻿using Microsoft.Practices.Unity;

namespace Concentrix.MobileApp.Helper.DependecyInjection
{
    public static class DIServiceLocator
    {
        public static IUnityContainer Container { get; set; }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
